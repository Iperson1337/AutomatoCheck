# Запуск проекта на локалном сервере
- Поменять server->config->development->telegram_bot.token на свой телеграм token
- npm run startDev

# Как запускать сервера через pm2
- pm2 start npm --no-automation --name front_server -- run startProd

# Checklist API

### Сотрудники
- [Все сотрудники](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D0%B2%D1%81%D0%B5-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B8)
- [Просмотр сотрудника](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D0%BF%D1%80%D0%BE%D1%81%D0%BC%D0%BE%D1%82%D1%80-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B0)
- [Редактировать сотрудника](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D1%80%D0%B5%D0%B4%D0%B0%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B0)
- [Уволить сотрудника](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D1%83%D0%B2%D0%BE%D0%BB%D0%B8%D1%82%D1%8C-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B0)
- [Количества сотрудников](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D0%BA%D0%BE%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%B0-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2)
- [Сотрудники в Офисе](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B8-%D0%B2-%D0%BE%D1%84%D0%B8%D1%81%D0%B5)
- [Сотрудники вне офиса](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B8-%D0%B2%D0%BD%D0%B5-%D0%BE%D1%84%D0%B8%D1%81%D0%B0)
- [Cтатистика месяца](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#c%D1%82%D0%B0%D1%82%D0%B8%D1%81%D1%82%D0%B8%D0%BA%D0%B0-%D0%BC%D0%B5%D1%81%D1%8F%D1%86%D0%B0)
- [Круговая диаграмма](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D0%BA%D1%80%D1%83%D0%B3%D0%BE%D0%B2%D0%B0%D1%8F-%D0%B4%D0%B8%D0%B0%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0)

### Департаменты
- [Все департаменты](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/departmant.md#%D0%B2%D1%81%D0%B5-%D0%B4%D0%B5%D0%BF%D0%B0%D1%80%D1%82%D0%B0%D0%BC%D0%B5%D0%BD%D1%82%D1%8B)
- [Количество департаментов](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/departmant.md#%D0%BA%D0%BE%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%BE-%D0%B4%D0%B5%D0%BF%D0%B0%D1%80%D1%82%D0%B0%D0%BC%D0%B5%D0%BD%D1%82%D0%BE%D0%B2)
- [Добавить департамент](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/departmant.md#%D0%B4%D0%BE%D0%B1%D0%B0%D0%B2%D0%B8%D1%82%D1%8C-%D0%B4%D0%B5%D0%BF%D0%B0%D1%80%D1%82%D0%B0%D0%BC%D0%B5%D0%BD%D1%82)
- [Удалить департамент](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/departmant.md#%D1%83%D0%B4%D0%B0%D0%BB%D0%B8%D1%82%D1%8C-%D0%B4%D0%B5%D0%BF%D0%B0%D1%80%D1%82%D0%B0%D0%BC%D0%B5%D0%BD%D1%82)
