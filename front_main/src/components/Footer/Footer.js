import React, { Component } from 'react'
class Footer extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }
  render () {
    return (
      <div className='flex fl-c fl-aic fl-dc h-10vh lh1i5'>
      <p className="fs-14w200 colorfff"><span className='colorfff pr-5px'>AUTOMATO </span> | СИСТЕМА УЧЕТА РАБОЧЕГО ВРЕМЕНИ СОТРУДНИКОВ</p>
      <div className='flex fl-dr fs-10w200 colorfff'>
        <p className='pr-5px'>© 2008 — 2017 MADE WITH</p>
        <img src="images/heart.svg" className='pos-rel top--1rem left--1rem h-46px' />
        <p className='pos-rel left--1-7rem'>BY AUTOMATO</p>
      </div>
      </div>
    )
  }
}

export default Footer