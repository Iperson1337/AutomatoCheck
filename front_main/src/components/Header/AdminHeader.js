import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class AdminHeader extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }
 
  render () {
    // (this.props.match.params.parameter, 'bind')
    return (
      <section className='h-4rem bgc2c394d flex fl-aic fl-sb'>
      <Link to="/adminmain"> 
        <img src='images/Automato.svg' className='ml-16px h-2rem' />
       </Link>
          
          <div className='flex mr-16px'>
          <div className=''>
            <Link to="/adminmain" className='text-up fs-10w600 colorfff6f6 '> Главная </Link>
            <Link to="/adminemployee" className='text-up fs-10w600 colorfff6f6 ml-16px'> сотрудники</Link>
            <Link to="/calendar" className='text-up fs-10w600 colorfff6f6 ml-16px'> праздники</Link>
          </div>
         </div>
      </section>
    )
  }
}


export default AdminHeader
// export default Header
