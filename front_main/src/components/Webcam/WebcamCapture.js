import React from 'react';
import Webcam from 'react-webcam';
import { Link } from 'react-router-dom'

class WebcamCapture extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
          img:'',
          showWeb: true
        }
        
      }
    setRef = (webcam) => {
      this.webcam = webcam;
    }
   
    capture = () => {
      this.setState({
       showWeb:false,
       img: this.webcam.getScreenshot()
    });
    };
    
    recapture = () => {
        this.setState({
           showWeb:true,
           img:''
        });
      };
      sent(){
        this.props.sentImage(this.state.img)
      }
   
    render() {
      const videoConstraints = {
        width: 600,
        height: 460,
        facingMode: 'user',
      };
      return (
        <section className="h-90vh flex  fl-dc fl-c fl-aic">
        <img src='images/Automato.svg' alt='vatafak' className="h-35px fl-dc" />
        <div className={this.props.code.isAdmin ? 'mt-2rem ' : 'hidden'}>
          <p className='colorfff fs-14w200'>Хотите перейте к админ панели?</p>
          <Link to='/adminmain' className='w-100 flex fl-c'>
            <p className='br-8px bgc146aa4 h-35px w-150px colorfff fs-14w200 mt-1rem flex fl-aic fl-c text-up'>да</p>
          </Link>
        </div>
        <div className='bgc42556a w-544px h-384px flex fl-dc fl-aic br-18px mt-5rem'>
        {this.state.showWeb?
          <Webcam
            className="br-30px"
            audio={false}
            height={345}
            ref={this.setRef}
            screenshotFormat="image/jpeg"
            width={450}
            style={{ position:"relative", top:'1rem'}}
            videoConstraints={videoConstraints}
          />
          : <img src={this.state.img} className="top-1rem pos-rel"/> }
        </div>
        {this.state.showWeb?
        <button className='br-8px bgc146aa4 h-35px w-150px colorfff fs-14w200 mt-2rem' onClick={this.capture.bind(this)}>CHEESE</button>
        :
        <div>
        <Link to="/approve2" className='mt-2rem'>
            <button className='br-8px bgc146aa4 h-35px w-150px colorfff fs-14w200 mt-2rem mr-2rem' onClick={this.recapture.bind(this)} >Переснять</button>
          </Link>
          <Link to={this.props.code.next=='out' ?"/approvereport" :"/approve3"}className='mt-2rem'>
          <button className='br-8px bgc146aa4 h-35px w-150px colorfff fs-14w200 mt-2rem ' onClick={this.sent.bind(this)} >Продолжить</button>
        </Link>
        </div>
        }
        
        {/* <img src={this.state.img  }/> */}
      </section>
      );
    }
  }

  
  export default WebcamCapture