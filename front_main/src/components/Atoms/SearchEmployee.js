import React, { Component }  from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as getEmployees from '../../actions/Employees'
class SearchEmployee extends Component {
    constructor (props) {
        super(props)
        this.state = {
            search:''
        }
        this.updateSearch = this.updateSearch.bind(this);
      }

    componentDidMount() {
        this.props.getEmployees.getEmployee()
      }
    updateSearch(e){
        this.setState({
            search: e.target.value
        })
    }
    render () {
        let filteredContacts = this.props.Employee.employees.filter(
            (employee) => {
                return employee.firstname.toLowerCase().indexOf(this.state.search.toLowerCase()) != -1 || employee.lastname.toLowerCase().indexOf(this.state.search.toLowerCase()) != -1 ;
            }
        )
        return (
            <section className='w-15 h-100vh  bgc3A455D'>
                <div className='h-2rem flex fl-aic '>
                    <p className='colorfff fs-10w200 text-up ml-16px'>Поиск </p>
                    <input className='searchinput pd0 ml-16px' value={this.state.search} onChange={this.updateSearch}/>
                </div>
                <div className='h-90vh flex fl-dc overflowyscroll'>
                {filteredContacts.map((employee, index) => 
                   <div className='flex fl-aic h-4rem w-100 mt-8px ml-16px' key={index} onClick={()=>this.props.selected(employee.employee_id,employee._id)}>
                        <img src={'/userimages/'+employee.employee_id.toLowerCase()+'.jpg'} onError={(e)=>{e.target.src="images/userphoto.png"}} className='h-3rem w-3rem br-50'/> 
                        <p className='colorfff fs-10w200 text-up ml-16px' >{employee.firstname + ' ' + employee.lastname} </p>
                    </div> 
                )
                }
                </div>
            </section>
        )
}
}

function mapStateToProps (state) {
    return {
        Employee: state.Employee
    }
  }
  function mapDispatchToProps (dispatch) {
    return {
        getEmployees: bindActionCreators(getEmployees, dispatch)
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(SearchEmployee)