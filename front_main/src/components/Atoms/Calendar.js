import React, { Component } from 'react'
import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import 'moment/locale/ru';
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment));// or globalizeLocalizer


    class Calendar extends Component {
      
        constructor(props){
            super(props)
            this.state = {
              events:[{  
                id: 0,
                title: 'выходные',
                allDay: true,
                start: '',
                end: '',
              }]}
            this.updateEvent=this.updateEvent.bind(this)
            this.removeEvent=this.removeEvent.bind(this)
          } 
          notify = () => {
        
            toast.success("Cохранено!", {
              position: toast.POSITION.TOP_CENTER
            });
          };  
          
          updateEvent(startTime,endTime){
            this.setState({ events: [...this.state.events, 
                ...[{
                    id: 0,
                    title: this.state.events[0].title,
                    allDay: this.state.events[0].allDay,
                    start: startTime,
                    end: moment(endTime).add(1,'days')
              }] 
            ] 
            })
            this.notify()
          }   
          removeEvent(event) {
            var array = [...this.state.events]; 
            var index = array.indexOf(event)
            array.splice(index, 1);
            this.setState({events: array});

          }
        render () {
            (this.state.events ,'default')
            const messages = {
              previous: 'Предыдущий месяц',
              next: 'Следующий месяц',
              today: 'Нынешний месяц',
            };
          return (
            <React.Fragment>
            <h3 className="callout text-up colorfff mt-2rem mb-1rem" >
            выберите даты праздников
            </h3>
            {/* <div className={this.state.events.length!=1 ? 'h-1rem flex fl-fe mr-16px' :'hidden h-1rem'}>
              <img src='images/done.svg' className='w-2rem '/>
              <p className='color9DE86F fs-12w200 text-a-center'>Cохранено</p>
            </div> */}
            <ToastContainer 
              closeOnClick={false} 
              draggablePercent={60} 
              hideProgressBar={true}
              autoClose={2000}
              />
            <BigCalendar
                selectable
                events={this.state.events}
                defaultView={BigCalendar.Views.MONTHS}
                scrollToTime={new Date(1970, 1, 1, 6)}
                defaultDate={new Date()}
                onSelectSlot={(event) =>
                  this.updateEvent(event.start, event.end)
                }
                messages={messages}
                onSelectEvent={ (event)=>this.removeEvent(event)}
            />
            </React.Fragment>
        )
      }
    }
        
  export default Calendar
