import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'

class TimeCounter extends Component {
  constructor (props) {
    super(props)
    this.state = {
        userId: JSON.parse(localStorage.getItem('userId'))
    }
  }
  componentDidMount(){
    this.props.getUsers
  }
  render () {
    const checkedIn=this.props.checkedIn
     const averagetime=this.props.averageTime
     const remainedtime=this.props.remainedTime
    return (
        <div >
        { this.props.employee.map((employee,index)=>(
              employee.employee_id.toLowerCase()==this.state.userId.toLowerCase()) ?
              <section key={index} className='w-100 flex fl-dw fl-sb mt-1rem'>
                    <div className='w-23 h-100px br-8px bgcimg65c93e flex fl-c fl-aic'>
                    <img src='images/clock.svg' className='mr-2rem h-2rem'/>
                    <div className='flex fl-dc fl-aifs '>
                        <p className='colorfff text-low fs-29w600'>{checkedIn? checkedIn.substring(0,checkedIn.indexOf(":"))+" Ч : "+checkedIn.substring(checkedIn.indexOf(":")+1, checkedIn.length) + " мин": null}</p>
                        <p className='text-up fs-10w700 colorfff mt-3px'>Последний чекин</p>
                    </div>
                    </div>
                    <div className='w-23 h-100px br-8px bgcimg70cdba flex fl-c fl-aic'>
                    <img src='images/timer.svg' className='mr-2rem h-2rem'/>
                    <div className='flex fl-dc fl-aifs '>
                        <p className='colorfff text-low fs-29w600'>{averagetime? averagetime.substring(0,averagetime.indexOf(":"))+" Ч : "+averagetime.substring(averagetime.indexOf(":")+1, averagetime.length) + " мин": null}</p>
                        <p className='text-up fs-10w700 colorfff mt-3px'>Время работы</p>
                    </div>
                    </div>
                    <div className='w-23 h-100px br-8px bgcimge03764 flex fl-c fl-aic'>
                    <img src='images/timer.svg' className='mr-2rem h-2rem'/>
                    <div className='flex fl-dc fl-aifs '>
                        <p className='colorfff text-low fs-29w600'>{remainedtime? remainedtime.substring(0,remainedtime.indexOf(":"))+" Ч : "+remainedtime.substring(remainedtime.indexOf(":")+1, remainedtime.length) + " мин": null}</p>
                        <p className='text-up fs-10w700 colorfff mt-3px'>Недоработки</p>
                    </div>
                    </div>
                    <div className='w-23 h-100px br-8px bgcimg44b8d5 flex fl-c fl-aic'>
                    <img src='images/karma.svg' className='mr-2rem h-35px'/>
                    <div className='flex fl-dc fl-aifs '>
                        <p className='colorfff text-low fs-29w600'>0 очков</p>
                        <p className='text-up fs-10w700 colorfff mt-3px'>Очки кармы</p>
                    </div>
                    </div>
                </section>
                    : null
                ) }
        </div>

        )
    }
}
export default TimeCounter
