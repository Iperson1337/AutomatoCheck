import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class AdminTimeCounter extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }
  render () {
    const checkedIn=this.props.checkedIn
    const averagetime=this.props.averageTime
    const remainedtime=this.props.remainedTime
    return (
        <div className='w-100 flex fl-dw fl-sb mt-1rem'>
                    <div className='w-22 h-50px br-8px bgcimg65c93e flex fl-c fl-aic ml-16px'>
                    <img src='images/clock.svg' className='mr-2rem h-1-5rem'/>
                    <div className='flex fl-dc fl-aifs '>
                        <p className='colorfff text-low fs-12w200'>{checkedIn? checkedIn.substring(0,checkedIn.indexOf(":"))+" Ч : "+checkedIn.substring(checkedIn.indexOf(":")+1, checkedIn.length) + " мин": null}</p>
                        <p className='text-up fs-10w200 colorfff mt-3px'>Последний чекин</p>
                    </div>
                    </div>
                    <div className='w-22 h-50px br-8px bgcimg70cdba flex fl-c fl-aic ml-16px'>
                    <img src='images/timer.svg' className='mr-2rem h-1-5rem'/>
                    <div className='flex fl-dc fl-aifs '>
                        <p className='colorfff text-low  fs-12w200'>{averagetime? averagetime.substring(0,averagetime.indexOf(":"))+" Ч : "+averagetime.substring(averagetime.indexOf(":")+1, averagetime.length) + " мин": null}</p>
                        <p className='text-up fs-10w200 colorfff mt-3px'>Время работы</p>
                    </div>
                    </div>
                    <div className='w-22 h-50px br-8px bgcimge03764 flex fl-c fl-aic ml-16px'>
                    <img src='images/timer.svg' className='mr-2rem h-1-5rem'/>
                    <div className='flex fl-dc fl-aifs '>
                        <p className='colorfff text-low  fs-12w200'>{remainedtime? remainedtime.substring(0,remainedtime.indexOf(":"))+" Ч : "+remainedtime.substring(remainedtime.indexOf(":")+1, remainedtime.length) + " мин": null}</p>
                        <p className='text-up fs-10w200 colorfff mt-3px'>Недоработки</p>
                    </div>
                    </div>
                    <div className='w-22 h-50px br-8px bgcimg44b8d5 flex fl-c fl-aic ml-16px'>
                    <img src='images/karma.svg' className='mr-2rem h-1-5rem'/>
                    <div className='flex fl-dc fl-aifs '>
                        <p className='colorfff text-low  fs-12w200'>0 очков</p>
                        <p className='text-up fs-10w200 colorfff mt-3px'>Очки кармы</p>
                    </div>
                    </div>
        </div>

        )
    }
}
export default AdminTimeCounter
