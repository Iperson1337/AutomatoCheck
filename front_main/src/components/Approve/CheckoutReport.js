import React, { Component } from 'react'
import { Link } from 'react-router-dom'


class CheckoutReport extends Component {
  constructor (props) {
    super(props)
    this.state = {
      report:''
    }
    this.handleChange=this.handleChange.bind(this)
  }
  handleChange(event) {
    this.setState({
      report: event.target.value
    });
  }
 
  render () {
    return (
      <section className="h-90vh flex  fl-dc fl-c fl-aic">
        <img src='images/Automato.svg' alt='vatafak' className="h-35px fl-dc" />
        <div className='h-3rem w-480px bgc354054 mt-5rem br-t-8px flex fl-aic fl-sb pdl-1rem colorfff fs-14w700'>
          <p>Перед завершением работы: </p>
      </div>
      <div className='h-10rem w-480px opacity02 bgc26364f br-b-8px pdx-1rem mb-3rem'>
        <div className='flex fl-sb w-30rem'>
            <textarea  onChange={this.handleChange} className='mt-1rem h-8rem w-24rem colorfff bgctrans b2E425E' placeholder='Напишите отчет...' required />
            <Link  to='/approveinsight' >
               <img src='images/next.svg' className="pos-rel w-4rem h-3rem top-3rem left--1rem" onClick={()=>this.props.checkoutReport(this.state.report)} />
            </Link>
          </div>

      </div>
        

        {/* <div className="flex fl-dr">
            <img src='images/dot.png' alt='vatafak' className="h-10px pr-5px" />
            <img src='images/active-dot.png' alt='vatafak' className="h-10px pr-5px" />
            <img src='images/dot.png' alt='vatafak' className="h-10px pr-5px" />
            <img src='images/dot.png' alt='vatafak' className="h-10px pr-5px" />
        </div> */}
      </section>
    )
  }
}

export default CheckoutReport
