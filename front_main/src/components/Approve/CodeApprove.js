import React, { Component } from 'react'
import { Link } from 'react-router-dom'


class CodeApprove extends Component {
  
  constructor (props) {
    super(props)
    this.state = {
      user:JSON.parse(localStorage.getItem('currentUser')),
      userId : JSON.parse(localStorage.getItem('userId'))
    }

  }
  componentDidMount(){
    const user = localStorage.getItem('userId')
  }
  render () {

    return (
      <section className="h-90vh flex  fl-dc fl-c fl-aic">
        <img src='images/Automato.svg' className='ml-16px h-2rem' />
        <div className="w-37rem h-20rem  bgc202d45 mt-5rem br-8px flex fl-dc fl-aic ">
        <div className="pos-rel top-15 flex fl-dc fl-aic">
            <img src={'/userimages/'+this.state.userId+'.jpg'} onError={(e)=>{e.target.src="images/userphoto.png"}} className='br-50 h-5rem w-5rem'/> 
            <p className="mt-1rem colorfff fs-14w200 opacity051"> Здравствуйте, {this.state.user.firstname}!</p>
        </div>
        <div className='flex fl-sb w-30rem'>
            <p className='fs-16w200 colorfff mb-1rem pos-rel top-2rem'>Введите код</p>
            <Link  to={this.props.code.next=='in' ?"/approve2" :"/"} >
             <img src='images/next.svg' className="pos-rel w-4rem h-3rem top-2rem left-16px" />
            </Link>
          </div>
          <input type='text' className='signInput w-30rem h-2rem' onChange={this.props.checkCode}/>
          <div className={this.props.success !=false ? "hidden" : "colorf0475f fs-12w200 mt-1rem"}>вы ввели не правильный ID</div>
        
          <div className="flex fl-dr pos-rel top--15 ml-3rem" >
              <p className="colorfff fs-14w200 opacity051 pr-5px">
              Это не я
              </p>
              <Link to="/approvelogin" onClick={()=>localStorage.removeItem('userId')}>
                 <img src='images/redclose.svg' className="pos-rel h-3-6rem top--1-2rem left--1-5rem" />
              </Link>
          </div>
        </div>
        {/* <div className="flex fl-dr mt-1rem">
            <img src='images/active-dot.png' className="h-10px pr-5px" />
            <img src='images/dot.png' className="h-10px pr-5px" />
            <img src='images/dot.png' className="h-10px pr-5px" />
            <img src='images/dot.png' className="h-10px pr-5px" />
        </div> */}
      </section>
    )
  }
}


export default CodeApprove
