import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as getDepartment from '../../actions/DepartmentList'
import * as showModalActions from '../../actions/ShowModal'
 
import 'react-datepicker/dist/react-datepicker.css';

class DepartmentList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      err:false
    };
    this.deleteDep=this.deleteDep.bind(this)
  }

  componentDidMount() {
    this.props.getDepartment.getDepartment()
  }
  async deleteDep(){
    await this.props.getDepartment.deleteDepartment()
    setTimeout(() => {
      this.props.DepartmentList.deletedDep.success==true ? this.props.getDepartment.getDepartment() 
    : null
    }, 100);
    
  }
  render () {
    return (
      <div className="w-100 h-100vh flex fl-dc fl-c fl-aic bg_161f29_08 pos-abs top-0">
       <div className='h-3rem w-480px bgc354054 mt-2rem br-t-8px flex fl-aic fl-sb text-up pdl-1rem colorfff fs-14w700'>
          <p>список отделов</p>
          <img src='images/close1.svg' className='w-50px cursor' onClick={() => this.props.showModalActions.showDepartmentList(false)}/>
      </div>
      <div className='h-27rem w-480px opacity02 bgc26364f br-b-8px pdx-1rem mb-7rem'>
        <div className='h-20rem overflowyscroll mt-2rem'>
        <div className='flex fl-ы'>
          <p className='ml-16px colorfff fs-12w200'>Название</p>
          <p className='ml-11rem colorfff fs-12w200'>Количество сотрудников</p>
        </div>
        {this.props.DepartmentList.departments.map((department, index) => 
            <div className='h-5rem flex fl-dr fl-aic ml-16px' key={index}>
                <img src='images/oval.png'/>
                <p className='pos-rel left--20px colorfff fs-14w400 text-up'>{department.name.substring(0,1)}</p>
                <p className='w-480px ml-2rem flex fl-s colorfff fs-14w400 text-cap'>{department.name}</p>
                <p className='w-160px color6885a7 fs-14w400 flex fl-s'>{this.props.DepartmentList.departments[index].employees.length}</p>
                <input type='checkbox' onChange={() => this.props.getDepartment.checkDepartment(department.short_name)}  />
            </div>
        )
        }
        </div>
        <div className='flex fl-fe mt-1rem'>
            <button type='submit' className='bgcc51212 w-150px h-2rem text-up colorfff br-8px fs-10w600' onClick={this.deleteDep}>удалить</button>
        </div>
        <div className={this.props.DepartmentList.deletedDep.success!=false ? "hidden" : "colorf0475f fs-12w200 mt-8px"}>Выбран отдел где есть сотрудники!</div>
      </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    DepartmentList: state.DepartmentList
  }
}
function mapDispatchToProps (dispatch) {
  return {
    getDepartment: bindActionCreators(getDepartment, dispatch),
    showModalActions: bindActionCreators(showModalActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DepartmentList)
