import {CHECKIN, CHECKCODE, SENTIMAGE, GETQUESTIONS,SENTNEGATIVEQUESTION, CHECKOUT} from '../constants/Checkin'
import axios from 'axios'

import createHistory from 'history/createBrowserHistory'
const history = createHistory({forceRefresh: true})
//list of checkin questions
const questions=[
{
    question: 'Зайди в Pivotal/Trello/Wunderlist и проверь свои задачи.',
    posAnswer: 'Проверил(-а) Pivotal/Trello/Wunderlist',
    negAnswer: 'Не проверил(-а) Pivotal/Trello/Wunderlist'
},
{
    question: 'Мозги на месте?',
    posAnswer: 'Мозги на месте',
    negAnswer: 'Мозги не на месте'
},
{
    question: 'Выспались?',
    posAnswer: 'Да, выспался(-лась)',
    negAnswer: 'Нет, не выспался(-лась)'
},
{
    question: 'Настройлись на работу?',
    posAnswer: 'Да, настроился(-лась)',
    negAnswer: 'Нет, не настроился(-лась)'
}]
export function getQuestions(){
    return function(dispatch){
        dispatch({
            type: GETQUESTIONS,
            payload: questions
        })
    }
}
//check userId
export function checkin(id){
    return function(dispatch){
        if(id.length>5){
            axios.post('/api/check',{id:id})
            .then((response)=>{
                console.log(response)
                localStorage.setItem('currentUser', JSON.stringify(response.data))
                dispatch({
                    type: CHECKIN,
                    payload:response.data
                })
                // userId=response.data.userId
                // history.push('/approve1')
            })
           .catch((err)=>{
            console.log(err)
           })
        }
    }
}
//check the telegram code
export function checkCode(code,id){
    return function(dispatch){
        if(code.length>5){
            axios.post('/api/check',{
                code: code,
                id:id})
            .then((response)=>{
                console.log(response.data)
                dispatch({
                    type: CHECKCODE,
                    payload:response.data
                })
                // history.push('/approve2')
            })
           .catch((err)=>{
            console.log(err)
           })
        }
    }
}
//SENT IMAGE TO THE SERVER
export function sentImage(image,id){
    return function(dispatch){
            axios.post('/api/check',{
                photo: image,
                id:id})
            .then((response)=>{
                console.log(response.data,"action")
                dispatch({
                    type: SENTIMAGE,
                    payload:response.data
                })
            })
           .catch((err)=>{
            console.log(err)
           })
        
    }
}
//send negative answer to admin and make checkin
export function sentQuestions(questions,id){
    return function(dispatch){
            axios.post('/api/check',{
                id: id,
                report: questions})
            .then((response)=>{
                console.log(response.data,"action")
                dispatch({
                    type: SENTNEGATIVEQUESTION,
                    payload:response.data
                })
                history.push('/main')
            })
           .catch((err)=>{
            console.log(err)
           })
        
    }
}
//checkout user and send report and insight
export function checkout(report,id){
    return function(dispatch){
            axios.post('/api/check',{
                id: id,
                report: report})
            .then((response)=>{
                console.log(report, 'rep from action')
                dispatch({
                    type: CHECKOUT,
                    payload:response
                })
                history.push('/main')
            })
           .catch((err)=>{
            console.log(err)
           })
        
    }
}


