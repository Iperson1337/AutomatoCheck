import {
	ShowAddDepartment,
	SHOWDEPARTMENTLIST,
	SHOWADDPOSITION,
	SHOWEDITEMPLOYEE
} from '../constants/ShowModal'

import createHistory from 'history/createBrowserHistory'
const history = createHistory({forceRefresh: true})

export function showDepartmentModal (showAddDepartment) {
	return function (dispatch) {
		dispatch({
			type: ShowAddDepartment,
			payload: showAddDepartment
		})
	}
}
export function showDepartmentList (showDepartmentList) {
	return function (dispatch) {
		dispatch({
			type: SHOWDEPARTMENTLIST,
			payload: showDepartmentList
		})
	}
}
export function showAddPosition (showAddPosition) {
	return function (dispatch) {
		dispatch({
			type: SHOWADDPOSITION,
			payload: showAddPosition
		})
	}
}
export function showEditEmployee(showEditEmployee) {
	return function (dispatch) {
		dispatch({
			type: SHOWEDITEMPLOYEE,
			payload: showEditEmployee
		})
	}
}

