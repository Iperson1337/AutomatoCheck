import React, { Component } from 'react'

import Form from '../components/Signup/Form'
import Footer from '../components/Footer/Footer.js'

class Signup extends Component {
  constructor (props) {
    super(props)
  }
  componentWillMount () {
    // axios.defaults.headers.common['Authorization'] = localStorage.getItem('token')
    // this.props.authActions.me()
  }

  render () {
    return (
      <section>
        <div className='w_100 h_100 signup'>
          <Form />
          <Footer />
        </div>
      </section>
    )
  }
}

export default Signup
