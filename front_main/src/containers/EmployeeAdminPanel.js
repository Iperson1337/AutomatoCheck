import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'
import Header from '../components/Header/Header.js'
import SearchEmployee from '../components/Atoms/SearchEmployee'
import AdminEmployeeInfo from '../components/Atoms/AdminEmployeeInfo'
import AdminTimeCounter from '../components/Atoms/AdminTimeCounter'
import Tasks from '../components/Atoms/Tasks.js';
import Sidebar from '../components/Atoms/Sidebar.js';
import { connect } from 'react-redux'
import EditEmployee from '../components/Modals/EditEmployee.js';

class Main extends Component {
  constructor(props){
    super(props)
    this.state = {
    }
  }

  render () {
    return (
      <section className='h-100vh overflowyhidden'>
        <Header />
        <div className='flex'>
            <SearchEmployee />
            <div className='flex fl-dc'>
                <AdminTimeCounter />
                <div className='flex'>
                <AdminEmployeeInfo/>
                <Tasks/>
                </div>
            </div>
            <Sidebar />
            
            {this.props.ModalShow.showEditEmployee ?
             <EditEmployee />
            : null
            }
        </div>
      </section>
    )
  }
}

function modalShow(state) {
  return {
      ModalShow: state.ModalShow
  }
}

export default connect(modalShow)(Main)