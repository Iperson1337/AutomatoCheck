import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'
import ReactSpeedometer from "react-d3-speedometer";

class Draft extends Component {
  constructor (props) {
    super(props)
  }
  componentWillMount () {
    // axios.defaults.headers.common['Authorization'] = localStorage.getItem('token')
    // this.props.authActions.me()
  }

  render () {
    return (
      <section>
        <ReactSpeedometer 
        ringWidth='2'
        startColor='#D9F5F6'
        endColor='#465B69'
        maxValue={500}
        value={473}
        />
        <img src='images/speedometer-bottom.png' className='pos-rel w-14rem top--261'/>
        <p className='opacity013 fs-38w600 color00fff9 top--321 pos-rel'>473 min</p>
        
      </section>
    )
  }
}

export default Draft
