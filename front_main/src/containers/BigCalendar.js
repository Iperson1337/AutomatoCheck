import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'
import Calendar from '../components/Atoms/Calendar.js'
import AdminHeader from '../components/Header/AdminHeader.js'


class BigCalendar extends Component {
  constructor(props){
    super(props)
    this.state = {
 
    }
  }

  render () {
    return (
      <section className='h-100vh overflowyhidden'>
          <AdminHeader />
          <Calendar />
      </section>
    )
  }
}

export default BigCalendar