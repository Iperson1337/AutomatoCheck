import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'
import Header from '../components/Header/Header.js'
import DepAndPos from '../components/Department/DepAndPos.js'
import AddDepartment from '../components/Modals/AddDepartment.js';
import DepartmentList from '../components/Modals/DepartmentList.js';
import { connect } from 'react-redux'
import AddPosition from '../components/Modals/AddPosition.js';

class DepartmentAndPosition extends Component {
  constructor(props){
    super(props)
    this.state = {
 
    }
  }

  render () {
    return (
      <section>
        <Header />
        <DepAndPos />
        {this.props.ModalShow.showAddDepartment ?
        <AddDepartment/>
        : null
        }
        {this.props.ModalShow.showDepartmentList ?
        <DepartmentList/>
        : null
        }
        {this.props.ModalShow.showAddPosition ?
        <AddPosition/>
        : null
        }
      </section>
    )
  }
}

function modalShow(state) {
    return {
        ModalShow: state.ModalShow
    }
  }
  
  export default connect(modalShow)(DepartmentAndPosition)