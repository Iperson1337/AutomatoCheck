import React, { Component } from 'react'
import CodeApprove from '../components/Approve/CodeApprove.js'
import CheckoutCode from '../components/Approve/CheckoutCode.js'
import CheckoutReport from '../components/Approve/CheckoutReport.js' 
import CheckoutInsight from '../components/Approve/CheckoutInsight.js'
import Footer from '../components/Footer/Footer.js'
import WebcamCapture from '../components/Webcam/WebcamCapture.js'
import Question1 from '../components/SignUpQuestions/Question1.js'
import Form from '../components/Signup/Form.js';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as checkin  from '../actions/CheckinAction'

class Approve extends Component {
  constructor (props) {
    super(props)
    this.state = {
      id:'',
      report:'',
      checkout:false
    }
    this.handleChange=this.handleChange.bind(this)
    this.checkCode=this.checkCode.bind(this)
    this.sentImage=this.sentImage.bind(this)
    this.onClickNext=this.onClickNext.bind(this)
    this.checkoutReport=this.checkoutReport.bind(this)
    this.checkout=this.checkout.bind(this)
  }
 handleChange(e){
   this.setState({
     id:e.target.value
   })
     this.props.checkin.checkin(e.target.value)
   }
  onClickNext(){
    if( this.props.Checkin.currentUser.success==true){
      localStorage.setItem('userId', JSON.stringify(this.state.id))
    }
  }
  checkCode(e){
      this.props.checkin.checkCode(e.target.value,this.state.id)
  }
  sentImage(image){
    this.props.checkin.sentImage(image,this.state.id)
  }
   checkoutReport(notice){
     this.setState({
      report: this.state.report + '\n' + notice 
    })
  }
  async checkout(notice){
   await  this.setState({
     report: this.state.report + '\n' +notice 
   })
   this.props.checkin.checkout(this.state.report, this.state.id)
 }
 render () {
    return (
      <section>
        <div className='w_100 h_100 signup'>
        {this.props.match.params.params=='login' ? 
          <Form 
            checkId={this.handleChange} 
            success={this.props.Checkin.currentUser.success}
            currentUser={this.props.Checkin.currentUser}
            onClick={this.onClickNext}
          /> 
        : null}
        {this.props.match.params.params==1 ? 
          <CodeApprove 
            currentUser={this.props.Checkin.currentUser}
            checkCode={this.checkCode }
            code={this.props.Checkin.code}
            success={this.props.Checkin.code.success}
          />
        
        : null}
        {this.props.match.params.params=='checkout' ? 
          <CheckoutCode
            currentUser={this.props.Checkin.currentUser}
            checkCode={this.checkCode }
            code={this.props.Checkin.code}
            success={this.props.Checkin.code.success}
          />
        
        : null}
        {this.props.match.params.params=='report' ? 
          <CheckoutReport
            checkoutReport={this.checkoutReport}
            />
        
        : null}
        {this.props.match.params.params=='insight' ? 
          <CheckoutInsight
            checkoutReport={this.checkoutReport}
            // checkout={this.state.checkout}
            // id={this.state.id}
            checkout={this.checkout}
            />
        
        : null}
        {this.props.match.params.params==2 ? 
          <WebcamCapture 
            sentImage={this.sentImage}
            code={this.props.Checkin.code}
            /> 
        : null}
        {this.props.match.params.params==3 ? <Question1 
          id={this.state.id}
        />
         : null}
        <Footer />
        </div>
      </section>
    )
  }
}
function mapStateToProps (state) {
  return {
    Checkin: state.Checkin
  }
}
function mapDispatchToProps (dispatch) {
  return {
    checkin: bindActionCreators(checkin, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Approve)
