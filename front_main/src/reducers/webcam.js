import {SAVEIMAGE} from '../constants/Webcam'

const initialState = {
	image: ''
}

export default function webcam(state = initialState, action) {

	switch (action.type) {
		case SAVEIMAGE:
        return {
            ...state,
            image: action.payload,
		}
		default:
			return state
	}

}
