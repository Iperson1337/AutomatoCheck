import {ShowAddDepartment, SHOWDEPARTMENTLIST,SHOWADDPOSITION, SHOWEDITEMPLOYEE} from '../constants/ShowModal'

const initialState = {
	showAddDepartment:false,
	showDepartmentList: false,
	showAddPosition: false,
	showEditEmployee: false

}

export default function modalShow(state = initialState, action) {

	switch (action.type) {
		case ShowAddDepartment:
        return {
            ...state,
            showAddDepartment: action.payload,
		}
		case SHOWDEPARTMENTLIST:
        return {
            ...state,
            showDepartmentList: action.payload,
		}
		case SHOWADDPOSITION:
        return {
            ...state,
            showAddPosition: action.payload,
		}
		case SHOWEDITEMPLOYEE:
        return {
            ...state,
            showEditEmployee: action.payload,
        }
		default:
			return state
	}

}
