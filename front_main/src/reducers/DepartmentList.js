import {
    DEPARTMENTLIST,
    ADDDEPARTMENT,
    CHECKDEPARTMENT,
    DELETEDEPARTMENT
} from '../constants/DepartmentList'
  const initialState = {
    departments:[],
    checkedDep:[],
    deletedDep:[],
    addDep:[]
  };

  export default function DepartmentList (state = initialState, action){

	switch (action.type) {
//вытаскивает данные с базы
        case DEPARTMENTLIST:
            return {
                ...state,
                departments: action.payload
            }
        case CHECKDEPARTMENT:
        return {
            ...state,
            checkedDep: action.payload
        }
        case DELETEDEPARTMENT:
        return {
            ...state,
            deletedDep: action.payload
        }
        case ADDDEPARTMENT:
        return {
            ...state,
            addDep: action.payload
        }
		default:
			return state
	}

}