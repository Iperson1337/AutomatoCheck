import {
    ADMINEMPLOYEEINFO
} from '../constants/AdminEmployeeInfo'

const initialState = {
    employee: [],
}

export default function AdminEmployeeInfo (state=initialState, action){
    switch(action.type){
        case ADMINEMPLOYEEINFO:
            return{
                ...state,
                employee: action.payload
            }
            
        default:
        return state
    }
}