# Сотрудники
- [Все сотрудники](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D0%B2%D1%81%D0%B5-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B8)
- [Просмотр сотрудника](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D0%BF%D1%80%D0%BE%D1%81%D0%BC%D0%BE%D1%82%D1%80-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B0)
- [Редактировать сотрудника](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D1%80%D0%B5%D0%B4%D0%B0%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B0)
- [Уволить сотрудника](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D1%83%D0%B2%D0%BE%D0%BB%D0%B8%D1%82%D1%8C-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B0)
- [Количества сотрудников](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D0%BA%D0%BE%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%B0-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2)
- [Сотрудники в Офисе](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B8-%D0%B2-%D0%BE%D1%84%D0%B8%D1%81%D0%B5)
- [Сотрудники вне офиса](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%B8-%D0%B2%D0%BD%D0%B5-%D0%BE%D1%84%D0%B8%D1%81%D0%B0)
- [Cтатистика месяца](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#c%D1%82%D0%B0%D1%82%D0%B8%D1%81%D1%82%D0%B8%D0%BA%D0%B0-%D0%BC%D0%B5%D1%81%D1%8F%D1%86%D0%B0)
- [Круговая диаграмма](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/employee.md#%D0%BA%D1%80%D1%83%D0%B3%D0%BE%D0%B2%D0%B0%D1%8F-%D0%B4%D0%B8%D0%B0%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0)

##  Просмотр сотрудника
```GET /api/employees/:id/show``` <br/>
Принимает параметры
- ``:id``  --- _ID сотрудника
- ``selectedMonth`` --- выбранный месяц (необязательное поле)

**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:
```    
"employee": {
        "firstname": "Имя",
        "lastname": "Фамилия",
        "fathername": "Отчество",
        "disabled": false,
        "competencies": [],
        "type": "fixed",
        "fixT": 420,
        "fixST": 300,
        "bonusPercent": 75,
        "checked": false,
        "holidays": [
            {
                "year": 2018,
                "month": 4,
                "days": [
                    1,
                    7,
                    8,
                    9,
                    12
                ]
            }
        ],
        "admin": true,
        "confirmed": false,
        "days": [
			            {
                "year": 2018,
                "month": 0,
                "day": 18,
                "check": [
                    [
                        "2018-01-18T06:18:17.893Z",
                        "2018-01-18T12:00:00.066Z"
                    ]
                ]
            },
            {
                "year": 2018,
                "month": 0,
                "day": 20,
                "check": [
                    [
                        "2018-01-20T10:51:25.454Z",
                        "2018-01-20T13:11:37.934Z"
                    ]
                ]
            }
		],
        "salaryOfMonth": [],
        "_id": "59e70a01049678483c40cb7a",
        "updated": "2018-07-31T10:08:56.887Z",
        "employee_id": "10IT01",
        "botId": "78923920",
        "department": "59d8c3b69e19530d2037cb0f",
        "department_id": "it",
        "vacancy": "Web",
        "salary_fixed": 52500,
        "salaryFull": 70000,
        "registered_at": "2017-10-18T08:00:01.874Z",
        "__v": 20,
        "tgname": "telegram_username"
    },
    "salary": {
        "bonus": 52500,
        "fix": 17500,
        "monthHrs": 174,
        "monthMins": 10440,
        "totalMonthMin": 276,
        "totalMinsOver": 0,
        "salMonth": 17500,
        "trueFix": 462.64367816091954,
        "salPerMin": 1.6762452107279693,
        "byDay": [
            {
                "workedMins": 276,
                "day": 1,
                "month": 7,
                "year": 2018
            }
        ],
        "nedorabotal": 10164
    },
    "department_name": "Департамент программной разработки",
    "reports": [],
    "success": true
```

## Редактировать сотрудника

``POST /api/admin/employee/update`` <br/>
Принимет массив
```
editedEmployee:{
     firstname,
     lastname,
     fathername,
     salaryFull,
     salary_fixed,
     vacancy,
     department,
     fixT,
     fixST,
     gender
}
```
**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:
```
success: true,
message: "Ваши изменения сохранены!"
```
## Все сотрудники

``GET /api/admin/employees/all`` <br/>
Ничего не принимает

**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:

    "employees": [
        {
	        "firstname": "Имя",
	        "lastname": "Фамилия",
	        "fathername": "Отчество",
	        "disabled": false,
	        "competencies": [],
	        "type": "fixed",
	        "fixT": 420,
	        "fixST": 300,
	        "bonusPercent": 75,
	        "checked": false,
	        "holidays": [
	            {
	                "year": 2018,
	                "month": 4,
	                "days": [
	                    1,
	                    7,
	                    8,
	                    9,
	                    12
	                ]
	            }
	        ],
	        "admin": true,
	        "confirmed": false,
	        "days": [
				            {
	                "year": 2018,
	                "month": 0,
	                "day": 18,
	                "check": [
	                    [
	                        "2018-01-18T06:18:17.893Z",
	                        "2018-01-18T12:00:00.066Z"
	                    ]
	                ]
	            },
	            {
	                "year": 2018,
	                "month": 0,
	                "day": 20,
	                "check": [
	                    [
	                        "2018-01-20T10:51:25.454Z",
	                        "2018-01-20T13:11:37.934Z"
	                    ]
	                ]
	            }
			],
	        "salaryOfMonth": [],
	        "_id": "59e70a01049678483c40cb7a",
	        "updated": "2018-07-31T10:08:56.887Z",
	        "employee_id": "10IT01",
	        "botId": "78923920",
	        "department": "59d8c3b69e19530d2037cb0f",
	        "department_id": "it",
	        "vacancy": "Web",
	        "salary_fixed": 52500,
	        "salaryFull": 70000,
	        "registered_at": "2017-10-18T08:00:01.874Z",
	        "__v": 20,
	        "tgname": "telegram_username"
	    },
	    {...},
	    {...},
	    {...},
	    {...}
    ],
    "success": true

## Уволить сотрудника

``POST /api/admin/employee/disabled``

Принимает параметры
-   `id`  — _ID сотрудника

**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:
```
success: true,
message: "Вы успешно удалили сотрудника!"
```

## Количества сотрудников

``GET /api/admin/employees/count``

**Ответ**
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:
```
    "number": 22,
    "success": true
```

## Сотрудники в Офисе

``GET /api/admin/employees/office/in`` <br/>
**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:
```
"employees": [
        {
	        "firstname": "Имя",
	        "lastname": "Фамилия",
	        "fathername": "Отчество",
	        "disabled": false,
	        "competencies": [],
	        "type": "fixed",
	        "fixT": 420,
	        "fixST": 300,
	        "bonusPercent": 75,
	        "checked": true,
	        "holidays": [
	            {
	                "year": 2018,
	                "month": 4,
	                "days": [
	                    1,
	                    7,
	                    8,
	                    9,
	                    12
	                ]
	            }
	        ],
	        "admin": true,
	        "confirmed": false,
	        "days": [
				            {
	                "year": 2018,
	                "month": 0,
	                "day": 18,
	                "check": [
	                    [
	                        "2018-01-18T06:18:17.893Z",
	                        "2018-01-18T12:00:00.066Z"
	                    ]
	                ]
	            },
	            {
	                "year": 2018,
	                "month": 0,
	                "day": 20,
	                "check": [
	                    [
	                        "2018-01-20T10:51:25.454Z",
	                        "2018-01-20T13:11:37.934Z"
	                    ]
	                ]
	            }
			],
	        "salaryOfMonth": [],
	        "_id": "59e70a01049678483c40cb7a",
	        "updated": "2018-07-31T10:08:56.887Z",
	        "employee_id": "10IT01",
	        "botId": "78923920",
	        "department": "59d8c3b69e19530d2037cb0f",
	        "department_id": "it",
	        "vacancy": "Web",
	        "salary_fixed": 52500,
	        "salaryFull": 70000,
	        "registered_at": "2017-10-18T08:00:01.874Z",
	        "__v": 20,
	        "tgname": "telegram_username"
	    },
	    {...},
	    {...},
	    {...},
	    {...}
    ],
    "success": true
```
## Сотрудники вне офиса

``GET /api/admin/employees/office/outside`` <br/>
**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:
```
"employees": [
        {
	        "firstname": "Имя",
	        "lastname": "Фамилия",
	        "fathername": "Отчество",
	        "disabled": false,
	        "competencies": [],
	        "type": "fixed",
	        "fixT": 420,
	        "fixST": 300,
	        "bonusPercent": 75,
	        "checked": false,
	        "holidays": [
	            {
	                "year": 2018,
	                "month": 4,
	                "days": [
	                    1,
	                    7,
	                    8,
	                    9,
	                    12
	                ]
	            }
	        ],
	        "admin": true,
	        "confirmed": false,
	        "days": [
				            {
	                "year": 2018,
	                "month": 0,
	                "day": 18,
	                "check": [
	                    [
	                        "2018-01-18T06:18:17.893Z",
	                        "2018-01-18T12:00:00.066Z"
	                    ]
	                ]
	            },
	            {
	                "year": 2018,
	                "month": 0,
	                "day": 20,
	                "check": [
	                    [
	                        "2018-01-20T10:51:25.454Z",
	                        "2018-01-20T13:11:37.934Z"
	                    ]
	                ]
	            }
			],
	        "salaryOfMonth": [],
	        "_id": "59e70a01049678483c40cb7a",
	        "updated": "2018-07-31T10:08:56.887Z",
	        "employee_id": "10IT01",
	        "botId": "78923920",
	        "department": "59d8c3b69e19530d2037cb0f",
	        "department_id": "it",
	        "vacancy": "Web",
	        "salary_fixed": 52500,
	        "salaryFull": 70000,
	        "registered_at": "2017-10-18T08:00:01.874Z",
	        "__v": 20,
	        "tgname": "telegram_username"
	    },
	    {...},
	    {...},
	    {...},
	    {...}
    ],
    "success": true
```

## Cтатистика месяца

``GET /api/admin/employees/statistics/month``
**Ответ**
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:
```
 "minutes": 491,
 "i": 1,
 "salary": [
     {
         "day": 1,
         "sal": 823.036398467433,
         "employees": 0,
         "count": null
     }
 ]
```

## Круговая диаграмма

``GET /api/admin/employees/statistics/piechart`` <br/>

**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:
```
  "dorabotaly": 0,
  "nedorabotaly": 1,
  "nebyli": 21
```


## Добавить выходные
```POST /api/admin/employees/add/holidays``` <br/>
Принимает <br/>
```

```
