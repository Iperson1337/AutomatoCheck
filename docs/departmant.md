# Департаменты
- [Все департаменты](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/departmant.md#%D0%B2%D1%81%D0%B5-%D0%B4%D0%B5%D0%BF%D0%B0%D1%80%D1%82%D0%B0%D0%BC%D0%B5%D0%BD%D1%82%D1%8B)
- [Количество департаментов](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/departmant.md#%D0%BA%D0%BE%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%BE-%D0%B4%D0%B5%D0%BF%D0%B0%D1%80%D1%82%D0%B0%D0%BC%D0%B5%D0%BD%D1%82%D0%BE%D0%B2)
- [Добавить департамент](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/departmant.md#%D0%B4%D0%BE%D0%B1%D0%B0%D0%B2%D0%B8%D1%82%D1%8C-%D0%B4%D0%B5%D0%BF%D0%B0%D1%80%D1%82%D0%B0%D0%BC%D0%B5%D0%BD%D1%82)
- [Удалить департамент](https://gitlab.com/Iperson1337/AutomatoCheck/blob/master/docs/departmant.md#%D1%83%D0%B4%D0%B0%D0%BB%D0%B8%D1%82%D1%8C-%D0%B4%D0%B5%D0%BF%D0%B0%D1%80%D1%82%D0%B0%D0%BC%D0%B5%D0%BD%D1%82)

##  Все департаменты
```GET /api/admin/departments```
**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:

```
    "departments": [
        {
            "employees": [
                "59e5e8a7cf378151339a28b6",
                "59f0d49e3009ce443cc96506",
                "59f0d80d3009ce443cc96508",
                "5a0d610e8f51da7359666ec4",
                "5a1fb5d0c97069726625a0ba",
                "5a1fcba6c97069726625a0bc"
            ],
            "_id": "59d8c3b69e19530d2037cb11",
            "name": "Департамент контент-маркетинга",
            "short_name": "cw",
            "__v": 7
        },
        {
            "employees": [
                "59e5ef14cf378151339a28b8",
                "59ea0304e3f939542d711fa5",
                "5a12f1708f51da7359666ed8",
            ],
            "_id": "59d8c3b69e19530d2037cb10",
            "name": "Департамент дизайна",
            "short_name": "ds",
            "__v": 18
        },
        {
            "employees": [
                "59e4b359af1aa74e620ee450",
            ],
            "_id": "59d8c3b69e19530d2037cb0f",
            "name": "Департамент программной разработки",
            "short_name": "it",
            "__v": 63
        }
    ],
    "success": true
```

##  Количество департаментов
```GET /api/admin/departments/count``` <br/>

**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело:
```
"number": 3,
"success": true
```



##  Добавить департамент
```POST /api/admin/departments/add```<br/>
Принимает обязательные параметры <br/>
- `name`  —  Названия департамента
-  `short_name`  — Короткое названия департамента <br/>

**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело: <br/>
```
 "success": true
 ```
При ошибке :
```
"success": false
```


##  Удалить департамент
```POST /api/admin/departments/delete ``` <br/>
Принимает обязательное параметр
- `short_name`  — Короткое названия департамента <br/>

**Ответ** <br/>
Успешный ответ приходит с кодом  `200 OK`  и содержит тело: <br/>
```
"success": true
```
При ошибке: <br/>
```
"success": false
```
