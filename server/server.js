const express = require('express');
const app = express();
const mongoose = require('mongoose');
const https = require('https');
const http = require('http');
const server = http.Server(app);
const bodyParser = require('body-parser');
const fs = require('fs');
const path =require('path');
const config = require('./config')
const io = require('socket.io')(server);

mongoose.connect(config.mongodb_uri);
var dbc = mongoose.connection;
dbc.once('open', function() {
  console.log('Mongoose Connected');
});

//CRON
require('./app/Schedule')

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(express.static('static/'))

app.use('/api', require('./routes/api'))
app.use('*', express.static('static/index.html'))
app.get('/style.css', function(req, res) {
  res.sendFile('style.css', { root: path.join(__dirname, '../static') });
})
app.get('*', function(req, res){
  res.sendFile('index.html', { root: path.join(__dirname, '../static') });
})

app.use(function(err, req, res, next){
  console.error(err.stack)
  res.status(500).send({ message: err.message })
})


io.on('connection', function(socket){
  console.log('socket connected');
});
// Exporting server
// export default { app }

app.listen(config.port, function() {
  console.log('Server running on '+config.port);
})



module.exports = app
