require('dotenv').config()

module.exports = {
  port: process.env.PORT,
  mongodb_uri: process.env.MONGO_DB_URI,
  telegram_token: process.env.TELEGRAM_BOT_API,
  jwt: {
    secret: process.env.JWT_SECRET,
    privateKey: process.env.PRIVATE_KEY,
    tokenExpiry: process.env.TOKEN_EXPIRY
  }

}
