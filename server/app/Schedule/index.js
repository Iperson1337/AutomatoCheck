const cron = require('node-cron');
const {
  autoCheckAlert,
  autoCheckOut,
  autoReminder,
  autoCheckIn,
  dayEndCheckOut
} = require('../Repository/check')

console.log('Cron launched');

var aAlertTime = '50 23'//'50 23 * * *';
var aCheckOutTime = '59 23'//'59 23 * * *';
var aCheckInTime = '0 0'//'0 0 * * *';
var aReminderTime1= '0 16' // 23 13 * * *;
var aReminderTime2= '0 13' // 20 20 * * *;

cron.schedule(aAlertTime+' * * *', function(){ // 50 23
  console.log('AutoCheckAlert!');
  autoCheckAlert();
});

cron.schedule(aCheckOutTime+' * * *', function(){// 55 23
  console.log('AutoCheckOut');
  autoCheckOut();
});

cron.schedule(aReminderTime1+' * * *', function(){// 23 13
  console.log('Reminder 1');
  autoReminder();
});

cron.schedule(aReminderTime2+' * * *', function(){// 20 20
  console.log('Reminder 2');
  autoReminder();
});

cron.schedule(aCheckInTime+' * * *', function(){// 0 0
  console.log('AutoCheckIn');
  autoCheckIn();
});

cron.schedule('*/1 * * * *', function(){// 0 0
  console.log('dayEndCheckOut');
  dayEndCheckOut((new Date().getHours()+':'+new Date().getMinutes()))
});
