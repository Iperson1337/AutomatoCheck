const Employee = require('../Models/Employee');
const {matchInObjArr, minToHour} = require('../../helpers/AllFunctions');
const {SalaryInfo} = require('../../helpers/salary');

// возвращает форматированную инфу по зп (принимает обьект, возвращаем string)
function grabInfo(obj, out, init ) {
  // console.log(obj)
  // тут используется функция fn(_) для формирования минут в читаемую строку
  var n ='\n';
  with (obj) {
    switch (out.toLowerCase()) {
      case 'salary':
        var result = init||"========\n";
        result +=
        'Фикса : '+ fix + 'тг \n'+
        'Бонус : '+ bonus + 'тг \n'+
        'Работал : '+ minToHour(totalMonthMin) + ' из ' + minToHour(monthMins) + ' \n'+ // преобразовать
        'Переработал: '+ minToHour(totalMinsOver) + '\n' +
        'Чисто заработанная фикса : '+ (trueFix*1).toFixed(2) + 'тг. \n'+
        'Стоимость 1 часа : '+ (salPerMin*60).toFixed(2) + ' \n'+
        'Всего работал дней: ' + ( byDay.length ) +'\n'+
        'Итого : '+ (trueFix).toFixed(2) + 'тг. \n'
        break;
      case 'anal':
        var result = init||"========\n";
        result +=
        'Фикса : '+ fix + 'тг \n'+
        'Бонус : '+ bonus + 'тг \n'+
        'Работал : '+ minToHour(totalMonthMin) + ' из ' + minToHour(monthMins) + ' \n'+ // преобразовать
        'Переработал: '+ minToHour(totalMinsOver) + '\n' +
        'Недоработал: '+ minToHour(nedorabotal) +' ('+ (nedorabotal*salPerMin).toFixed(2) + 'тг)\n' +
        'Зп на данный момент : '+ (trueFix*1).toFixed(2) + 'тг. \n'+
        'Стоимость 1 часа : '+ (salPerMin*60).toFixed(2) + ' \n'+
        'Всего работал дней: ' + ( byDay.length ) +'\n'+
        'Итого : '+ (trueFix).toFixed(2) + 'тг. \n'
        break;
        case 'info':
          var result = init||"========\n";
      default:
    }
  }
  return result
}

function getUserInfoNew(Some_id, callback) {
  var query = Some_id ?
  (/^[а-яa-zА-ЯA-Z]+$/i.test(Some_id) ? {firstname: Some_id} : {employee_id: Some_id.toString().toUpperCase()})
  : false;
  Employee.find(query).exec(function (err,users) {
    if(!err){
      if(users){
        callback(null, users)
      } else {
        // callback('Ошибка: '+Some_id+' не найден.\n
        // Запрос: '+ JSON.stringify(query),null)
        callback({
          id: Some_id,
          query
        }, null)
      }
    } else {
      callback(err, null)
    }
  })
}

// просто обёрткая для employee.fondone
function getUserInfo(Some_id, callback) {
  var query = Some_id ?
  (/^([0-9]{8,10})$/i.test(Some_id) ? {botId: Some_id} : {employee_id: Some_id.toString().toUpperCase()})
  : false;
  if(!query) return
  Employee.findOne(query).exec(function (err,user) {
    if(!err){
      if(user){
        callback(null, user)
      } else {
        // callback('Ошибка: '+Some_id+' не найден.\n
        // Запрос: '+ JSON.stringify(query),null)
        callback({
          id: Some_id,
          query
        }, null)
      }
    } else {
      callback(err, null)
    }
  })
}

// получаетинфо по зп
function employeeInfo(Some_id, year, month, callback) {
  // тут он в зависиости от передаваемого параметра Some_id создает строку запроса в бд (если передали "17it01" то ищет по employee_id если 31231241 то по botId)
  var query = Some_id ?
  (/^([0-9]{8,10})$/i.test(Some_id) ?
  {botId: Some_id} :
  {employee_id: Some_id.toString().toUpperCase()})
  : false;
  if(!query) return false
  // ищет чела
  // console.log(query, "query");
  Employee.findOne(query, function (err, emp) {
    if(err){
      console.log("начальный еррор");
      callback(true, null) // no employee
    } else {
      if(emp){
        var inds = matchInObjArr(emp.days, [year,month], ['year', 'month']);
        var newDays = [];
        if(inds.length > 0){
          // берет days за указаный месяц
          inds.forEach(function (i) {
            newDays.push(emp.days[i])
          })
          ////////

          // берет Выходные за указаный месяц
          var h = emp.holidays;
          var currHols = [];
          if(h.length > 0){
            for (let i = 0;i < h.length; i++) {
              if (h[i].month == [month] && h[i].year == [year] ){
                currHols=h[i].days;}
            }
          } else {
             currHols = [];
             console.log('нет выходных');
          }
        // calc
        // console.log(emp.inTime, emp.outTime, emp.SinTime, emp.SoutTime);
        var salary = SalaryInfo(emp.salaryFull, emp.bonusPercent||75,
                                  year, month, emp.fixT*1, emp.fixST*1, currHols,newDays,
                                  emp.inTime||"08:30",emp.outTime||"18:00", 
                                  emp.SinTime||"10:00", emp.SoutTime||"16:00", emp.type
                                )
          callback(null, {employee: emp, salary})
        } else {
          console.log("нету инфо за месяц");
          callback( true, null)} // no month info

      } else {
        console.log("не найден чел");
        callback( true, null) // no employee
      }
    }

  })
}

module.exports.grabInfo = grabInfo;
module.exports.getUserInfoNew = getUserInfoNew;
module.exports.getUserInfo = getUserInfo;
module.exports.employeeInfo = employeeInfo;
