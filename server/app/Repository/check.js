const Employee = require('../Models/Employee');
const adminAccess = require('../Models/adminAccess');
const { bot } = require('../../bot/initBot');
const BotHelper = require('../../bot/helper/BotFunctions');
const { SendMsgToAdmins } = require('./admin');
const fn = require('../../helpers/AllFunctions');
const {SalaryInfo} = require('../../helpers/salary');

var allChecked = [];
var onWork = [];
var msgs = [];

/******************************Cron functions******************************************/
// если человек нажал кнопу ""Я на работе"", то он попадает в список авто чекина
function autoCheckOut() {
  Employee.find({
    disabled: false,
    checked: true
  }, function(err, emp) {
    if (!err && emp.length > 0) {
      emp.forEach(function(e) {
        console.log('checking out ', e.employee_id);
        CheckMe(e.employee_id, false, function(s, m) {
          console.log('checkme called CallBack => ', s, m);
          allChecked.push(e.employee_id);
          // console.log(onWork,onWork.indexOf(e.employee_id), e.employee_id);
          if (onWork.indexOf(e.employee_id) == -1) {
            console.log('i`ll mark', e.employee_id, 'that he was forgot to CheckOut');
            saveNote(e.employee_id)
          }
          // bot.sendMessage(e.botId, 'Был сделан автоматический чекаут. \nЧекин будет сделан ровно в 00:00')
          console.log(e.employee_id, s
            ? 'Successfully Auto-UnChecked'
            : 'ERROR on Auto-UnCheck Hz pochemu :(' + m)
        }, null, true)
      })
    }
  })
}

// предупреждает
function autoCheckAlert() {
  Employee.find({
    disabled: false,
    checked: true
  }, function(err, emp) {
    if (!err && emp.length > 0) {
      emp.forEach(function(e) {
        console.log('Человек с ID [' + e.employee_id + '] Забыл сделать чекаут. Попрошу его об этом.')
        bot.sendMessage(e.botId, 'Пожалуйста, не забудьте сделать чекаут до 23:59!', BotHelper.makeButtons(['Я всё ещё работаю!'], ['StillOnWork|' + e.employee_id + '.' + new Date().getDate()], //12
            1, true)) //then//
      })
    }
  })
}

// Напоминает о trello
function autoReminder() {
  Employee.find({
    disabled: false,
    checked: true
  }, function(err, emp) {
    if(err)
        console.log(err)
    if (!err && emp.length > 0) {
      emp.forEach(function(e) {
        console.log('Напомню человеку с ID [' + e.employee_id + '] что нужно чекнуть trello и pivotal tracker .')
        bot.sendMessage(e.botId, 'Пожалуйста, проверьте ваш Trello и Pivotal Tracker');//.catch(err => console.log(err))
         //then//
      })
    }
  })
}

// авто чекин
function autoCheckIn() {
  console.log('Список всех чекиненых на голову:',allChecked,' Из них всё ещё на работе:', onWork)
  if(allChecked.length > 0){
    allChecked.forEach(function (chid) {
      if(onWork.indexOf(chid)>=0){
        isChecked(chid, function (err,ch) {
          if(!err){
            if(ch.checked==false){
              CheckMe(chid, true, function (s) {
                console.log(chid,(s ? 'Successfully Re-Checked' : 'ERROR on Re-Check Hz pochemu :('))
              },null,true)

            } else {
              CheckMe(chid, false, function (s) {
                console.log('Разчекал задним днём',chid);
              }, fn.daysAgo(fn.nd('23:59'),1),true)
              console.log(chid,'Уже зачекался так что я его расчекаю задним днём')
            }
          } else {
            SendMsgToAdmins(err.msg)
          }
        })
      }
    })
    allChecked = [];
    onWork = []
  }
}

//обнуляет все "предупрежждения" человека
function saveNote(id) {
  Employee.findOne({
    employee_id: id.toUpperCase()
  }, '-__v', function(err, e) {
    if (e) { // человек найден
      console.log('yes u found!');
      var ne = new Date();
      var todayArray = [ne.getFullYear(), ne.getMonth(), ne.getDate()]
      var day = fn.matchInObjArr(e.days, todayArray, ['year', 'month', 'day']);
      if (day.length > 0) {
        if (!e.days[day[0]].notes)
          e.days[day[0]].notes = {}
        //selfGo gone false
        var sg = e.days[day[0]].notes.selfGo;
        if (sg) {
          console.log('==>', sg);
          if (sg[sg.length - 1].length != 0 && !sg[sg.length - 1][1]) {
            // var compare = fn.comp(sg[sg.length-1][0], fn.nd(e.outTime), 'min')
            // if(compare.result )
            console.log('==>', sg.length, sg[sg.length - 1][1]);
            sg[sg.length - 1][1] = sg[sg.length - 1][0] //fn.nd(e.outTime)
          }
        }
        if (e.days[day[0]].notes.gone) {
          e.days[day[0]].notes.gone = false;
        }
        if (e.days[day[0]].notes.lunch) {
          e.days[day[0]].notes.lunch = false;
        }
        e.days[day[0]].notes.zabil = true;
        console.log('Result of scan is ', e.days[day[0]].notes);
        e.markModified('days');
      }
      e.save().catch(function(err, s) {
        if(err) console.log(err);
        if(s){
          console.log(s);
        }
      })
    }
  })
}

bot.on('callback_query', function(msg) { // когда нажимают кнопку "я все еще на работе"
  var re = /^StillOnWork\|(.+)\.(.+)/i
  var result = re.exec(msg.data)
  if (result) {
    if ((new Date().getDate().toString() == result[2].toString()) && onWork.indexOf(result[1]) < 0) {
      onWork.push(result[1])
      console.log('Запушил в Он Ворк ' + result[1], onWork);
      bot.editMessageText('Плодотворной вам работы! \nИ не забывайте про отдых :)', {
        message_id: msg.message.message_id,
        chat_id: msg.message.chat.id
      });
    } else {
      console.log('нажал кнопку слишком поздно')
      bot.editMessageText('Сори, но уже поздно.', {
        message_id: msg.message.message_id,
        chat_id: msg.message.chat.id
      });
    }
  }
})

// смс отправить
function say(id, msg, callback) {
  function f() {}
  bot.sendMessage(id, msg || '...').then(callback || f).catch(callback || f)
}

// авто чекаут в конце РАБОЧЕГО дня
function dayEndCheckOut(time) {
  Employee.find({
    disabled: false,
    checked: true
  }, '-__v', function(err, emp) {
    // console.log(emp);
    if (!err && emp.length > 0) {
      var t = time.split(':')
      time = ((t[0] * 1 < 10)
        ? ('0' + t[0] * 1)
        : t[0]) + ':' + ((t[1] * 1 < 10)
        ? ('0' + t[1] * 1)
        : t[1])
      emp.forEach(function(e) {
        // console.log(e);
        var ne = new Date();
        var h = fn.matchInObjArr(e.holidays || [], [ne.getFullYear(), ne.getMonth()], ['year', 'month'])
        if ((h || h.length > 0) && e.holidays[h[0]]) {
          var currHols = [...(e.holidays[h[0]].days || [])];
        } else {
          var currHols = []
        }
        var thisDay = new Date().getDay();
        var weekEnd = (e.fixST == 0)
          ? ((thisDay == 6)
            ? true
            : ((thisDay == 0 || currHols.includes(ne.getDate()))
              ? true
              : false))
          : (thisDay == 0 || currHols.includes(ne.getDate()))
            ? true
            : false;
            if(weekEnd) console.log('today is weekend for ', e.employee_id);
        var type = e.type ? e.type.toLowerCase() : (e.tarif ? e.tarif.toLowerCase() : 'fixed');

        if (type!='free'&&!weekEnd && (e.outTime || (e.SoutTime
          ? (e.SoutTime)
          : true))) {
            // console.log(e.SoutTime, e.outTime);
          t = (thisDay == 6)
            ? ((e.SoutTime||"16:00").split(':'))
            : ((e.outTime||"18:00").split(':'))
          var outTime = ((t[0] * 1 < 10)
            ? ('0' + t[0] * 1)
            : t[0]) + ':' + ((t[1] * 1 < 10)
            ? ('0' + t[1] * 1)
            : t[1])
          // console.log(outTime, time);

          if (outTime == time) {
            // время совпало
            CheckMe(e.employee_id, false, function(s, m) {
              Employee.findOne({
                employee_id: e.employee_id
              }, '-__v', function(error, empl) {
                if (error)
                  console.log(error);
                if (empl) {

                  var ne = new Date();
                  var todayArray = [ne.getFullYear(), ne.getMonth(), ne.getDate()]
                  var day = fn.matchInObjArr(empl.days, todayArray, ['year', 'month', 'day']);
                  if (day.length > 0) {
                    var thisDay = empl.days[day[0]]
                    if (!thisDay.notes)
                      thisDay.notes = {}
                    //selfGo gone false
                    var sg = thisDay.notes.selfGo;
                    if (sg) {
                      if (sg[sg.length - 1].length != 0 && !sg[sg.length - 1][1]) {
                        // var compare = fn.comp(sg[sg.length-1][0], fn.nd(empl.outTime), 'min')
                        // if(comparempl.result )
                        sg[sg.length - 1][1] = sg[sg.length - 1][0] //fn.nd(empl.outTime)
                      }
                    }
                    if (thisDay.notes.gone) {
                      thisDay.notes.gone = false;
                    }
                    if (thisDay.notes.lunch) {
                      thisDay.notes.lunch = false;
                    }
                    empl.markModified('days');
                    empl.save().catch(function(err, s) {
                      console.log(err, s);
                    })
                    // console.log(empl.holidays);
                    /// cur hols
                    console.log(thisDay);
                    var salary = SalaryInfo(empl.salaryFull || empl.salary_fixed, empl.bonusPercent || 0,
                      ne.getFullYear(), ne.getMonth(), empl.fixT, empl.fixST, currHols, [thisDay],
                      empl.inTime || "08:30", empl.outTime || "18:00",
                      empl.SinTime || "10:00", empl.SoutTime || "16:00"
                    )
                    console.log(salary);

                    ////// formatting
                    // console.log(salary.byDay);
                    var byDay = fn.matchInObjArr(salary.byDay, todayArray, ['year', 'month', 'day'])
                    var result = "==Сегодня ты отлично постарался!==\n";
                    console.log(salary.byDay[byDay[0]]);
                    with (salary.byDay[byDay[0]]) {
                      result += 'Работал : ' + fm(workTime) + ' \n' + 'Осталось  : ' + fm(todayMustWork - workTime) + ' из ' + fm(todayMustWork) + ' \n' + 'Отработал сегодня : ' + fm(Fulfilled) + '\n=======\n' + 'Пришел раньше на : ' + fm(beforeWorkTime) + '\n' + 'Опоздал на : ' + fm(late) + '\n' +
                      // 'Ушел позже на : '+ fm(afterWorkTime) + '\n' +//' ('+(finalBonusSal*1).toFixed(2)+'тг.) \n======\n'+
                      'Недоработал : ' + fm(nedor) + '\n================\n'
                      // 'Зп за сегодняшний день : '+ ( trueFix + ((Fulfilled + Dorabotal) * salPerMin) + finalBonusSal ).toFixed(2) + 'тг. \n'

                    }

                    //// end formatting

                    say(empl.botId, 'Уважаемый(ая) ' + empl.firstname + '! \nБыл сделан автоматический чекаут!\n' /*+ result*/ + 'Если у тебя есть желание работать дальше, \nто сделай пожалуйста чекин!')
                  }
                  console.log(empl.employee_id, s
                    ? 'Successfully Auto-UnChecked'
                    : 'ERROR on Auto-UnCheck :(' + m);
                  SendMsgToAdmins(empl.firstname+' '+empl.lastname+ ' (' +empl.employee_id +') - автоматический чекаут');
                  /////
                }
              })
            }, null)
          } //время совпало
        }
      })
    }
  })
}

/****************************************Check Helpers*************************************************/
// чекинит человека (id, true(чекИН), callback, date, false(авто чекин/чекаут или человек сам делает))
function CheckMe(id, In, cb, date, auto) {
  console.log('Checking.. [', id, ' In? >', In, date, auto);
  Employee.findOne({
    employee_id: id.toUpperCase()
  }, '-__v', function(error, emp) {
    var chDate = (date || new Date());
    if (date)
      console.log('Check Date =>', date.toString());

    if (emp) {
      console.log('Found! He is ', emp.checked
        ? ''
        : 'not', 'checked and he gonna ', In
        ? 'Check IN!'
        : 'Check OUT!');
      var curr = {
        y: chDate.getFullYear(),
        m: chDate.getMonth(),
        d: chDate.getDate()
      }

      var di = -1;
      var Day = fn.matchInObjArr(emp.days, [ curr.y, curr.m, curr.d ], ['year', 'month', 'day']);
      if (Day.length == 0) {
        emp.days.push({year: curr.y, month: curr.m, day: curr.d, check: []})
        di = emp.days.length - 1;
      } else {
        di = Day[Day.length - 1];
      }
      console.log('Current day is ', curr, '[di is', di, '] Found Days length is ', Day.length);

      if (In) {
        emp.checked = true;
        emp.checkedIn = chDate;
        if (!emp.days[di].check)
          emp.days[di].check = [];
        console.log('===IN=CHECK===', emp.days[di].check)
        emp.days[di].check = checkNext(emp.days[di].check, chDate, In);
        console.log('===IN=CHECK===', emp.days[di].check)
      } else {
        if (!auto && onWork.indexOf(emp.employee_id) > -1) { //chDate.getHours() == 23 && chDate.getMinutes() >= 50 &&
          onWork.splice(onWork.indexOf(emp.employee_id), 1);
          bot.sendMessage(emp.botId, 'Так как вы сделали чекаут раньше 00:00, автоматического чекина не будет.');
        }
        emp.checked = false;
        emp.checkedOut = chDate;

        console.log('===OUT=CHECK===', emp.days[di].check);
        emp.days[di].check = checkNext(emp.days[di].check, chDate, In);
        console.log('===OUT=CHECK===', emp.days[di].check);
      }
      emp.markModified('days'); /////////////////  КАКАЯ ПОЛЕЗНАЯ ВЕЩЬ!!!!!!!!!!!!!!!!!
      emp.save().then(function(a) {
        if (auto){
          bot.sendMessage(emp.botId, 'Автоматический чек-' + (In
            ? 'ин'
            : 'аут') + ' прошел успешно.');
          SendMsgToAdmins(emp.firstname+' '+emp.lastname+ ' (' +emp.employee_id +') - автоматический чек-' + (In
            ? 'ин'
            : 'аут'));}
        cb(true);
      }).catch(function(err) {
        cb(false, err)
        console.log(err);
      });
    } else {
      cb(false, 'Не знаю как, но вы решили зачекинить человека которого нет О_о. Сообщите об этом Администратору!')
    }
    // } // time check
  })
}

// принимает ([date/"", date/""], date , true/false) в итоге если пришло ([date, ""], date2, false) то вернет [date,date2]
function checkNext(check,date,In){
  console.log('Executed CheckNext with [',check,date,In,']');
  if(!check) check = [];
  if(In) {
    check.push(['',''])
    check[check.length-1][0] = date;
  } else {
    check[check.length-1][1] = date;
  }
  console.log('setted check ',check[check.length-1] );
  return check;
}

// зачекинен ли?
function isChecked(id, cb) {
  Employee.findOne({
    employee_id: id.toUpperCase()
  }, function(e, f) {
    if (!e) {
      if (f) {
        date = (f.checked ? f.checkedIn : f.checkedOut);
        cb(null, {
          checked: f.checked,
          date
        });
      } else {
        cb({
          error: true,
          msg: 'User Not Found'
        }, null)
      }
    } else {
      cb({
        error: true,
        msg: 'Mongoose Error'
      }, null)
    }
  })
}



module.exports.autoCheckOut = autoCheckOut;
module.exports.autoCheckAlert = autoCheckAlert;
module.exports.autoReminder = autoReminder;
module.exports.autoCheckIn = autoCheckIn;
module.exports.saveNote = saveNote;
module.exports.say = say;
module.exports.dayEndCheckOut = dayEndCheckOut;
module.exports.CheckMe = CheckMe;
module.exports.checkNext = checkNext;
