const {bot} = require('../../bot/initBot');

const Employee = require('../Models/Employee');
const adminAccess = require('../Models/adminAccess');
const {makeHash} = require('../../helpers/AllFunctions')


function adminMe(id) {
  Employee.findOne({
    employee_id: id
  }, function(error,emp) {
    if (emp) {
      if (emp.admin==false) {
        emp.admin = true;
      } else {
        emp.admin = false;
      }
      emp.save();
    }
  })
}

// генерирует админский хэш
function genAdminHash(id) {
  var hash = makeHash();
  adminAccess.findOneAndRemove({id},function (er,w) {
    var newAccess = new adminAccess({
      id,
      hash
    })
    newAccess.save();
  })
  return hash;
}

// удаляет
function removeAdminHash(id) {
  adminAccess.remove({id}, function (err,removed) {
    if(!err){
      console.log('Admin logged Out. Access hash removed.');
    } else {
      console.log(id+' is not admin. So i`ll ingore him.');
    }
  })
}

// отправляет сообщение всем админам
function SendMsgToAdmins(msg) {
  Employee.find({admin:true},function (err,admins) {
    if(admins){
      admins.forEach(function (admin) {
        var adm = admin.botId;
        console.log(adm);
        //console.log('Sending Error to Boss',photo,adm,msg)
        bot.sendMessage(adm,msg)
      })
    }
  })
}

// отправляет фотку админам( сейчас то не фотка а просто ссылка на фотку)
function SendPhotoToAdmins(photo,msg,then) {
  Employee.find({admin:true},function (err,admins) {
    if(admins){
      admins.forEach(function (admin) {
        var adm = admin.botId;
        console.log('Sending Otchet to Boss',photo,adm,msg)
        bot.sendMessage(adm, photo+ '?r=' + new Date().getTime())
          .then(function (a) {
            bot.sendMessage(adm,msg)
            then();
          }).catch(function (err) {
            console.log('Cant send a Photo->',err)
            then();
          });
      })
    }
  })
}


module.exports.adminMe = adminMe;
module.exports.genAdminHash = genAdminHash;
module.exports.removeAdminHash = removeAdminHash;
module.exports.SendMsgToAdmins = SendMsgToAdmins;
module.exports.SendPhotoToAdmins = SendPhotoToAdmins;
