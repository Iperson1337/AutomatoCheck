const path = require('path');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const Employee = require('../Models/Employee');
const adminAccess = require('../Models/adminAccess');

const config = require('../../config')
const file = require('../../helpers/files');

const BotHelper = require('../../bot/helper/BotFunctions');
const {bot} = require('../../bot/initBot');
const {CheckMe} = require('../Repository/check');
const {genAdminHash, SendPhotoToAdmins} = require('../Repository/admin');
const {CodeGen, GetObjById, ValInObjArr, comp, checkForNumber} = require('../../helpers/AllFunctions');

const lastPhoto = path.join(__dirname, '../../../static/photos/');
var existImg = [];
var step = [{
  id: 'test',
  step: 0,
  code: '',
  status: 'in',
  photo: '',
  firstname: '',
  lastname: ''
}];

// получает текущий шаг того кто чекинится
function getStep(id) {
  var st = GetObjById(step, id);
  if (!checkForNumber(st)) {
    return false
  }
  return st.step;
}

// делает следующий шаг чекина
function nextStep(id) {
  var st = ValInObjArr(step, id, 'id');
  if (st > -1) {
    step[st].step++;
    return true;
  }
  return false
}

// тут обрабатываются все уровни чекина чекаута человека
function stepsCheckInOrOut(data, send) {
  let {id} = data;
  switch (getStep(id)) {
    // on code step. Когда человек отправил код, он обрабатывается тут
    case 0: // тут используются функции genAdminHash и nextStep
      checkCode(data, send)
      break;

    // on checkIn step
    case 1: // когда человек сфоткался, он попадает сюда тут фотка сохраняется в файл
      savePhoto(data, send)
      break;

    // on checkout step
    case 2: // после того как человек написал инсайт или нажал на кнопки типа "всё на месте всё в порядке"
      // тут отправляется фото админу и юзеру пишется что чел ЫЫЫЫ
      sendReport(data, send)
      break;

    // on initial (first) step
    case false: // когда человек вводит id и отправляет, он попадает сюда
      checkID(data, send)
      break;
    default:
      send({
        success: false,
        msg: 'Кажется тебя занесло куда-то не туда.'
      })
  }

}

/**
 * принимает employee_id
 */
function checkID(data, send) {
  let {id} = data;
  if (id&&checkForNumber(getStep(id)) == false ) {

    Employee.findOne({ // тут его находят по id и создают ему уровень 0
      employee_id: id.toUpperCase()
    }, function(e, f) {
      if (f) {

        var code = CodeGen();
        var next = f.checked ? 'out' : 'in';
        var date = f.checked ? f.checkedIn : f.checkedOut;
        var day = new Date().getDate();
        step.push({
          id: id,
          BotId: f.botId,
          step: 0, // значит что если он снова отправит сюда свой id , то он будет обрабатываться проверкой пароля
          code: code, // код который ожидается
          status: next,
          admin: f.admin,
          firstname: f.firstname,
          lastname: f.lastname,
          tgname: f.tgname ? f.tgname : "",
          day
        })

        bot.sendMessage(f.botId, 'Код для check'+next+':'+code)
        send({
          success: true,
          firstname: f.firstname,
          gender: f.gender,     // для учета окончания при чекине
          lastname: f.lastname,
          userId: f._id,
          next,
          date
        })
        console.log(code, '< Code of',id,f.firstname,'Going to',next)
      } else {
        send({
          success: false,
          msg: 'Работник с таким ID не найден!'
        })
      }
    })
  } else {
    send({
      succss: false,
      msg:''
    })
  }
}

/**
 *  принимает employee_id
 *  принимает Code
 */
function checkCode(data, send) {
  let {id, code} = data
  var stp = GetObjById(step, id);
  console.log(id,'code step');
  var fncomp = comp(new Date(), '23:59', 'min');
  // console.log('stp.day = ',stp.day, 'and comp =',fncomp);
  if( fncomp.result == '=') {
    send({
      success: false,
      msg: 'Извините, сейчас вы не можете делать Чекин или Чекаут. Попробуйте через 1 Минуту'
    })
  } else
  if (id && code) { //если пришел код и ID
    if (stp) {
      if (stp.code == code) { // если код совпал с кодом который генерируется в начале
        // console.log('Debut Code:', code,stp.code)
        //что в jwt токен
        const payload = {
            _id: id,
            admin: stp.admin
        }
        //генерация jwt токена
        var token = jwt.sign(payload, config.jwt.secret, {expiresIn: config.jwt.tokenExpiry})

        if(stp.admin){ // админ
          send({
            success: true,
            next: stp.status,
            isAdmin: stp.admin,
            hash: genAdminHash(id),
            // employee: payload,
            token: token // jwt
          });
        } else {//не админ
          send({
            success: true,
            next: stp.status,
            isAdmin: stp.admin,
            // employee: payload,
            token: token
          });
        }
        nextStep(id);
      }else{ //код не верный
        send({
          success: false,
          msg: 'Вы ввели неверный код.'
        })
      }
    }
  } else { //код не отправлен
    // сюда попадают люди, которые перезагрузили страницу или если кто-то пытается нелепо обойти чекин
    if(id) {
      step.splice(ValInObjArr(step, id, 'id'), 1);
      stepsCheckInOrOut(data,function (an) {
        send(an)
      })
    }
  }
}

/**
 *  принимает employee_id
 *  принимает Photo
 */
function savePhoto(data, send) {
  let {id, photo} = data
  var stp = GetObjById(step, id);
  // в 23:59 чтобы не могли делать чекин чекаут
  if( comp(new Date(), '23:59', 'min').result == '=') {
    send({
      success: false,
      msg: 'Извините, сейчас вы не можете делать Чекин или Чекаут. Попробуйте через 1 Минуту'
    })
  } else if (id && photo) {
    // сохраняем фотку
    fs.writeFile(lastPhoto+id.toLowerCase()+'.jpg', new Buffer(photo, "base64"), function(err, asd) {
      console.log('photo saved as '+lastPhoto+id+'.jpg', stp)
      if (stp.status == 'in') {
        nextStep(id);
        step[ValInObjArr(step, id, 'id')].photo = lastPhoto+id+'.jpg';
        send({
          success: true,
          next: stp.status
        })
      } else {
        nextStep(id);
        step[ValInObjArr(step, id, 'id')].photo = lastPhoto+id+'.jpg';
        send({
          success: true,
          next: stp.status
        })
      }
    });

  } else {
    if (id) {
      step.splice(ValInObjArr(step, id, 'id'), 1);
      stepsCheckInOrOut(data, function(an) {
        send(an)
      })
    }
  }
}

/**
 *  принимает employee_id
 *  принимает report
 */
function sendReport(data, send) {
  let {id, report} = data
  if(comp(new Date(), '23:59', 'min').result == '=') {
    send({
      success: false,
      msg: 'Извините, сейчас вы не можете делать Чекин или Чекаут. Попробуйте через 1 Минуту'
    })
  } else if (id&&report) {
    var stp = GetObjById(step, id);
    if(stp.status == 'in'){ // если чеовек делал чекин
      var pathToPhoto = 'https://checklist.automato.me/photos/'+stp.id.toLowerCase()+'.jpg'
      // var pathToPhoto = lastPhoto+id.toLowerCase()+'.jpg'
      // человеку отправляется сообщение что он сделал чекин
      bot.sendMessage(stp.BotId,'Вы успешно сделали Чек-Ин. \nПожалуйста, не забывайте делать вовремя Чек-Аут')
      if(stp.tgname && (stp.tgname!=undefined || stp.tgname!="")){
        console.log(stp, "tgname not found");
        SendPhotoToAdmins(pathToPhoto, id.toUpperCase()+': '+stp.firstname+' '+stp.lastname+' (@'+stp.tgname+')  в офисе.\n'+report,function () {})
      } else {
        console.log(stp, "tgname found");
        SendPhotoToAdmins(pathToPhoto, id.toUpperCase()+': '+stp.firstname+' '+stp.lastname+' в офисе.\n'+report,function () {})
      }
      step.splice(ValInObjArr(step, id, 'id'), 1);
      //вызывается функция checkme которая делает чекин на этот id
      CheckMe(id, true, function (success,msg) {
        send({
          success: success,
          next: stp.status,
          msg: msg||''
        })
      });
    } else {
      var vkReg = /(facebook\.com)\/automato.me(.+)\/([0-9]{3,20})/i
      if(vkReg.test(report)){ // если оставил инсайт
        var pathToPhoto = 'https://checklist.automato.me/photos/'+stp.id.toLowerCase()+'.jpg'
        bot.sendMessage(stp.BotId,'Вы успешно сделали Чек-Аут. \nНадеемся, день у вас был плодотворным!')
        // SendPhotoToAdmins(pathToPhoto, id+': '+stp.firstname+' '+stp.lastname+'\nОтчет за день: '+report,function () {
        // })
        if(stp.tgname && (stp.tgname!=undefined || stp.tgname!="")){
          SendPhotoToAdmins(pathToPhoto, id+': '+stp.firstname+' '+stp.lastname+' (@'+stp.tgname+') \nОтчет за день: '+report+'\n ',function () {})
        }else{
          SendPhotoToAdmins(pathToPhoto, id+': '+stp.firstname+' '+stp.lastname+'\nОтчет за день: '+report,function () {})
        }
        step.splice(ValInObjArr(step,id,'id'),1);
        CheckMe(id,false,function (success,msg) {
          // removeAdminHash(id);
          console.log('Checked Out =>',success,msg,'<= Checked Out');
          send({success:success,msg:msg||''})
        });

      } else {
        send({success: false, msg:'Пожалуйста, оставьте правильную ссылку на инсайт.'})
      }
    }
  }else {
    if(id) {
      step.splice(ValInObjArr(step, id, 'id'), 1);
      stepsCheckInOrOut(data,function (an) {
        send(an)
      })
    }
  }
}


// Check in-out requests handler
function checkHandler(req, res) {
  stepsCheckInOrOut(req.body, function(r) {
    res.send(r);
  })
}

module.exports.checkHandler = checkHandler;// нужен для кика юзера
