var moment = require('moment');
var Employee = require('../Models/Employee');
var Department = require('../Models/Department');
var Vacancy = require('../Models/Vacancy');
var adminAccess = require('../Models/adminAccess');
var {employeeInfo} = require('../Repository/employee');
var {dateToArray} = require('../../helpers/AllFunctions');
var waterfall= require('async/waterfall');



function allEmployees(req, res, next) {
  Employee.find({disabled: false}, (err, employees)=>{
    if(err) console.log(err);
    if(employees){
      res.send({
        employees: employees,
        success: true
      })
    }
  })
}

function showEmployee(req, res, next) {

  /**
   *  query id - _id employee
   *  необезательное - query selectedMonth - принимает дату(например последный день месяца 2018-05-31T18:00:00.000Z)
   */

    var reports =[];

    if (req.query.selectedMonth){//если был передан при обращении к api конкретный месяц
      const incomeMonth = req.query.selectedMonth;
      var selectedMonth = moment(incomeMonth).toObject();
    }
    Employee.findOne({_id: req.params.id}, (err, employee)=>{
      if(err) console.log(err);
      if(employee){
          var l =0;
          var length =  employee.days.length;
          employee.days.forEach(function(day, d){
            if(selectedMonth && day.year.toString() == selectedMonth.years && day.month.toString() == selectedMonth.months
            && day.day.toString() != selectedMonth.date){//если был передан при обращении к api конкретный месяц
              day.mins = 0;
              day.salDay = 0;
              reports.push(day);
            }else if(day.year.toString() === moment().get('year').toString() && day.month.toString() === moment().get('month').toString()
            && day.day.toString() != moment().get('date')){
              day.mins = 0;
              day.salDay = 0;
              reports.push(day);
            }
            l++;
          })
          if(l==length){
            Department.findOne({short_name: employee.department_id}, (err, department)=>{
              if(err) console.log(err);
              if(department){
                if(selectedMonth) {//если был передан при обращении к api конкретный месяц
                  employeeInfo(employee.employee_id, selectedMonth.years, selectedMonth.months, function (err, inf) {
                    if(err) console.log(err);
                    if(inf) {
                      inf.salary.byDay.forEach(function(min, m){
                        if(reports[m]!=undefined){
                          reports[m].salDay = Math.floor(min.workedMins*inf.salary.salPerMin);
                          reports[m].mins = min.workedMins;
                        }
                      })
                      res.send({
                        employee: employee,
                        salary: inf.salary,
                        department_name: department.name,
                        reports: reports,
                        success: true
                      })
                    }
                    else {
                      res.send({
                        employee: employee,
                        salary: inf,
                        department_name: department.name,
                        reports: reports,
                        success: true
                      })
                    }
                  })
                }
                else { //если не был передан при обращении к api конкретный месяц
                  var d = dateToArray();
                  employeeInfo(employee.employee_id, d[0], d[1], function (err, inf) {
                    if(err) console.log(err);
                    if(inf){
                      inf.salary.byDay.forEach(function(min, m){
                        if(reports[m]!=undefined){
                          reports[m].salDay = Math.floor(min.workedMins*inf.salary.salPerMin);
                          reports[m].mins = min.workedMins;
                        }
                      })
                      res.send({
                        employee: employee,
                        salary: inf.salary,
                        department_name: department.name,
                        reports: reports,
                        success: true
                      })
                    }
                    else {
                      res.send({
                        employee: employee,
                        salary: inf,
                        department_name: department.name,
                        reports: reports,
                        success: true
                      })
                    }
                  })
                }
              }
            })
          }
      }
      else{
        res.send({
          success: false
        })
      }
    })
}

function updateEmployee(req, res) {
    const editedEmployee = req.body.editedEmployee;
    Employee.findOne({_id: editedEmployee._id}, (err, employee)=>{
      if(err) console.log(err);
      if(employee){
        employee.firstname = editedEmployee.firstname;
        employee.lastname = editedEmployee.lastname;
        employee.fathername = editedEmployee.fathername;
        employee.salaryFull = editedEmployee.salaryFull;
        employee.salary_fixed =  editedEmployee.salary_fixed;
        employee.vacancy = editedEmployee.vacancy;
        employee.department = editedEmployee.department;
        employee.fixT = editedEmployee.fixT;
        employee.fixST = editedEmployee.fixST;
        employee.gender = editedEmployee.gender;
        employee.save(function(err, edited){
          if(err) console.log(err);
          if(edited){
            res.send({
              success: true,
              message: "Ваши изменения сохранены!"
            })
          }
        })
      }
    })
}

function disabledEmployee(req, res) {
    Employee.findOne({_id: req.body.id}, (err, employee)=>{
      if(err) console.log(err);
      if(employee){
        employee.disabled = true;
        employee.save(function(err, disabled){
          if(err) console.log(err);
          if(disabled){
            res.send({
              success: true,
              message: "Вы успешно удалили сотрудника!"
            })
          }
        })
      }
    })
}

function countEmployees(req, res, next) {
    Employee.count({disabled:false}, (err, number)=>{
      if(err) console.log(err);
      if(number){
        res.send({
          number: number,
          success: true
        })
      }
    })
}

function inOfficeEmployees(req, res, next) {
  Employee.find({checked: true, disabled: false}, (err, employees)=>{
    if(err) console.log(err);
    if(employees){
      res.send({
        employees: employees,
        success: true
      })
    }
  })
}

function outsideOfficeEmployees(req, res, next) {
  Employee.find({checked: false, disabled: false}, (err, employees)=>{
    if(err) console.log(err);
    if(employees){
      res.send({
        employees: employees,
        success: true
      })
    }
  })
}

function addHolidays(req, res, next) {//добавление праздника в holidays
  const editedEmployee = req.body.editedEmployee;
  console.log(editedEmployee);
  var holidayDate = moment(editedEmployee).toObject();
  Employee.find({employee_id : req.body.id}, (err, employees) =>{
        if(err) console.log(err);
        if(employees){
          employees.forEach(function(emp, e){
            if (emp.holidays.length == 0) {
              var holArray= [];
              holArray.push(+holidayDate.date);
              emp.holidays.push({"year" : +holidayDate.years, "month": +holidayDate.months, "days" : holArray});
            } else {
            var flagFindHoliday = true;
            for (let i = 0; i < emp.holidays.length; i++) {

              if ( emp.holidays[i].year == holidayDate.years && emp.holidays[i].month == holidayDate.months ) {
                flagFindHoliday = false;
                if (emp.holidays[i].days.indexOf(+holidayDate.date) == -1){
                  emp.holidays[i].days.push(+holidayDate.date);
                  emp.markModified('holidays');
                 }
                 else {
                 console.log('такой праздник уже есть');
                }
              }
            }
            if (flagFindHoliday){
              var holArray= [];
                  holArray.push(+holidayDate.date);
                  emp.holidays.push({"year" : +holidayDate.years, "month": +holidayDate.months, "days" : holArray});
            }
          }
        emp.save(function(err, edited){
          if(err) console.log(err);
          // if(edited){
          //   res.send({
              //success: true,
          //     message: "Ваши изменения сохранены!"
          //   })
          // }
        })
      })
    }
  })
}

function piechartstatistic(req, res, next) {
    Employee.find({disabled: false}, (err, employees) => {
    if (err) console.log(err);
    if (employees) {
      var dorabotaly = 0, nedorabotaly = 0, nebyli = 0, c = 0;
      var length = employees.length;
      var d = dateToArray();
      employees.forEach(function(emp, e) {
        employeeInfo(emp.employee_id, d[0], d[1], function (err, inf) {
          if(err) {
            nebyli++;
            c++;
            console.log(err);
          } else if (inf) {
            if (inf.salary.nedorabotal) {
               c++;
               nedorabotaly++;
             }
              else {
                dorabotaly++;
                c++;
             }

          }
          if (c == employees.length) {
            res.send({
              dorabotaly: dorabotaly,
              nedorabotaly: nedorabotaly,
              nebyli: nebyli
            })
          }
        })
      })
    }
  })
}

function statisticofmonth(req, res, next) {
    Employee.find({disabled: false}, (err, employees)=>{
      if(err) console.log(err);
      if(employees){
          var minutes = 0, i =0, averageMin=0, c=0;
          var length = employees.length;
          var d = dateToArray();
          var salaryPerDay =[];
          for(var j=1; j<d[2]; j++){
            salaryPerDay.push({
              day: j,
              sal: 0,
              employees: 0
            })
          }
          employees.forEach(function(emp, e){
          employeeInfo(emp.employee_id, d[0], d[1], function (err, inf) {
           // c++;
            if(err) {
              c++;
              console.log(err);
            }
            if(inf){
              c++;
              // console.log(minutes, i, 261);
              minutes = minutes + inf.salary.totalMonthMin/inf.salary.byDay.length;
              i++;
              inf.salary.byDay.forEach(function(bd, b){
                salaryPerDay.forEach(function(salar, s){
                  if(salar.day == bd.day){
                    salar.sal = salar.sal+bd.workedMins*inf.salary.salPerMin;
                    salar.count++;
                    // console.log(salar.sal, +bd.workedMins, inf.salary.salPerMin, 275);
                    // console.log(salar.sal, s, 275);
                  }
                })
              })
            }
              if(c == employees.length){
                // console.log(minutes, i, "--------------------------------------------------");
                res.send({
                  minutes: minutes,
                  i: i,
                  salary: salaryPerDay
                })
              }
          })
        })
      }
    })
}


module.exports.allEmployees = allEmployees
module.exports.showEmployee = showEmployee
module.exports.updateEmployee = updateEmployee
module.exports.disabledEmployee = disabledEmployee
module.exports.countEmployees = countEmployees
module.exports.inOfficeEmployees = inOfficeEmployees
module.exports.outsideOfficeEmployees = outsideOfficeEmployees
module.exports.piechartstatistic = piechartstatistic
module.exports.statisticofmonth = statisticofmonth
module.exports.addHolidays = addHolidays
