var Task = require('../Models/Task')
var Employee = require('../Models/Employee');
var requestify = require('requestify');
var monitorTasks = 'http://localhost/api/lastTasks';
var moment = require('moment');

var today = moment().startOf('day')
var tomorrow = moment(today).endOf('day')
var startMonth = moment().startOf('month')
var endMonth = moment(startMonth).endOf('month')

function storeTasks(req, res) {
  /**
   * @param {Array} req.body.tasks - массисв из задач
   * @param {Mongo:ObjectId} req.body.id - _ID Employee @param
   */

  let tasks = req.body.tasks;

  Employee.findOne({_id: req.body.id}, (err, employee) => {
    if(err) console.log(err);
    if(employee){
      let count = 0
      tasks.forEach(function(task, t){
        ++count
        var newTask = new Task({
          employee_id: employee._id,
          project_name: task.project_name,
          text: task.text,
          created_at: today,
          deadline_year: new Date(task.deadline).getFullYear(),
          deadline_month: new Date(task.deadline).getMonth(),
          deadline_day: new Date(task.deadline).getDate()
        }).save()
        .then( savedtask => {
          console.log('succesfully save task', savedtask._id);
        })
        .catch(err => {
          console.log(err);
        })

      })
      if(tasks.length == count) {
        res.status(200).json({
          success: true,
          message: "Задачи успешно добавлены"
        })
      } else {
        res.status(422).json({
          success: false,
          message: "Не все задачи добавлены"
        })
      }
    }
  })
}

function updateTask(req, res) {
  /**
   * @param {Mongo:ObjectId} req.body.id  - _ID Task
   * @param {String} req.body.text  - Task text
   * @param {String} req.body.project_name  - Названия проекта
   * @param {Date} req.body.deadline  - deadline задачи
   */

  Task.findById( req.body.id, function (err, task) {
    if (err) console.log(err);
    task.project_name = req.body.project_name;
    task.text = req.body.text;
    task.updated_at = today;
    task.deadline_year = new Date(req.body.deadline).getFullYear(),
    task.deadline_month = new Date(req.body.deadline).getMonth(),
    task.deadline_day = new Date(req.body.deadline).getDate()
    task.save()
      .then(result => {
        res.status(200).json({
          success: true,
          msg: 'успешно сохранен'
        })
      })
      .catch(err => {
        res.status(500).json({
          error: err
        })
      });
  });

}

function getTodayTasks(req, res) {

  if(req.query.id && req.query.id!=undefined){
    Employee.findOne({employee_id: req.query.id.toUpperCase()}, (err, employee)=>{
        if(err) console.log(err);
        if(employee){
          Task.find({employee_id: employee._id, created_at: {"$gte": today, "$lt": tomorrow}, complated: false}).populate('employee_id').exec(function(err, tasks){
            if(err) console.log(err);
            if(tasks){
              res.send({
                tasks: tasks
              })
            }
          })
        }
    })
  } else {
    Employee.findOne({_id: req.query._id}, (err, employee)=>{
        if(err) console.log(err);
        if(employee){
          Task.find({employee_id: employee._id, created_at: {"$gte": today, "$lt": tomorrow}, complated: false}).populate('employee_id').exec(function(err, tasks){
            if(err) console.log(err);
            if(tasks){
              res.send({
                tasks: tasks
              })
            }
          })
        }
    })
  }

}

function statisticsofTasks(req, res) {
  var today = new Date();
  Task.count({complated: false, created_at: {"$gte": startMonth, "$lt": endMonth}}, (err, tasks)=>{
    if(err) console.log(err);
    else {
      Task.count({complated: true, created_at: {"$gte": startMonth, "$lt": endMonth}}, (err, donetasks)=>{
        if(err) console.log(err);
        else {
          res.send({
            percents: [donetasks, tasks]
          })
        }
      })
    }
  })
}

function doneTasks(req, res) {
  var donetasks = req.body.tasks;
  // console.log(req.body.tasks, 258);
  var sendtasks= [];
  Employee.findOne({employee_id: req.body.id.toUpperCase()}, (err, employee)=>{
      if(err) console.log(err);
      if(employee){
        var i =0;
        var length = donetasks.length;
        if(length === 0) {
          res.send({msg: "succesfully saved"})
        } else {
          donetasks.forEach(function(task, t){
            Task.findOne({employee_id: employee._id, _id: task, complated: false}, (err, donetask)=>{
              if(err) {
                i++;
                if(i == length){
                  SendTasksToMonitor(sendtasks);
                  // console.log(271);
                  res.send({msg: "succesfully saved"})
                }
                console.log(err);
              }
              if(donetask){
                donetask.complated = true;
                donetask.complated_at = today;
                donetask.save(function(err, saved){
                  if(err){
                    i++;
                    console.log(err);
                    if(i == length){
                      SendTasksToMonitor(sendtasks);
                      // console.log(287);
                      res.send({msg: "succesfully saved"})
                    }
                  }
                  if(saved){
                    sendtasks.push({
                      name: employee.firstname+" "+ employee.lastname,
                      task: donetask.text
                    })
                    i++;
                    if(i == length){
                      SendTasksToMonitor(sendtasks);
                      // console.log(298);
                      res.send({msg: "succesfully saved"})
                    }
                  }
                })
              }
            })
          })
        }
      }
  })
}

function SendTasksToMonitor(all) {
  requestify.post(monitorTasks, {
     all: all
  })
  .then(function(response) {
   console.log('Sended tasks to monitor', all);
  })
  .catch(function (e) {
   console.log('error sending to monitorTask: '+monitorTasks, e);
  });
}

module.exports.getTodayTasks = getTodayTasks
module.exports.statisticsofTasks = statisticsofTasks
module.exports.doneTasks = doneTasks
module.exports.updateTask = updateTask
module.exports.storeTasks = storeTasks
