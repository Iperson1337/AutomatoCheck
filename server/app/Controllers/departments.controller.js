var Employee = require('../Models/Employee');
var Department = require('../Models/Department');
var Vacancy = require('../Models/Vacancy');
var adminAccess = require('../Models/adminAccess');

function departments(req, res, next){
  Department.find({}, (err, departments)=>{
    if(err) console.log(err);
    else{
      res.send({
        departments: departments,
        success: true
      })
    }
  })
}

function countDepartments(req, res, next){
  Department.count({}, (err, departments)=>{
    if(err) console.log(err);
    else{
      res.send({
        number: departments,
        success: true
      })
    }
  })
}

function addDepartment(req, res, next){
  new Department({
    name: req.body.department_name,
    short_name: req.body.short_name
  }).save(function(err, savedDepartment){
    if(err) console.log(err);
    if (savedDepartment) {
      console.log(savedDepartment);
      res.send({
        success: true
      })
    }
  });
}

function deleteDepartments(req, res){
   Department.find({short_name: req.body.short_name}, (err, departments)=>{
    if(err) console.log(err);
    else{
      var flagDeleteDepartment = false;
      var depsWithEmployee = 0;
      foundDeps = [];
      departments.forEach(function(department, d){//сначала проверяем есть ли отмеченные отделы с сотрудниками внутри
        if (department.employees.length === 0){
          // department.remove({_id: department._id});
          foundDeps.push(department._id);
          flagDeleteDepartment = true;
        } else {
          console.log("Выбран отдел где есть сотрудники!");
          depsWithEmployee++;//увеличиваем счетчик если сотрудники в каком-то отделе есть
          flagDeleteDepartment = false;
        }
      });
      if (flagDeleteDepartment && depsWithEmployee === 0 ) {
        for (let i = 0; i < foundDeps.length; i++) {
          departments.forEach(function(department, d){//удаляем отдел только если не были отмечены отделы с сотрудниками
            department.remove({_id: foundDeps[i]});
          })
        }
        res.send({
          success: true
        })
      } else {
        res.send({
          success: false
        })
      }
    }
  })
}

module.exports.departments = departments;
module.exports.countDepartments = countDepartments;
module.exports.addDepartment = addDepartment;
module.exports.deleteDepartments = deleteDepartments;
