var adminAccess = require('../../Models/adminAccess');

function checkAdmin(req, res, next) {
  adminAccess.findOne({id:req.body.id}).exec(function(err, admin){
		if(!admin || err){
			res.send({success:false});
			return;
		}
		res.status(200).send({
			success: true,
			info: admin
		});
	});

}

module.exports.checkAdmin = checkAdmin;
