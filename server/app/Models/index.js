/**
 * Includes all models into one place
 *
 * @module models
 * @author Alibi
 */

module.exports = {
  Employee: require('./Employee'),
  Departments: require('./Department'),
  Task: require('./Task'),
  Holidays: require('./Holidays'),
  Vacancy: require('./Vacancy'),
  adminAccess: require('./Vacancy')
};
