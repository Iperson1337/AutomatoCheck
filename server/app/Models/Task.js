'use strict';

var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;


var TaskSchema = new Schema({
    employee_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Employee"
    },
    project_name: String,
    text: {
      type: String
    },
    deadline_year: Number,
    deadline_month: Number,
    deadline_day: Number,
    deferred: { type: Number, default: 0},
    complated: {type: Boolean, default: false},
    complated_at: Date,
    created_at: Date,
    updated_at: Date
},{
    timestamp:true
});

module.exports = mongoose.model('Task', TaskSchema);
