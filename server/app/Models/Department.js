var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;
//var passportLocalMongoose = require('passport-local-mongoose');

var DepartmentSchema = new Schema({
    name: String,
    short_name: {type: String, unique: true},
    employees: [{type: mongoose.Schema.Types.ObjectId, ref: 'Employee'}]
},{
    timestamp:true
});

module.exports = mongoose.model('Department', DepartmentSchema);
