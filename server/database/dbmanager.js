/** Get schemas **/
const {
  Employee,
  Departments,
  Task,
  Holidays,
  Vacancy,
  adminAccess
} = require('../app/Models');

function findEmployee(query) {
  return new Promise((fullfill) => {
  Employee.findOne(query)
    .exec((err, employee) => {
      if (err) throw err;
      else fullfill(employee);
    });
  });

}

module.exports = {
  findEmployee
}
