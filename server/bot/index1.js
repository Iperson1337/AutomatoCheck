const mongoose = require('mongoose');
const moment = require('moment');
const waterfall = require('async/waterfall');
const fs = require('fs');
const BotHelper = require('./helper/BotFunctions');
const Employee = require('../app/Models/Employee');
const Departments = require('../app/Models/Department');
const func = require('../helpers/AllFunctions');
const { bot } = require('./initBot');
const { CheckMe } = require('../app/Repository/check');
const { SendMsgToAdmins } = require('../app/Repository/admin');
const { grabInfo, getUserInfoNew, getUserInfo, employeeInfo } = require('../app/Repository/employee');
const { getWordEndSyaAs, getWordEndElLa, getWordEnd, kickUser, autoCheckInUser, checkEmployees, absentEmployees, updating} = require('./functions/employees');
const { alertAdmins, goToLunch, iWillLate, leaveEarly, noteStatus } = require('./functions/warningBoss')

var userWork = 50;
var userInfo = 100;
var wordEnding;
var wordEnd;
var wordEndElLa;


var que = [
  {
    index: 0,
    qType: 'text',
    options: {},
    answers:[
      {
        AnsType: 'text',
        reg:/(.+)/i
      }
    ],
    data: 'Введи свой ID  '
  },
  {
    index: 1,
    qType: 'text',
    options: {},//BotHelусper.makeButtons(['Да, это я','Нет, я его не знаю'],['yes','no'],2,true),
    answers:[
      {
        AnsType: 'inline'
      }
    ],
    data: 'Вы уверены что это ваш ID?'
  },
  {
    index: 2,
    qType: 'text',
    options: BotHelper.makeButtons(['Помощь','Меню'],['Инфо','Работа'],2),
    answers:[
      {
        AnsType: 'text',
        // reg:/(инфо|info|команд|как)/i,
        next: 2
      },
      {
        AnsType: 'text',
        reg:/(меню)/i,
        next: 2
      }
    ],
    data: 'Напиши или нажми на «Помощь» или «Меню», чтобы узнать доступные команды',
    err: 'Балда, сказано же «Помощь» или «Меню»'
  },
  ...require('./UserMenu'),
  ...require('./Registration')
]
var trig = {
  stopOn: {
    force: [],
    //  [{
    //   stop:false,
    //   uid:0
    // }]
    AnsType: 'text',
    reg: /^\/Уволиться/
  },
  startOn: {
    AnsType: 'text',
    reg: /(.*)/i
  }
}


// получает индекс вопроса
function getIndex(index) {
  return func.IndInObjArr(que, index, 'index')[0]
}
//отправляет смс через теллеграмм бот
function say(id,msg,callback){
  function f(){}
  bot.sendMessage(id,msg||'...').then(callback||f).catch(callback||f)
}

// админ ли данный чел
function isAdmin(Some_id, callback){
  var query = Some_id ?
  (/^([0-9]{8,10})$/i.test(Some_id) ? {botId: Some_id, admin: true} : {admin: true, employee_id: Some_id.toString().toUpperCase()})
  : false;
  if(!query) return false
  Employee.findOne(query, callback)
}

// вривязывет id к botId
function bindBotId(id, bindID, callback) {
  Employee.findOne({employee_id: id.toUpperCase()},function (err,emp) {
    if(emp){
      if(!(/^([0-9]{8,10})$/i.test(emp.botId))){
        emp.botId = bindID;
        emp.save()
        .then(function (asd) {
          callback(null, 'Тигр! Ты успешно привязал Телеграм!')
        })
        .catch(function (asd) {
          callback('Извини, но у этого ID уже есть владелец', null)
        })

      } else {
        console.log(emp.botId, (/^([0-9]{8,10})$/i.test(emp.botId)),'<-----------')
        callback(null, 'Ваш телеграм уже привязан к данному ID.', emp.botId)
      }
    } else {
      callback('Ошибка. Обратитесь к Администратору.', null)
    }
  })
}

// инфо по командам
function infoCommands(command){
  var total = /(помощь|команд|инфо|как)/i
  var info = 'Все доступные команды:\n'+
  '«Помощь» — список всех команд\n'+
  '«Меню» — меню с информацией по работе\n'+
  '===Команды Админа===\n'+
  '«/salary ID <MM.ГГ>» — Просмотр зароботной платы за предыдущий или указанный месяц\n'+
  '«/аналитика ID» — Аналитика на текущий месяц\n'+
  '«/check» — узнать кто в офисе\n'+
  '«/absent» — узнать кто не в офисе\n'+
  '«/kick ID» — кикнуть кто по факту не в офисе\n'
  return info
}
// 4 параметр - колбэк
BotHelper.askFor(bot, que, trig,
  function (user_Id,username,msg,cb) {
    // Start callback
    /* когда человек пишет старт вызывается этот колбэк
    тут находит его в бд по id и если его нет то говорит ему регистрироваться (50 вопрос)
    */
    console.log(username, 'going to start');
    Employee.findOne({botId: user_Id}, function (err, found) {
      if(err) console.log(err);
      if(found) {
        console.log('found');
        say(user_Id, 'Здравствуй, ' + found.firstname+' Твой ID: '+found.employee_id)
        cb(true, 2)
      } else {
        console.log('not found');
        cb(true, 50)
      }
    })
  },
  function (user_Id,answers,ind,cb,msg) {
    // Mid callback
    /*
    Эта функция вызывается когда человек после слова старт вызывает меню и жмет кнопки
    каждый кейс отвечает за каждый вопрос
    все вопросы в переменной que.
    */
    if(msg.from.username && msg.from.username != undefined){
      Employee.findOne({botId: msg.from.id}, function(err, foundemployee){
        if(err) console.log(err);
        if(foundemployee){
          foundemployee.tgname = msg.from.username;
          foundemployee.save(function(err, saved){
            if(err) console.log(err);
            if(saved){
              console.log("saved tgname");
            }
          })
        }
      })
    }
    switch (ind) { // индекс вопроса
      case 0:
        // Bind ID Logic

        getUserInfo(answers[ind], function (err,user) {
          if(err) {
            console.log('user_Id',user_Id,answers[ind],err);
            bot.sendMessage(user_Id, 'Хэй, ID '+answers[ind]+' не существует.\nПожалуйста, проверь правильность введённых данных.')
            cb(true, 0, true);
          } else {
            bot.sendMessage(user_Id,'Ты '+user.firstname+' '+user.lastname+'?',
            BotHelper.makeButtons(['Да, это я','Нет, я его не знаю'],['yes|'+user.employee_id,'no|'+user.employee_id],2,true));
            cb(true, 1, true);
          }
        })
        break;
      case 1:
        raw = answers[ind].split('|');
        var opts = {
          message_id : msg.message.message_id,
          chat_id : msg.message.chat.id
        };
        if(raw[0] == 'yes'){
          bindBotId(raw[1], user_Id, function (err, ok, binded) {
            if(!ok){
              bot.editMessageText(err,opts)
              // bot.sendMessage(user_Id, err)
              cb(true, 0)
            } else {
              if(binded && binded != user_Id) { // ? (msg.from.username) : (msg.from.firstname ||  + )
                bot.sendMessage(binded, (msg.from.username || (msg.from.firstname || '')+' '+(msg.from.lastname || '')) +
                ' пытается украсть твой ID');
                bot.editMessageText('Извини, но у этого ID уже есть владелец.', opts)
                // bot.sendMessage(user_Id, 'Извините, но к данному ID уже привязан телеграм.')
                cb(true, 0)
              } else {
                bot.editMessageText(ok,opts)
                // bot.sendMessage(user_Id, ok)
                cb(true)
              }
            }
          })
        } else {
          cb(true, 0);
        }
        // cb(true,null,true);
        break;
      case 2: // когда человек нажимает помощь, меню
        // в этом кейсе вызываются мункции isAdmin - возвращает колбюэк если нет ошибки то второй параметр - адин (весь объект)
        // employeeInfo - возвращает нам информацию по зп
        // Ещё в зависимости от того Админ или Юзер пишет команду, вызываются определенные пункты меню
        isAdmin(user_Id, function (e,user) {
            if(e) console.log(e);
            if(/(меню)/i.test(answers[ind])){
              // bot.sendMessage(user_Id, answers[ind])
                // console.log(e, user);
                if (user){ // если админ
                    // console.log(user.admin);
                    cb(true,2)
                  } else {
                    console.log('10?');
                    cb(true,10)
                  }
                // }

            } else if (/(помощь|инфо|команд|как)/i.test(answers[ind])) {
              bot.sendMessage(user_Id, infoCommands())
              // BotHelper.drop('test', user_Id)
              console.log('info')
              cb(true,null,true)
            } else if (/\/salary (.+)/i.test(answers[ind])||(/\/зп (.+)/i.test(answers[ind]))) {
              isAdmin(user_Id, function (e,user) {
                if(user){
                  // console.log('=================================');
                  var empId = answers[ind].match(/\/salary (.+)/i);
                  if(!empId) {console.log(empId);empId = answers[ind].match(/\/зп (.+)/i);}
                  if(empId) var valid1 = true; //если есть айди
                  console.log(empId[1]);
                  var data = answers[ind].match(/\/salary (.+) (.+)/i);
                  if(!data) data = answers[ind].match(/\/зп (.+) (.+)rs/i);
                  console.log('Data is ',data);
                  if(data) {
                    var valid1 = false;
                    if(data[2].indexOf(/\.|\s|\-/i)) {
                      ///([0-9]{1,2})(\.\s\-\/)([0-9]{2}|[0-9]{4})/i.test(data[2])
                      valid2 = true;
                      console.log('Какой-то там valid2 стал тру');
                    }
                  }
                  if(valid1){
                    var d = func.dateToArray(func.Ago('months',-1))
                    employeeInfo(empId[1], d[0], d[1], function (err, info) {
                      console.log('Запустил employeeInfo: ',d[0], d[1]);
                      if(!err) { // zp info user LastMonth
                        const { firstname, employee_id, lastname } = info.employee;
                        say(user_Id, grabInfo(info.salary, 'salary', 'ID: '+employee_id+'; '+lastname+' '+firstname+'\n'), function () {
                          cb(true)
                        })
                      } else {
                        // bot.answerCallbackQuery(msg.id, 'Ошибка. Месяц или ID указан неверно.')
                        cb(true)
                      }
                    } )

                  } else if (valid2) {
                    var d = data[2].split(/\.|\s|\-/i)
                    d[1] = (d[1].length > 2) ? d[1] : '20'+d[1]
                    if(( (d[0] < 13) && (d[0] > 0 ) )  && d[1] <= new Date().getFullYear()){
                      employeeInfo(data[1],d[1], d[0]-1 , function (err, info) {
                        if(!err) { // zp info user LastMonth
                          console.log(info);
                          const { firstname, employee_id, lastname } = info.employee;
                          say(user_Id, grabInfo(info.salary, 'salary', 'ID: '+employee_id+'; '+lastname+' '+firstname+'\n'), function () {
                            cb(true)
                          })
                        } else {
                          // bot.answerCallbackQuery(msg.id, 'Ошибка. Месяц или ID указан неверно.')
                          cb(true)
                        }
                      } )
                    }
                  }
                //([0-9]{1,2})(\.\s\-\/)([0-9]{2}|[0-9]{4})
              } else {
                cb(true, 2)
              }
            })
          } else if (/\/anal (.+)/i.test(answers[ind])||(/\/аналитика (.+)/i.test(answers[ind]))) {
              isAdmin(user_Id, function (e,user) {
                if(user){
                  var empId = answers[ind].match(/\/anal (.+)/i);
                  if(!empId) empId = answers[ind].match(/\/аналитика (.+)/i);
                  if(empId){
                    var d = func.dateToArray(func.nd())
                    employeeInfo(empId[1], d[0], d[1], function (err, info) {
                      if(!err) { // zp info user LastMonth
                        const { firstname, employee_id, lastname } = info.employee;
                        say(user_Id, grabInfo(info.salary, 'anal', 'ID: '+employee_id+'; '+lastname+' '+firstname+'\n'), function () {
                          cb(true)
                        })
                      } else {
                        // bot.answerCallbackQuery(msg.id, 'Ошибка. Месяц или ID указан неверно.')
                        cb(true)
                      }
                    } )

                  }
              } else {
                cb(true,2)
              }
            })
          } else if (/\/check/i.test(answers[ind])||(/\/проверка /i.test(answers[ind]))) {
            isAdmin(user_Id, function (e,user) {
              if(e) console.log(e);
              if(user){
                checkEmployees((err, result) => {
                  // say(user_Id, result);
                  bot.sendMessage(user_Id, result)
                  cb(true);
                })
              } else {
                cb(true,2)
              }
            })
          } else if (/\/absent/i.test(answers[ind])||(/\/отсутствуют /i.test(answers[ind]))) {
            isAdmin(user_Id, function (e,user) {
              if(e) console.log(e);
              if(user){
                absentEmployees((err, result) => {
                  // say(user_Id, result);
                  bot.sendMessage(user_Id, result)
                  cb(true);
                })
              } else {
                cb(true,2)
              }
            })
          } else if (/\/info (.+)/i.test(answers[ind])) {
            isAdmin(user_Id, function (e,user) {
                if(user){
                var empId = answers[ind].match(/\/info (.+)/i);
                if(!empId) {console.log(empId+'Unkown user');}
                if(empId) {
                  console.log(empId, 254);
                  getUserInfoNew(empId[1], function (err,users) {
                    users.forEach(function(user) {
                      if(!user) {
                        bot.sendMessage(user_Id, 'Такой сотрудник не существует.\nПожалуйста, проверь правильность введённых данных.')
                        cb(true, 2, true);
                      } else {
                        // bot.sendMessage(user_Id, user.firstname+' '+user.lastname+ ' '+user.department+ ' ' +user.vacancy+ ' ' +user.gender)
                        bot.sendMessage(user_Id,'ID: ' +user.employee_id+ '\nBot ID: ' +user.botId+ '\nФИО: ' +user.lastname+ ' ' +user.firstname+' ' +user.fathername+ '\nДепартамент: ' +user.department_id+ '\nВакансия: ' +user.vacancy+ '\nПол: ' +(user.gender === 'male' ? 'Мужской' : 'Женский'))
                        cb(true, 2, true);
                      }
                    })
                  })
                }
                //([0-9]{1,2})(\.\s\-\/)([0-9]{2}|[0-9]{4})
              } else {
                cb(true,2)
              }
            })
          } else if (/\/kick (.+)/i.test(answers[ind]) ) { //для кика юзера, который по факту не в офисе
              isAdmin(user_Id, function (e,user) {
                if(user){
                  var empId = answers[ind].match(/\/kick (.+)/i);
                  if(!empId) {console.log(empId+'хз');}
                  getUserInfo(empId[1], function (err,user) {
                      if(err) {
                        console.log('user_Id',user_Id,answers[ind],err);
                        bot.sendMessage(user_Id, 'Такого ID '+answers[ind]+' не существует.\nПожалуйста, проверь правильность введённых данных.')
                      } else {
                        if(empId) var valid1 = true; //если есть айди
                          if(valid1){
                            kickUser(empId[1].toUpperCase(), user_Id)
                          }
                      }
                  })
                } else {
                  cb(true, 2)
                }
              })
          } else if (/\/kick/i.test(answers[ind]) ) { //если не указали айди для кика
            isAdmin(user_Id, function (e,user) {
              if(user) {
                    bot.sendMessage(user_Id, 'Не указан employee_id');
              } else {
                cb(true, 2)
              }
            })
        }
        })
        break;
      case 10:
        getUserInfo(user_Id, function(err, user) {
          if (user.checked === true && answers[ind] === 'autoCheckIn') {
            bot.answerCallbackQuery(msg.id, 'Ты уже зачекинен.')
          } else {
            if(answers[ind] === 'autoCheckIn') {
              var i = getIndex(70);
              Employee.find({admin: true},function (err, admins) {
                if (err) {console.log(err, 2018); cb(true, 10); return;}
                if(admins && admins.length>0){
                  var adms = [];
                  var ids= [];
                  var ans = [];
                  admins.forEach(function (admin) {
                    adms.push(admin.firstname);
                    ids.push(admin.employee_id);
                  })
                  que[i].options = BotHelper.makeButtons([...adms, 'Назад'], [...ids, 'back'], 1, true);
                  cb(true);
                }
              })
            } else {
              cb(true)
            }
          }
        })
        break;
      case 20://lunch late etc hub
        // тут происходит нечто удивительное. Если человек жмет "ланч"  то следующие кнопки для вопроса формируются под этого юзера; и тд.
        // тут используются функции noteStatus (возвращает его предупреждения)
        switch (answers[ind].toLowerCase()) {
          case 'lunch':
            getWordEndSyaAs (user_Id);
            noteStatus(user_Id, function (n) {
              console.log(n,func.IndInObjArr(que, '21', 'index')[0],'<---');
              if(n.lunch){
                que[func.IndInObjArr(que, '21', 'index')[0]].options = BotHelper.makeButtons(['Уже наел'+wordEnding, '<-- Назад'],
                ['FromLunch','back'],1,true)
              } else {
                que[func.IndInObjArr(que, '21', 'index')[0]].options = BotHelper.makeButtons(['Пойду поем', '<-- Назад'],
                ['ToLunch','back'],1,true)
              }
              cb(true);
            })
            break;
          case 'willgo':
              getWordEndElLa(user_Id);
              noteStatus(user_Id, function (n) {
                var index = func.IndInObjArr(que, '25', 'index')[0];
                if(n.gone || (n.selfGo && !n.selfGo[n.selfGo.length-1][1] )){
                  que[index].options = BotHelper.makeButtons(['Вернулся', '<-- Назад'],
                  [n.gone ? 'came': 'cameself','back'],1,true)
                  que[index].data = 'Вернулся?'
                } else {
                  que[index].options = BotHelper.makeButtons(['Личные дела','Пош'+wordEndElLa+' в банк', 'Пош'+wordEndElLa+' на почту', 'Пош'+wordEndElLa+' в налоговый комитет', '<-- Назад'],
                                                                                               ['self','bank', 'mail', 'nalog', 'back'],2,true)
                  que[index].data = 'Почему ты уходишь? :C'

                }
                cb(true);
              })
              break;
          case 'early':
            noteStatus(user_Id, function (n) {
                if(answers[ind] != 'back'){
                  var index = func.IndInObjArr(que, '22', 'index')[0];
                  var noted = [false, false]
                  console.log(n);
                  n.forEach(function (note, i) {
                    if(note.nedorabotaiu){
                      noted[i] = true;
                    }
                  })
                  var op = {}
                  if(noted[0] && noted[1]){
                    bot.answerCallbackQuery(msg.id, 'Офигел? Ты же уже отпрашивался на сегодня!')
                    op = BotHelper.makeButtons(['<-- Назад'],
                                                   ['back'],1,true)
                  }
                  if(!noted[0] && noted[1]){
                    bot.answerCallbackQuery(msg.id, 'Офигел? Ты же уже отпрашивался на завтра!')
                    op = BotHelper.makeButtons(['Сегодня', '<-- Назад'],
                                                   ['today','back'],1,true)
                  }
                  if(noted[0] && !noted[1]){
                    bot.answerCallbackQuery(msg.id, 'Офигел? Ты же уже отпрашивался на сегодня!')
                    op = BotHelper.makeButtons(['Завтра', '<-- Назад'],
                                                   ['tomorrow','back'],1,true)
                  }
                  if(!noted[0] && !noted[1]){
                    op = BotHelper.makeButtons(['Сегодня','Завтра', '<-- Назад'],
                                                   ['today','tomorrow','back'],2,true)
                  }

                  console.log(n, answers[ind], noted);
                  que[index].options = op
                  cb(true)

                }
              },[func.nd(), func.Ago('days', 1)] )
              break
            // case ''
          default:
          cb(true)
        }
        break;
      case 21://lunch
        if(answers[ind] != 'back'){
          goToLunch(user_Id, /ToLunch/i.test(answers[ind]))
          bot.answerCallbackQuery(msg.id, /ToLunch/i.test(answers[ind]) ? 'Не подавись! Кушай хорошо — работы много!' : 'Плодотворной работы! Дедлайн не за горами...')
        }
        cb(true);
        break;
      // case 22:
      //   noteStatus(user_Id, function (n) {
      //     if(answers[ind] != 'back'){
      //       if(answers[ind] == 'today'){
      //         var index = func.IndInObjArr(que, '23', 'index')[0];
      //
      //       } else {
      //         var index = func.IndInObjArr(que, '24', 'index')[0];
      //
      //       }
      //       console.log(n, answers[ind]);
      //       cb(true)
      //
      //     }
      //   },(answers[ind] == 'today') ? func.dateToArray() : func.dateToArray( func.Ago('days', 1) ) )
      //   break;
      case 23://early
        //
        if(answers[ind] != 'back'){
          leaveEarly(user_Id, 'сегодня', answers[ind])
          bot.answerCallbackQuery(msg.id, 'Не забудь отработать выходной. Много тусить нельзя!')
        }
        cb(true)
        break;
      case 24://early
        if(answers[ind] != 'back'){
          console.log(answers[ind], 200);
          leaveEarly(user_Id, 'завтра', answers[ind])
          bot.answerCallbackQuery(msg.id, 'Не забудь отработать выходной. Много тусить нельзя!')
        }
        cb(true)
        break;
      case 25://`ill go`
        if(answers[ind] != 'back'){
          alertAdmins(user_Id, answers[ind], msg)
        }
        cb(true)
        break;
      case 26://Late
        if(answers[ind] != 'back'){
          iWillLate(user_Id, answers[ind],function (err) {
            if(err)
            bot.answerCallbackQuery(msg.id, 'Извините, но сейчас Уже поздно отпрашиваться')
            else
            bot.answerCallbackQuery(msg.id, 'Видимо, ночь выдалась нелегкой, раз опаздываешь. Не забудь отработать!')
          } )
        }

        cb(true)
        break;
      case 27://Late
        if(answers[ind] != 'back'){
          bot.answerCallbackQuery(msg.id, 'Видимо, ночь выдалась нелегкой, раз опаздываешь. Не забудь отработать!')
          getUserInfo(user_Id, function (err, user) {
            SendMsgToAdmins('Босс, '+user.firstname+' '+ user.lastname +' говорит что '+answers[ind]+' не придет! Накажите его потом.')
            // cb(true)
          })

        }

        cb(true)
        break;
      case 36:// alerts
        if(answers[ind] != 'back'){
          if(answers[ind] == 'alerts') {
            var d = func.dateToArray(func.nd())
            noteStatus(user_Id,function (note) {
              // console.log(note);
              if(note.reasons&&note.reasons.length > 0){
                say(user_Id,note.reasons.join('\n'),function () {
                  cb(true,10)
                })
              } else {
                bot.answerCallbackQuery(msg.id, 'На сегодня нет оповещений')
                cb(true,10)

              }
            })
          } else if (answers[ind] == 'salary') {
            cb(true)
          }
        }  else {cb(true)}
        break;
      case 37://anal
        // когда кто-то хочет посмотрть аналитику  вызывается этот кейс
        if(answers[ind] != 'back'){
          if(answers[ind] == 'prediction') {
            var d = func.dateToArray(func.nd())
            employeeInfo(user_Id,d[0], d[1], function (err, info) {
              if(!err) { // ANAL
                say(user_Id, grabInfo(info.salary, 'anal', 'На текущий месяц ты:\n'),function () {
                  cb(true)
                })
              } else {
                bot.answerCallbackQuery(msg.id, 'Ты такой раздолбай, что на текущий месяц у тебя нет данных!.')
                cb(true)
              }
            })
          } else {cb(true)}
        }  else {cb(true)}
        break;
      case 38://salaryInfo
        // вызывается когда кто-то хочет посмотреть инфо по зп на определенный месяц или предыдущий месяц
        if(answers[ind] != 'back'){
          if(answers[ind] != 'lastmonth'){//&& answers[ind].indexOf(/\.|\s|\-/i) > 0) {
            var d = answers[ind].split(/\.|\s|\-/i)
            console.log(d,' <------------------Splited D');
            d[1] = (d[1].length > 2) ? d[1] : '20'+d[1]
            if(( (d[0] < 13) && (d[0] > 0 ) )  && d[1] <= new Date().getFullYear()){
              employeeInfo(user_Id,d[1], d[0]-1 , function (err, info) {
                if(!err) { // zp info user
                  bot.sendMessage(user_Id, grabInfo(info.salary, 'salary'))
                  cb(true)
                } else {
                  // bot.answerCallbackQuery(msg.id, 'Не забудьте отработать!')
                  cb(true)
                }
              } )
            }
          } else {
            var d = func.dateToArray(func.Ago('months',-1))
            console.log(d, '< ===========        LastMonth D ');
            employeeInfo(user_Id, d[0], d[1], function (err, info) {
              if(!err) { // zp info user LastMonth
                // console.log(JSON.stringify(info));
                say(user_Id, grabInfo(info.salary, 'salary'),function () {
                  // console.log('--------------------->>>>>>',info.salary.bonus);
                  cb(true)
                })
              } else {
                bot.answerCallbackQuery(msg.id, 'В твоем деле нет информации за предыдущий месяц')
                cb(true)
              }
            } )
          }

        } else {cb(true)}
        break;
      case 39://alertsMenu
        if(answers[ind] != 'back'){
          var d = func.dateToArray(func.nd())


            switch (answers[ind]) {
              case 'today':
                noteStatus(user_Id, function (note) {
                  var result = 'Сегодня ты меня оповестил о том, что: \n'
                  var r = ''
                  for (var n in note) {
                    if (note.hasOwnProperty(n)) {
                      switch (n) {
                        case 'gone':
                          r = 'Ушел по делам'
                          break;
                        case 'nedorabotaiu':
                          r = 'Уйду пораньше'
                          break;
                        default:
                          r=n
                      }
                      result += r + '\n'

                    }
                  }
                  bot.sendMessage(user_Id, result).then(function () {
                    cb(true)
                  }).catch(function () {
                    cb(true)
                  })
                })

                break;
              default:
                cb(true)
            }


            cb(true)

        }  else {cb(true)}
        break;
      case 52:
        //   que[i].data = answers[0];
        if(answers[ind] == 'back'){
          cb(true);
          return;
        }
        var i = getIndex(53);
        Departments.find({},function (err, departments) {
          if(err) {console.log(err); cb(true, 52); return;}
          if(departments.length>0){
            var deps = [];
            var ids= [];
            departments.forEach(function (dep) {
              deps.push(dep.name);
              ids.push(dep._id);
            })
            // console.log(deps,ids);
            que[i].data = answers[50]+', выберите, пожалуйста, ваш департамент';
            que[i].options = BotHelper.makeButtons([...deps, 'Назад'], [...ids, 'back'], 1, true);
            cb(true);
          }
        })
        break;
      case 53:
        var i = getIndex(54);
        que[i].data = answers[50]+', укажите, пожалуйста, вашу должность:';
        cb(true);
        break;
      case 54:
        if(answers[ind] == 'back'){
          var i = getIndex(53);
          Departments.find({},function (err, departments) {
            if(err) { console.log(err); cb(true, 2); return;}
            if(departments.length>0){
              var deps = [];
              var ids= [];
              departments.forEach(function (dep) {
                deps.push(dep.name);
                ids.push(dep._id);
              })
              que[i].options = BotHelper.makeButtons([...deps, 'Назад'], [...ids, 'back'], 1, true);
              cb(true);
            }
          })
          // que[i].options = [deps,'Назад']
        } else {
          // var i = getIndex(55);
          // que[i].data = 'А теперь, '+answers[0]+', введите, пожалуйста, ваши компетенции (каждую компетенцию существительным из одного слова) через запятую \nНапример: 1C, продажи, переговоры, ...';
          cb(true)
        }
          break;
      case 55:
        var i = getIndex(57);
        que[i].data = answers[50]+', выберите, пожалуйста, ваш пол';
        que[i].options = BotHelper.makeButtons(['М','Ж', 'Назад'], ['male','female', 'back'], 2, true);
        cb(true);
        break;
      case 56:
        Departments.findOne({_id: answers[53]}, function(err, department){
          if(err) {console.log(err); cb(true, 56)}
          if(department){
            var year =( answers[55].toString().length>2)?(answers[55].toString()[2]+''+answers[55].toString()[3]):answers[55];
            Employee.count({employee_id: new RegExp(year+department.short_name,'i')}, function(err, count){
              // console.log();
              var employee = new Employee({
                employee_id: year+department.short_name.toUpperCase()+fmt(count+1),
                botId: user_Id,
                firstname: answers[50],
                lastname: answers[51],
                fathername: answers[52],
                department: answers[53],
                department_id: department.short_name,
                vacancy: answers[54],
                gender: answers[57],
                salary_fixed: answers[56]*0.25,
                salaryFull: answers[56]
              }).save((err, savedEmployee)=>{
                if(err) {console.log(err); cb(true, 56)}
                if(savedEmployee){
                  department.employees.push(savedEmployee._id);
                  department.save(function(err, savedDepartment){
                    if(err) {console.log(err); cb(true, 56)}
                    if(savedDepartment){
                      console.log(savedDepartment)
                      say(user_Id, 'Спасибо, '+savedEmployee.firstname+ '!  Ваш ID:'+ savedEmployee.employee_id)
                    }
                  })
                  cb(true,2);
                }
              })


            })
          }
        })
        break;
      case 70:
        // bot.answerCallbackQuery(msg.id, 'Ждите ответа от админа.')
        getUserInfo(user_Id, function(err, user) {
          if(user) {
            var messageToAdmin = user.firstname + ' говорит что ходит по вашим делам. Сделать ли ему авточекин?';
            if (answers[70] === 'back') {
              cb(true, 10)
            } else if (answers[70] !== 'back') {
              bot.answerCallbackQuery(msg.id, 'Ждите ответа от админа.')
              Employee.findOne({employee_id: answers[70]}, function (err, admin) {
                bot.sendMessage(admin.botId, messageToAdmin, BotHelper.makeButtons(['Да', 'Нет'], ['yes', 'no'], 2, true));
                bot.once('callback_query', function(msg) {
                  switch (msg.data) {
                    case 'yes':
                      if (user.checked === false) {
                          CheckMe(user.employee_id, true, function(s, m) {
                            console.log(s, m);
                          }, null, true);
                        }
                        cb(true, 2, true)
                      break;
                    case 'no':
                      bot.sendMessage(user.botId, 'Авточекин не одобрен.')
                      bot.answerCallbackQuery(msg.id, 'Авточекин не одобрен.')
                      bot.sendMessage(admin.botId, 'Ты не дал добро на авточекин.')
                      cb(true, 2, true)
                      break;
                    default:
                      break;
                  }
                })
              })
              cb(true, 10)
            }
          } else {
              cb(true, 10)
          }
        })
        break;
      case 100:

        break;
      default:
      // bot.answerCallbackQuery(msg.id, 'Index:'+ind+ ' Answer: '+answers[ind])
      console.log('Index:'+ind, 'Answer: '+answers[ind]);
      cb(true)

    }

  },
  function (user_Id,answers,cb,msg) {
    // этот колбэк не должен вызываться. Он вызывается если пункты меню закончились (при ошибке в коде)
    // в любом случае человека перенаправляет обратно во второй вопрос
    console.log('end.')
    cb(2);
  },
  function (user_Id,force,answers) {
    sendMessage(user_Id, 'Поздравляю, вы успешно уволились.')
  }

)




// добавляет департаменты
// function funct() {
//   var d = [
//           {
//               "name": "Департамент программной разработки",
//               "short_name": "it"
//           },
//           {
//               "name": "Департамент дизайна",
//               "short_name": "ds"
//           },
//           {
//               "name": "Департамент контент-маркетинга",
//               "short_name": "cw"
//           }
//       ]
//   var department = {}
//   d.forEach(function (sh,i) {
//     department.name = sh.name;
//     department.short_name = sh.short_name;
//     var dep = new departments(department).save()
//     console.log('saved',i);
//   })
// }
