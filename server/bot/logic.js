/**
 * Основная логика бота, которая обрабатывает входящие сообщения и маршрутизирует логику в файлы Helper
 *
 * @module bot/logic
 */

const { bot } = require('./initBot');
const dbmanager = require('../database/dbmanager');
const employee = require('./helper/employee');
const check = require('./helper/messageParser');
const keyboards = require('./helper/keyboards');
const strings = require('./helper/strings');

require('./helper/warnBoss');


bot.on('message', msg => {
  if (!msg) return;
  else if (!msg.from.username) {
    Employee.sendAskForUsername(bot, msg);
    return;
  }
  employee.textInputCheck(msg, (isTextInput, employee) => {
    if (employee) {
      if (employee.disabled) {
        employee.sendBanMessage(bot, msg);
        return;
      }

      if (isTextInput) {
        global.eventEmitter.emit(((msg.text === strings().cancel) ? 'cancel' : '') + isTextInput, { msg, employee, bot });
      } else if (check.replyMarkup(msg)) {
        handleKeyboard(msg);
      } else if (check.botCommandStart(msg)) {
        keyboards.sendMainMenu(bot, msg.chat.id, false);
      } else {
        console.log('Команда не найден');
        keyboards.sendMainMenu(bot, msg.chat.id, false);
        // bot.sendMessage(-1001236556632, `@${user.username} sent strange message:\n\n\`${msg.text}\``, {parse_mode: 'Markdown'});
      }
    } else {
      bot.sendMessage(msg.from.id, 'Register')
    }
  });
});

/**
 * Fired when user clicks button on inlline keyboard
 *
 * @param {Telegram:Message} msg - Message that gets passed from user and info about button clicked
 */
bot.on('callback_query', (msg) => {
  if (!msg.from.username) {
    profile.sendAskForUsername(msg);
    return;
  }

  dbmanager.findEmployee({ botId: msg.from.id })
    .then((employee) => {
      if (employee.disabled) {
        employee.sendBanMessage(msg);
        return;
      }

      const options = msg.data.split(strings().inlineSeparator);
      const inlineQuery = options[0];
      global.eventEmitter.emit(inlineQuery, { bot, msg, employee });
    })
    .catch(/** todo: handle error */);
});

bot.on('inline_query', (msg) => {
  dbmanager.findEmployee({ botId: msg.from.id })
    .then((user) => {
      const results = [{
        type: 'article',
        id: `${getRandomInt(1000000000000000, 999999999999999999)}`,
        title: strings().shareProfile,
        input_message_content: {
          message_text: user.getTextToShareProfile(),
        },
      }];

      const opts = {
        cache_time: 60,
        is_personal: true,
      };

      bot.answerInlineQuery(msg.id, results, opts)
        .catch(/** todo: handle error */);
    })
    .catch(/** todo: handle error */);
});



/**
 * Handler for custom keyboard button clicks
 *
 * @param {Telegram:Message} msg - Message that is passed with click and keyboard option
 */
function handleKeyboard(msg) {
  const text = msg.text;
  const mainMenuOptions = strings().mainMenuOptions;
  const employeeMenuOptions = strings().employeeMenuOptions;

  switch (text) {
    case mainMenuOptions.menu:
      keyboards.sendEmployeeMenu(bot, msg.chat.id);
      break;
    case mainMenuOptions.help:
      keyboards.sendHelp(bot, msg.chat.id);
      break;
    case employeeMenuOptions.back:
      keyboards.sendMainMenu(bot, msg.chat.id);
      break;
    default:
      keyboards.sendMainMenu(bot, msg.chat.id);
  }

}
