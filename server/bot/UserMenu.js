var BotHelper = require('./helper/BotFunctions');
var lunchMenu = [//21
  {index: 21, // lunch
    qType: 'text',
    options: BotHelper.makeButtons(['Пойду поем','Уже наелся', '<-- Назад'],
                                   ['ToLunch','FromLunch','back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^ToLunch/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^FromLunch/i,
        next: 20 // индекс инфо по зар.плате admin
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
  ],
    data: 'Чё там с обедом?.',
    err: 'Балда, ты на кнопку нажми сначала!.'
  }
]
var earlyMenu = [// 22 - 23
  {index: 22, //
    qType: 'text',
    options: BotHelper.makeButtons(['Сегодня','Завтра', '<-- Назад'],
                                   ['today','tomorrow','back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^today/i
      },
      {
        AnsType: 'inline',
        reg:/^tomorrow/i,
        next: 24
      },
      // {
      //   AnsType: 'text',
      //   reg: /(^[0-3]{1})([0-9]{1})\ ([0-1]{1})([0-9]{1})\ ([1-2]{1})([0,9]{1})([0-9]{2}$)/i
      // },{
      //   AnsType: 'text',
      //   reg: /(^[0-3]{1})([0-9]{1})\-([0-1]{1})([0-9]{1})\-([1-2]{1})([0,9]{1})([0-9]{2}$)/i
      // },{
      //   AnsType: 'text',
      //   reg: /(^[0-3]{1})([0-9]{1})\.([0-1]{1})([0-9]{1})\.([1-2]{1})([0,9]{1})([0-9]{2}$)/i
      // },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
    ],
    data: 'Когда уходить собрался?',
    err: 'Балда, ты на кнопку нажми сначала!'
  },
  {index: 23, // time
    qType: 'text',
    data: 'На сколько раньше?',
    err: 'Балда, ты на кнопку нажми сначала!',
    options: BotHelper.makeButtons(['1 час','2 часа', '3 и более', '<-- Назад'],
                                   ['1 час', '2 часа', '3 и более часов', 'back'],3,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/[0-9]/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 22 // <-
      }
    ]
  },
  {index: 24, // time
    qType: 'text',
    data: 'На сколько раньше?',
    err: 'Балда, ты на кнопку нажми сначала!',
    options: BotHelper.makeButtons(['1 час','2 часа', '3 и более', '<-- Назад'],
                                   ['1 час', '2 часа', '3 и более часов', 'back'],3,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/[0-9]/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 22 // <-
      }
    ]
  }
]
var lateMenu = [//24
  {index: 26, // time
    qType: 'text',
    data: 'Чё там, на сколько опаздываешь?',
    err: 'Балда, ты на кнопку нажми сначала!',
    options: BotHelper.makeButtons(['15 минут','1 час', '3 часа и более', '<-- Назад'],
                                   ['15 минут','1 час', '3 часа и более', 'back'],3,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^[0-9]/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
    ]
  }

]
var willGo = [//25
  {index: 25, //
    qType: 'text',
    options: BotHelper.makeButtons(['Личные дела','Пошел в банк', 'Пошел на почту', 'Пошел в налоговый комитет', '<-- Назад'],
                                   ['self','bank', 'mail', 'nalog', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/came/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/self||bank||mail||nalog/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'text',
        next: 20
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
  ],
    data: 'Почему ты уходишь? Нажми на кнопку или напиши.',
    err: 'Балда, ты на кнопку нажми сначала или напиши чё там!'
  },
  {index: 27, // time
    qType: 'text',
    data: 'Когда не придешь?',
    err: 'Балда, ты на кнопку нажми!',
    options: BotHelper.makeButtons(['Сегодня','Завтра', '<-- Назад'],
                                   ['Сегодня','Завтра', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/(.*)/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
    ]
  }
]
var alertMenu = [//21-25
  ...lunchMenu,
  ...earlyMenu,
  ...lateMenu,
  ...willGo
]
var salaryIndex = 37;
var salaryMenu = [
  {index: 37, // time
    qType: 'text',
    data: 'Чего хотел?',
    err: 'Балда, ты на кнопку нажми сначала!',
    options: BotHelper.makeButtons(['ЗП за месяц', 'Прогноз ЗП', '<-- Назад'],
                                   ['sal_month','prediction', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/(sal_month)/i,
        next: 38 // индекс salary
      },
      {
        AnsType: 'inline',
        reg:/(prediction)/i,
        next: 36 // aler
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 36 // <-
      }
    ]
  },
  {index: 38, // time
    qType: 'text',
    data: 'За какой месяц хочешь отчёт? Напиши в формате: "ММ.ГГ"',
    err: 'Формат : ММ.ГГ (04.17), придурок!',
    options: BotHelper.makeButtons(['Прошедший месяц', '<-- Назад'], // generate
                                   ['lastmonth', 'back'],1,true),
    answers:[
      {
        AnsType: 'text',
        reg:/(.+)/i,
        next: 36 // индекс salary
      },
      {
        AnsType: 'inline',
        reg:/(lastmonth)/i,
        next: 36 // aler
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 36 // <-
      }
    ]
  }
]
var alertsIndex = 39;
var alertsMenu = [
  {index: 39, // time
    qType: 'text',
    data: 'Выберите период.',
    err: 'Балда, ты на кнопку нажми сначала!',
    options: BotHelper.makeButtons(['За сегодня', 'За текущий месяц', 'За всё время', '<-- Назад'],
                                   ['today', 'thisMonth', 'allTime', 'back'],3,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^(today|thisMonth|allTime)/i,
        next: 36 // индекс salary
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 36 // <-
      }
    ]
  }
]
var kosyakIndex = 40;
var kosyakMenu = [
  {index: 40, // time
    qType: 'text',
    data: 'Выберите период.',
    err: 'Балда, ты на кнопку нажми сначала!',
    options: BotHelper.makeButtons(['За текущий месяц', 'За прошлый месяц', '<-- Назад'],
                                   ['thisMonth', 'lastMonth', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^(lastMonth|thisMonth|allTime)/i,
        next: 36 // индекс salary
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 36 // <-
      }
    ]
  }
]
var basicInfo = [
  {index: 36, // time
    qType: 'text',
    data: 'Чего хотел?',
    err: 'Балда, ты на кнопку нажми сначала!',
    options: BotHelper.makeButtons(['Моя ЗП', 'Мои оповещения', /*'Мои недоработки/опаздания',*/ '<-- Назад'],
                                   ['salary','alerts', /*'kosyaki',*/  'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/(salary)/i,
        next: salaryIndex // индекс salary
      },
      {
        AnsType: 'inline',
        reg:/(alerts)/i,
        next: alertsIndex // aler
      },
      {
        AnsType: 'inline',
        reg:/(kosyaki)/i,
        next: kosyakIndex // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 10 // <-
      }
    ]
  }
]
var analMenu = [//35 - 45
  ...basicInfo,
  ...salaryMenu,
  ...alertsMenu
]

var autoCheckInMenu = [
  {index: 70, // time
    qType: 'text',
    data: 'По чьим делам ходишь?',
    err: 'Балда, ты на кнопку нажми сначала!',
    options: {},
    answers:[
      {
        AnsType: 'inline',
        next: 10
      }
    ]
  }
]

var menu = [//10,20-26
  {index: 10, // user menu after rabota button
    qType: 'text',
    options: BotHelper.makeButtons(['Предупредить Биг Босса','Чё там ЗП', 'Авточекин', '<-- В главное меню'],
                                   ['alert','salaryInfo','autoCheckIn','back'],1,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^alert/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^salaryInfo/i,
        next: 36 // индекс инфо по зар.плате Юзера
      },
      {
        AnsType: 'inline',
        reg:/^autoCheckIn/i,
        next: 70
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 2 // <-
      }
    ],
    data: 'Выбери действие',
    err: 'Балда, ты на кнопку нажми сначала!'
  },
  {index: 20, // оповещание юзер
    qType: 'text',
    options: BotHelper.makeButtons(['Обед','Опоздаю', 'Ухожу по делам', 'Уйду пораньше', 'Не приду', '<-- вернуться'],
                                   ['lunch', 'will_late', 'willGo', 'early','not come', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^lunch/i,
        next: 21 //
      },
      {
        AnsType: 'inline',
        reg:/^not come/i,
        next: 27
      },
      {
        AnsType: 'inline',
        reg:/^will_late/i,
        next: 26 //
      },
      {
        AnsType: 'inline',
        reg:/^willgo/i,
        next: 25 //
      },
      {
        AnsType: 'inline',
        reg:/^early/i,
        next: 22 //
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 10 //
      }
    ],
    data: 'Выбери действие',
    err: 'Балда, ты на кнопку нажми сначала!'
  },
  ...alertMenu,
  //
  ...analMenu,
  //
  ...autoCheckInMenu
]


module.exports = menu
