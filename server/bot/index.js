/**
 * Arbeit Telegram Bot executable file;
 * Setup mongoose and starts logic.js.
 *
 * @module app
 */

/** Dependencies */
const events = require('events');

// noinspection JSAnnotator
global.eventEmitter = new events.EventEmitter();

/** Connect DB */
require('../database/connect');

/** Start bot */
require('./logic');


console.log('Bot has been started...');
