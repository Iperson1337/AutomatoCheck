const TelegramBot = require('node-telegram-bot-api');
const config = require('../config')

var bot = new TelegramBot(config.telegram_bot.token, {
  polling: true
});

module.exports.bot = bot;
