const strings = require('./strings')
const dbmanager = require('../../database/dbmanager')
const keyboards = require('./keyboards');
const { matchInObjArr, dateToArray, comp, nd, Ago} = require('../../helpers/AllFunctions')
const { SendMsgToAdmins } = require('../../app/Repository/admin')
require('./employee')

/**
 * Отраватывается когда Сотрудник Нажмиает Предупредить Биг Босса
 * Оправлает Опций Предупредить Биг Босса
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.warnBossInline, ({ bot, msg }) => {
  sendWarnOptions(bot, msg)
})

/**
 * Отраватывается когда Сотрудник Нажмиает Обед
 * Оправлает Опций Обед
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.sendLunchOptionsInline, ({ bot, msg, employee }) => {
  console.log('Event Обед');
  sendLunchOptions(bot, msg, employee)
})

/**
 * Отраватывается когда Сотрудник Нажмиает Я ушел на обед
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.igotoLunchInline, ({ bot, msg, employee }) => {
    goToLunch(employee, true)
      .then(result => {
        if (result.success) {
          SendMsgToAdmins(`${employee.firstname} ${employee.lastname} (${employee.employee_id}) ${strings(employee).LunchMessagesForAdmin.wentToLunch}`)
          bot.answerCallbackQuery(msg.id, strings().LunchAnswerCallback.iwilleat)
        }
      })
      .then(sendWarnOptions(bot, msg))
      .catch(err => {
        console.log('======igotoLunchInline======');
        console.log(err);
        console.log('======END igotoLunchInline======');
        bot.answerCallbackQuery(msg.id, strings().error)
      })
})

/**
 * Отраватывается когда Сотрудник Нажмиает  Я наелся(-лась)
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.icomeFromLunchInline, ({ bot, msg, employee }) => {
    goToLunch(employee, false)
      .then(result => {
        if(result.success) {
          SendMsgToAdmins(`${employee.firstname} ${employee.lastname} (${employee.employee_id}) ${strings(employee).LunchMessagesForAdmin.cameFromLanch}`)
          bot.answerCallbackQuery(msg.id, strings().LunchAnswerCallback.alreadyAte)
        }
      })
      .then(sendWarnOptions(bot, msg))
      .catch(err => {
        console.log(err, 'Error');
        bot.answerCallbackQuery(msg.id, strings().error)
      })
})

/**
 * Отраватывается когда Сотрудник Нажмиает  Опаздаю
 * Оправлает Опций Опаздаю
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.sendLateOptionsInline, ({ bot, msg, employee }) => {
  sendLateOptions(bot, msg)
})

/**
 * Отраватывается когда Сотрудник Вабрал времия на сколько опаздает
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.beLateForInline, ({ bot, msg, employee }) => {
  let latefor = msg.data.split(strings().inlineSeparator)[1] // на сколька опаздает
  iWillLate(employee, latefor)
    .then(result => {
      if (result.success)
        bot.answerCallbackQuery(msg.id, 'Видимо, ночь выдалась нелегкой, раз опаздываешь. Не забудь отработать!')
      else
        bot.answerCallbackQuery(msg.id, 'Извините, но сейчас Уже поздно отпрашиваться')
    })
    .then(sendWarnOptions(bot, msg))
    .catch(err => {
      bot.answerCallbackQuery(msg.id, strings().error)
      console.log(err);
    })
})

/**
 * Отраватывается когда Сотрудник нажимает Ухожу По делам
 * Оправлает Опций Ухожу По делам
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.sendiWentInline, ({ bot, msg, employee }) => {
  sendWentOptions(bot, msg, employee)
})

/**
 * Отраватывается когда Сотрудник Выбрал куда уходить
 * отправляет Админам оповещение куда уходить
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.iWentInline, ({ bot, msg, employee }) => {
  let whereGo = msg.data.split(strings().inlineSeparator)[1]
  iWentTo(employee, whereGo, false)
    .then(result => {
      if(result.success) {
        bot.answerCallbackQuery(msg.id, `Ты сообщил БигБоссу, что ${result.messageForEmployee}`)
        SendMsgToAdmins(`${employee.firstname} ${employee.lastname} (${employee.employee_id}) Оповещает вас, что ${result.messageForAdmin}`)
      }
    })
    .then(sendWarnOptions(bot, msg))
    .catch(err => {
      bot.answerCallbackQuery(msg.id, strings().error)
      console.log(err);
    })
})

/**
 * Отраватывается когда Сотрудник уходил по личным делам и Нажмиает Вернулся
 * Сохраняет
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.iambackSelfInline, ({ bot, msg, employee }) => {
  let whereGo = msg.data.split(strings().inlineSeparator)[1]
  iWentTo(employee, whereGo, true)
    .then(result => {
      if(result.success) {
        bot.answerCallbackQuery(msg.id, `Ты сообщил БигБоссу, что ${result.messageForEmployee}`)
        SendMsgToAdmins(`${employee.firstname} ${employee.lastname} (${employee.employee_id}) Оповещает вас, что ${result.messageForAdmin}`)
      }
    })
    .then(sendWarnOptions(bot, msg))
    .catch(err => {
      bot.answerCallbackQuery(msg.id, strings().error)
      console.log(err);
    })

})

/**
 * Отраватывается когда Сотрудник Нажмиает Уйду пораньше
 * отправляет пикер День
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.sendLeaveEarlyInline, ({ bot, msg, employee }) => {
  sendLeaveEarlyDayPicker(bot, msg, employee)
})

/**
 * Отраватывается когда Сотрудник Выбрал дату
 * отправляет пикер времия
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.iwillLeaveEarlyDayInline, ({ bot, msg, employee }) => {
  let day = msg.data.split(strings().inlineSeparator)[1]
  sendLeaveEarlyTimePicker(bot, msg, employee, day)
})

/**
 * Отраватывается когда Сотрудник Выбрал Время
 * отправляет Админам собщение оповещение когда и насколько пораньше уйдёт
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.iwillLeaveEarlyTimeInline, ({ bot, msg, employee }) => {
  let minut = msg.data.split(strings().inlineSeparator)[1]
  let day = msg.data.split(strings().inlineSeparator)[2]
  leaveEarly(employee, day, minut)
    .then(result => {
      if (result.success) {
        SendMsgToAdmins(employee.firstname+' '+employee.lastname + ' (' +employee.employee_id +') говорит что '+day+' уйдёт пораньше на '+minut  )
        bot.answerCallbackQuery(msg.id, 'Не забудь отработать выходной. Много тусить нельзя!')
      }
    })
    .then(sendWarnOptions(bot, msg))
    .catch(err => {
      bot.answerCallbackQuery(msg.id, strings().error)
      console.log(err);
    })
})


/**
 * Отраватывается когда Сотрудник нажимает Не приду
 * отправляет Picker Day
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.sendNotComeInline, ({ bot, msg, employee }) => {
  sendNotComeDayPicker(bot, msg, employee)
})
/**
 * Отраватывается когда Сотрудник выбрал день когда не придеть
 * отправляет Админам собщение оповещение когда не придет
 * @param {Telegram:Bot} bot - Бот, который должен ответить
 * @param {Telegram:Messager} msg - Message received
 */
global.eventEmitter.on(strings().actionTypes.iNotComeDayInline, ({ bot, msg, employee }) => {
  let day = msg.data.split(strings().inlineSeparator)[1]
  sendWarnOptions(bot, msg)
  bot.answerCallbackQuery(msg.id, 'Видимо, ночь выдалась нелегкой, раз опаздываешь. Не забудь отработать!')
  SendMsgToAdmins(`Босс, ${employee.getFullName()} говорит что не придет! Накажите его потом.`)

})


// *** Return Inline Keybords *** //

const warnOptionsInlines = [
  [
    { text: strings().warnOptions.igotoLunch, callback_data: strings().actionTypes.sendLunchOptionsInline },
    { text: strings().warnOptions.iwillLate, callback_data: strings().actionTypes.sendLateOptionsInline }
  ],
  [
    { text: strings().warnOptions.iwillgo, callback_data: strings().actionTypes.sendiWentInline },
    { text: strings().warnOptions.iwillLeaveEarly, callback_data: strings().actionTypes.sendLeaveEarlyInline }
  ],
  [
    { text: strings().warnOptions.iwillNotCome, callback_data: strings().actionTypes.sendNotComeInline },
    { text: strings().back, callback_data: strings().actionTypes.employeeMainMenu }
  ]

]

/**
 * @param {Mongoose:Employee} employee - Бот, который должен ответить
 * @param {Object} note - Опавещения на сегоднешный день
 * @return {Array} keyboard - Inline keyboard
 */
function LunchOptionsInlines(employee, note) {
  let keyboard = [];
  if(note.lunch) {
    keyboard = [
      [{ text: strings(employee).LunchOptions.alreadyAte, callback_data: strings().actionTypes.icomeFromLunchInline }],
      [{ text: strings().back, callback_data: strings().actionTypes.warnBossInline }]
    ]
  } else {
    keyboard = [
      [{ text: strings().LunchOptions.iwilleat, callback_data: strings().actionTypes.igotoLunchInline }],
      [{ text: strings().back, callback_data: strings().actionTypes.warnBossInline }]
    ]
  }
  return keyboard
}

const LateOptionsInlines = [
    [
      { text: strings().timePicker.min15, callback_data: strings().actionTypes.beLateForInline + strings().inlineSeparator + strings().timePicker.min15 },
      { text: strings().timePicker.hour1, callback_data: strings().actionTypes.beLateForInline +strings().inlineSeparator + strings().timePicker.hour1  },
      { text: strings().timePicker.hour3OrMore, callback_data: strings().actionTypes.beLateForInline + strings().inlineSeparator + strings().timePicker.hour3OrMore }
    ],
    [
      { text: strings().back, callback_data: strings().actionTypes.warnBossInline }
    ]
]

/**
 * @param {Mongoose:Employee} employee - Бот, который должен ответить
 * @param {Object} note - Опавещения на сегоднешный день
 * @return {Array} keyboard - Опций Ухожу по делам
 */
function iWentOptionsInlines(employee, note) {
  let keyboard = []
  if (note.gone || note.selfGo && !note.selfGo[note.selfGo.length-1][1]) {
    let iamback = note.gone ? strings().actionTypes.iWentInline : strings().actionTypes.iambackSelfInline
    keyboard = [
      [
        {
          text: strings(employee).iWentOptions.returned,
          callback_data: iamback + strings().inlineSeparator + strings().whereWentOptions.ireturned
        }
      ],
      [
        { text: strings().back, callback_data: strings().actionTypes.warnBossInline }
      ]
    ]
  } else {
    keyboard = [
        [
          { text: strings(employee).iWentOptions.personalThings, callback_data: strings().actionTypes.iWentInline + strings().inlineSeparator + strings().whereWentOptions.iwentPersonalThings},
          { text: strings(employee).iWentOptions.bank, callback_data: strings().actionTypes.iWentInline + strings().inlineSeparator + strings().whereWentOptions.iwentBank }
        ],
        [
          { text: strings(employee).iWentOptions.postOffice, callback_data: strings().actionTypes.iWentInline + strings().inlineSeparator + strings().whereWentOptions.iwentPostOffice },
          { text: strings(employee).iWentOptions.taxCommitte, callback_data: strings().actionTypes.iWentInline + strings().inlineSeparator + strings().whereWentOptions.iwentTaxCommitte  }
        ],
        [{ text: strings().back, callback_data: strings().actionTypes.warnBossInline }]
    ]
  }

  return keyboard
}


/**
 * @param {Mongoose:Employee} employee - Бот, который должен ответить
 * @param {Array} notes - Опавещения на сегодя и завтра(ecли есть)
 * @return {Array} keyboard - Опций Уйду пораньше Picker День
 */
function leaveEarlyDayPickerInlines(employee, notes) {
  let noted = [false, false]
  let keyboard = []
  notes.forEach(function (note, i) {
    if(note.nedorabotaiu){
      noted[i] = true;
    }
  })
  if(noted[0] && noted[1]) {
    // bot.answerCallbackQuery(msg.id, 'Офигел? Ты же уже отпрашивался на сегодня!')
    keyboard = [
        [
          { text: strings().back, callback_data: strings().actionTypes.warnBossInline }
        ]
    ]
  }
  if(!noted[0] && noted[1]){
    // bot.answerCallbackQuery(msg.id, 'Офигел? Ты же уже отпрашивался на завтра!')
    keyboard = [
        [
          { text: strings().dayPicker.today, callback_data: strings().actionTypes.iwillLeaveEarlyDayInline + strings().inlineSeparator + strings().dayPicker.today }
        ],
        [
          { text: strings().back, callback_data: strings().actionTypes.warnBossInline }
        ]
    ]
  }
  if(noted[0] && !noted[1]){
    // bot.answerCallbackQuery(msg.id, 'Офигел? Ты же уже отпрашивался на сегодня!')
    keyboard = [
        [
          { text: strings().dayPicker.tomorrow, callback_data: strings().actionTypes.iwillLeaveEarlyDayInline +strings().inlineSeparator + strings().dayPicker.tomorrow }
        ],
        [
          { text: strings().back, callback_data: strings().actionTypes.warnBossInline }
        ]
    ]
  }
  if(!noted[0] && !noted[1]){
    keyboard = [
        [
          { text: strings().dayPicker.today, callback_data: strings().actionTypes.iwillLeaveEarlyDayInline + strings().inlineSeparator + strings().dayPicker.today },
          { text: strings().dayPicker.tomorrow, callback_data: strings().actionTypes.iwillLeaveEarlyDayInline +strings().inlineSeparator + strings().dayPicker.tomorrow },
        ],
        [
          { text: strings().back, callback_data: strings().actionTypes.warnBossInline }
        ]
    ]
  }
  return keyboard
}

/**
 * @param {Mongoose:Employee} employee - Бот, который должен ответить
 * @param {String} day - сегодня или завтра
 * @return {Array} keyboard - Опций Уйду пораньше Picker Время
 */
function leaveEarlyTimePickerInlines(employee, day) {
  let keyboard = [
      [
        { text: strings().timePicker.min15, callback_data: strings().actionTypes.iwillLeaveEarlyTimeInline + strings().inlineSeparator + strings().timePicker.min15  + strings().inlineSeparator + day },
        { text: strings().timePicker.hour1, callback_data: strings().actionTypes.iwillLeaveEarlyTimeInline +strings().inlineSeparator + strings().timePicker.hour1  + strings().inlineSeparator + day },
        { text: strings().timePicker.hour3OrMore, callback_data: strings().actionTypes.iwillLeaveEarlyTimeInline + strings().inlineSeparator + strings().timePicker.hour3OrMore  + strings().inlineSeparator + day }
      ],
      [
        { text: strings().back, callback_data: strings().actionTypes.sendLeaveEarlyInline }
      ]
  ]
  return keyboard
}

//Не приду
const NotComeDayPickerInlines = [
    [
      { text: strings().dayPicker.today, callback_data: strings().actionTypes.iNotComeDayInline + strings().inlineSeparator + strings().dayPicker.today },
      { text: strings().dayPicker.tomorrow, callback_data: strings().actionTypes.iNotComeDayInline + strings().inlineSeparator + strings().dayPicker.tomorrow },
    ],
    [
      { text: strings().back, callback_data: strings().actionTypes.warnBossInline }
    ]
]




// *** Send Inline Keybords *** //

/**
  * Оправлает опций для Предупреждений Биг босса
  * @param {Telegram: Bot} Bot бот который должен ответить
  * @param {Telegram: message} msg Полученное сообщение
 */
function sendWarnOptions(bot, msg) {
  const chatId = msg.message.chat.id
  const msgId = msg.message.message_id
  keyboards.editMessage(
      bot,
      chatId,
      msgId,
      strings().chooseActionMessage,
      warnOptionsInlines
  );
}

/**
  * Отправить Опций для Оьед
  * @param {Telegram: Bot} бот Bot, который должен ответить
  * @param {Telegram: message} msg Полученное сообщение
  * @param {Mongoose: Employee} employee Сотрудик который отпрашивается
 */
function sendLunchOptions(bot, msg, employee) {
  const note = checkForNotes(employee, new Date())[0]
  const chatId = msg.message.chat.id
  const msgId = msg.message.message_id
  keyboards.editMessage(
      bot,
      chatId,
      msgId,
      strings().LunchQuestion,
      LunchOptionsInlines(employee, note)
  );
}

/**
  * Отправить Опций для Опаздание
  * @param {Telegram: Bot} бот Bot, который должен ответить
  * @param {Telegram: message} msg Полученное сообщение
 */
function sendLateOptions(bot, msg) {
  const chatId = msg.message.chat.id
  const msgId = msg.message.message_id
  keyboards.editMessage(
      bot,
      chatId,
      msgId,
      strings().LateQuestion,
      LateOptionsInlines
  );
}

/**
  * Отправить Опций для пойлу по делам
  * @param {Telegram: Bot} бот Bot, который должен ответить
  * @param {Telegram: message} msg Полученное сообщение
  * @param {Mongoose: Employee} employee Сотрудик который отпрашивается
 */
function sendWentOptions(bot, msg, employee) {
  const chatId = msg.message.chat.id
  const msgId = msg.message.message_id
  const note = checkForNotes(employee, new Date())[0]
  let question = strings().iWentQuestion
  if(note.gone || (note.selfGo && !note.selfGo[note.selfGo.length-1][1])) {
    question = strings(employee).youReturnedQuestion
  }
  keyboards.editMessage(
      bot,
      chatId,
      msgId,
      question,
      iWentOptionsInlines(employee, note)
  );
}


/**
  * Отправить Опций для пойлу по делам
  * @param {Telegram: Bot} бот Bot, который должен ответить
  * @param {Telegram: message} msg Полученное сообщение
  * @param {Mongoose: Employee} employee Сотрудик который отпрашивается
 */
function sendLeaveEarlyDayPicker(bot, msg, employee) {
  const chatId = msg.message.chat.id
  const msgId = msg.message.message_id
  const notes = checkForNotes(employee, [nd(), Ago('days', 1)])
  let question = strings(employee).iwillLeaveEarlyQuestion
  keyboards.editMessage(
      bot,
      chatId,
      msgId,
      question,
      leaveEarlyDayPickerInlines(employee, notes)
  );
}

/**
  * Отправить Опций для пойлу по делам
  * @param {Telegram: Bot} бот Bot, который должен ответить
  * @param {Telegram: message} msg Полученное сообщение
  * @param {Mongoose: Employee} employee Сотрудик который отпрашивается
 */
function sendLeaveEarlyTimePicker(bot, msg, employee, day) {
  const chatId = msg.message.chat.id
  const msgId = msg.message.message_id
  let question = strings(employee).iwillLeaveEarlyQuestion
  keyboards.editMessage(
      bot,
      chatId,
      msgId,
      question,
      leaveEarlyTimePickerInlines(employee, day)
  );
}

/**
  * Отправить Опций Не приду
  * @param {Telegram: Bot} бот Bot, который должен ответить
  * @param {Telegram: message} msg Полученное сообщение
  * @param {Mongoose: Employee} employee Сотрудик который отпрашивается
 */
function sendNotComeDayPicker(bot, msg, employee) {
  const chatId = msg.message.chat.id
  const msgId = msg.message.message_id
  const notes = checkForNotes(employee, [nd(), Ago('days', 1)])
  let question = strings().iNotComeDayQuestion
  keyboards.editMessage(
      bot,
      chatId,
      msgId,
      question,
      NotComeDayPickerInlines
  );
}





/** Send Warn Functions **/

/**
  * говорит что уходит на обед
  * @param {Mongoose: Employee} employee Сотрудик который отпрашивается
  * @param {Boolean} going пошел или Вернулся
 */
function goToLunch(employee, going) {
  return new Promise((resolve, reject) => {
    if(employee) {
      let todayArray = dateToArray()
      let day = matchInObjArr(employee.days, todayArray, ['year', 'month', 'day']);
      if(going) {
        if(day.length > 0 ) {
          if(!employee.days[day[0]].notes) employee.days[day[0]].notes = {}
            employee.days[day[0]].notes.lunch = true;
            if(!employee.days[day[0]].notes.reasons) employee.days[day[0]].notes.reasons =[]
            employee.days[day[0]].notes.reasons.push(strings(employee).LunchMessagesForAdmin.wentToLunch)
        } else {
          employee.days.push({
            year:todayArray[0],
            month: todayArray[1],
            day: todayArray[2],
            notes: {
              lunch: true,
              reasons: [strings(employee).LunchMessagesForAdmin.wentToLunch]
            }
          })
        }
      } else {
        if(day.length > 0 ) {
          if(!employee.days[day[0]].notes) employee.days[day[0]].notes = {}
          if(employee.days[day[0]].notes.lunch){
            employee.days[day[0]].notes.lunch = false;
          } else {
            employee.days[day[0]].notes.lunch = false;
          }
        } else {
          employee.days.push({
            year:todayArray[0],
            month: todayArray[1],
            day: todayArray[2],
            notes: {
              lunch: false
            }
          })
        }
      }
      employee.markModified('days');
      employee.save()
        .then(emp => {
          resolve({
            success: true
          })
        })
        .catch(err => {
          reject(err)
        })
    } else {
      reject({
        success: false
      })
    }
  })
}


/**
  * вызывается если человек говорит что опаздает
  * @param {Mongoose: Employee} employee Сотрудик который отпрашивается
  * @param {String} period Время на сколько опаздает (15 мин \\ 30 мин \\ 1 час )
 */
function iWillLate(employee, period) {
    let inTime = '10:15'
    let cmp = comp(new Date(), inTime, 'min')
    return new Promise((resolve, reject) => {
      if (cmp.result == '<') {
        SendMsgToAdmins(`${employee.firstname} ${employee.lastname} (${employee.employee_id}) говорит что опоздает на ${period}` )
        let todayArray = dateToArray()
        let day = matchInObjArr(employee.days, todayArray, ['year', 'month', 'day']);
        if(day.length > 0 && employee.days[day[0]].notes) {
          if(employee.days[day[0]].notes.late){
            employee.days[day[0]].notes.late = true;
            if(!employee.days[day[0]].notes.reasons) employee.days[day[0]].notes.reasons =[]
            employee.days[day[0]].notes.reasons.push('Опаздаю на '+period)
          }
        } else {
          employee.days.push({
            year:todayArray[0],
            month: todayArray[1],
            day: todayArray[2],
            notes: {
              late: true,
              reasons: ['Опаздаю на '+period]
            }
          })
        }
        employee.markModified('days');
        employee.save()
          .then( emp => {
            resolve({
              success: true
            })
          })
          .catch(err => {
            reject(err)
          })
      } else {
        resolve({
          success: false
        })
      }
    })

}


/**
  * говорит что уходить куда-либо в боте
  * @param {Mongoose: Employee} employee Сотрудик который отпрашивается
  * @param {String} whereGo cообщение
  * @param {Вoolean} forself если true для себя или для компании
 */
function iWentTo(employee, whereGo, forself) {
    let where = 'ушел не известно куда'
    let msgForEmp = 'ушел не известно куда'
    let came = false //пришел или нет
    let self = forself
    switch (whereGo) {
      case strings().whereWentOptions.iwentPersonalThings:
        where = strings(employee).iWentMessagesForAdmin.personalThings
        msgForEmp = strings().iWentAnswerCallback.personalThings
        self = true
        break;
      case strings().whereWentOptions.iwentBank:
        where = strings(employee).iWentMessagesForAdmin.bank
        msgForEmp = strings().iWentAnswerCallback.bank
        break;
      case strings().whereWentOptions.iwentPostOffice:
        where = strings(employee).iWentMessagesForAdmin.postOffice
        msgForEmp = strings().iWentAnswerCallback.postOffice
        break;
      case strings().whereWentOptions.iwentTaxCommitte:
        where = strings(employee).iWentMessagesForAdmin.taxCommitte
        msgForEmp = strings().iWentAnswerCallback.taxCommitte
        break;
      case strings().whereWentOptions.ireturned:
        where = strings(employee).iWentMessagesForAdmin.returned
        msgForEmp = strings(employee).iWentAnswerCallback.returned
        came = true;
        break;
      default:
        where = 'ушел по причине: ' + whereGo
    }

    return new Promise((resolve, reject) => {
      let todayArray = dateToArray()
      let day = matchInObjArr(employee.days, todayArray, ['year', 'month', 'day'])
      if(self){
        console.log(self, '(SELF)');
        if(day.length > 0 ) {
          if(!employee.days[day[0]].notes) employee.days[day[0]].notes = {}
          if(!employee.days[day[0]].notes.selfGo) employee.days[day[0]].notes.selfGo = []
            if(!came){
              if(!employee.days[day[0]].notes.reasons) employee.days[day[0]].notes.reasons =[]
              if(employee.days[day[0]].notes.reasons.indexOf(msgForEmp) < 0) {
                employee.days[day[0]].notes.reasons.push(msgForEmp)
                employee.days[day[0]].notes.selfGo.push([nd(),false])
              }
            } else {
              employee.days[day[0]].notes.selfGo[employee.days[day[0]].notes.selfGo.length-1][1]= nd()
            }
        } else {
          employee.days.push({
            year:todayArray[0],
            month: todayArray[1],
            day: todayArray[2],
            notes: {
              selfGo: [[nd(),false]],
              reasons: [msgForEmp]
            }
          })
        }
      } else {
        if(day.length > 0 ) {
          if(!employee.days[day[0]].notes) employee.days[day[0]].notes = {}
            employee.days[day[0]].notes.gone = came ? false : nd();
            if(!came) {if(!employee.days[day[0]].notes.reasons) employee.days[day[0]].notes.reasons =[]
            if(employee.days[day[0]].notes.reasons.indexOf(where) <0)
            employee.days[day[0]].notes.reasons.push(where);}
        } else {
          employee.days.push({
            year:todayArray[0],
            month: todayArray[1],
            day: todayArray[2],
            notes: {
              gone: came ? false : nd(),
              reasons: [where]
            }
          })
        }
      }
      employee.markModified('days')
      employee.save()
        .then(emp => {
          resolve({
            success: true,
            messageForAdmin: where,
            messageForEmployee: msgForEmp,
          })
        })
        .catch(err => {
          reject(err)
        })
    })
}

/**
  * вызывается когда кто-то предупреждает в боте о том что уйдет пораньше
  * @param {Mongoose: Employee} employee Сотрудик
  * @param {String} alert cообщение
 */
function leaveEarly(employee, when, time) {
    let todayArray = dateToArray((when == strings().dayPicker.today) ? null : Ago('days', 1))
    let day = matchInObjArr(employee.days, todayArray, ['year', 'month', 'day']);
    return new Promise((resolve, reject) => {
      if(day.length > 0 ) {
        if(!employee.days[day[0]].notes) employee.days[day[0]].notes = {}
          employee.days[day[0]].notes.nedorabotaiu = nd();
          if(!employee.days[day[0]].notes.reasons) employee.days[day[0]].notes.reasons =[]
          employee.days[day[0]].notes.reasons.push(when+' уйду пораньше на '+time )
      } else {
        employee.days.push({
          year:todayArray[0],
          month: todayArray[1],
          day: todayArray[2],
          notes: {
            nedorabotaiu: nd(),
            reasons: ['уйду пораньше на '+time]
          }
        })
      }
      employee.markModified('days')
      employee.save()
        .then(emp => {
          resolve({
            success: true
          })
        })
        .catch(err => {
          reject(err)
        })
    })
}



// *** Helper Functions *** //

/**
  * Проверка сотрудника на предупреждения
  * @param {Mongoose: Employee} employee Сотрудик
  * @param {Date} dateArray Дата
  * @return {Array} Опавещений на определенный день или дни
 */
function checkForNotes(employee, dateArray) {
    let result = [];
    if(dateArray && dateArray instanceof Array ){
      dateArray.forEach(function (date) {
        let todayArray = dateToArray(date)
        let day = matchInObjArr(employee.days, todayArray, ['year', 'month', 'day']);
        if(day.length > 0 ) {
          if(employee.days[day[0]].notes) {
            result.push(employee.days[day[0]].notes)
          } else {
            result.push({})
          }
        } else {
          result.push({})
        }
      })
    }
    else {
      let todayArray = dateToArray(dateArray)
      let day = matchInObjArr(employee.days, todayArray, ['year', 'month', 'day']);
      if(day.length > 0) {
        if(employee.days[day[0]].notes){
          result.push(employee.days[day[0]].notes)
        } else {
          result.push({})
        }
      } else {
        result.push({})
      }

    }

    return result
}
