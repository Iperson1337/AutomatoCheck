const strings = require('./strings')
const dbmanager = require('../../database/dbmanager')
const keyboards = require('./keyboards');

global.eventEmitter.on(strings().actionTypes.employeeMainMenu, ({ bot, msg, employee }) => {
  console.log('event employeeMainMenu');
  sendEmployeeMenuInline(bot, msg)
})

global.eventEmitter.on(strings().actionTypes.backMenu, ({ bot, msg, employee }) => {
  keyboards.sendMainMenu(bot, msg.message.chat.id);
})


const employeeKeyboardInline = [
  [{ text: strings().employeeMenuOptions.warnToBoss, callback_data: strings().actionTypes.warnBossInline }],
  [{ text: strings().employeeMenuOptions.salaryInfo, callback_data: strings().actionTypes.salaryInfoInline }],
  [{ text: strings().employeeMenuOptions.autocheckin, callback_data: strings().actionTypes.autoCheckinInline }],
  [{ text: strings().employeeMenuOptions.back, callback_data: strings().actionTypes.backMenu }],
];

// Functions
function sendEmployeeMenuInline(bot, msg) {
  const chatId = msg.message.chat.id
  const msgId = msg.message.message_id
  keyboards.editMessage(
      bot,
      chatId,
      msgId,
      strings().chooseActionMessage,
      employeeKeyboardInline
  );
}



/**
 * Function to send request for username to users without Telegram username
 * @param {Telegram:Bot} bot - Bot that should send message
 * @param {Telegram:Message} msg - Message that triggered this action
 */
function sendAskForUsername(bot, msg) {
  bot.sendMessage(msg.from.id, strings().askForUsername);
}

/**
 * Checks if state of user that sent message is one of input ones
 *
 * @param {Telegram:Message} msg - Message received
 * @param {Function} callback - Callback(input_state, user) that is called when check is done
 */
function textInputCheck(msg, callback) {
  dbmanager.findEmployee({ botId: msg.chat.id })
    .then((employee) => {
      if (employee) callback(employee.input_state, employee);
      else callback();
    })
    .catch(/** todo: handle error */);
}

/**
 * Function to send note to user that they are banned
 * @param {Telegram:Bot} bot - Bot that should send message
 * @param {Telegram:Message} msg - Message that triggered this action
 */
function sendBanMessage(bot, msg) {

  bot.sendMessage(msg.from.id, strings().banMessage);
}

module.exports = {
  sendAskForUsername,
  textInputCheck
}
