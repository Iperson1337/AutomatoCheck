/**
 * Storage for all the strings in project; change once, use everywhere ;)
 *
 * @module helpers/strings
 */

/**
 * Function to get the right language file
 * @param {Mongoose:User} user User that requests file
 * @return {Object[String:String]} Object containing localization strings
 */
function locale(employee) {
  let elOrLa = ''
  let sheOrhe = ''
  let syaOrAs = ''
  let HimOrHer = ''

  if(employee) {
    elOrLa = employee.getSuffixElLa()
    sheOrhe = employee.getSuffix()
    syaOrAs = employee.getSuffixSyaAs()
    HimOrHer = employee.getSuffixHimHer()
  }
  const russian = {
    mainMenuMessage: 'Напиши или нажми на «Помощь» или «Меню», чтобы узнать доступные команды',
    chooseActionMessage: 'Выбери действие',
    helpMessage: 'Все доступные команды: \n «Помощь» — список всех команд \n «Меню» — меню с информацией по работе',
    banMessage: 'Похоже, Вас забанили. Пожалуйста, напишите нам в поддержку @support, если произошла какая-то ошибка — разберемся 👍',
    loadingMessage: '🦄 Загрузка...',
    askForUsername: 'Похоже, у Вас еще нет юзернейма в Телеграме. Пожалуйста, зайдите в настройки Телеграма и установите себе юзернейм. Спасибо!',
    deprecatedMessage: 'Это сообщение устарело. Пожалуйста, пролистайте вниз, чтобы увидеть новую версию сообщения ⤵️',
    backMainMenu: 'В главное меню',
    error: `Извините произошла непредвиденная ошибка, повторите попытку позднее`,
    back: '⬅️ Назад',
    selected: '✅ ',
    Left: '<',
    Right: '>',
    inlineSeparator: '~',
    cancel: '❌ Отмена',
    true: true,
    false: false,
    mainMenuOptions: {
      menu: 'Меню',
      help: 'Помощь',
    },
    employeeMenuOptions: {
      warnToBoss: 'Предупредить биг босса',
      salaryInfo: 'Че там ЗП ?',
      autocheckin: 'Авточекин',
      back: '🔙 Назад',
    },
    //все значения должны быть уникальными
    actionTypes: {
      //mainMenuOptions
      warnBossInline: 'wbI',
      salaryInfoInline: 'siI',
      autoCheckinInline: 'aciI',
      //warnOptions
      sendLunchOptionsInline: 'lunchI',
      sendLateOptionsInline: 'lateI',
      sendLeaveEarlyInline: 'earlyI',
      sendiWentInline: 'wentI',
      sendNotComeInline: 'notComeI',
      //LunchOptions
      igotoLunchInline: 'igtlI',
      icomeFromLunchInline: 'icflI',
      //iWillLateOptions
      beLateForInline: 'blfI',
      //iWillGoOptions
      iWentInline: 'iwentI',
      iambackSelfInline:'iambackSelf',
      //iwillLeaveEarly
      iwillLeaveEarlyDayInline: 'iwledI',
      iwillLeaveEarlyTimeInline: 'iwletI',
      //iwillNotCome
      iNotComeDayInline: 'incI',

      backMenu: 'backMenu',
      employeeMainMenu: 'emI'
    },

    warnOptions: {
      igotoLunch: "Обед",
      iwillLate: "Опаздаю",
      iwillgo: "Ухожу по делам",
      iwillLeaveEarly: "Уйду пораньше",
      iwillNotCome: "Не приду",
    },

    timePicker: {
      min15: '15 минут',
      hour1: '1 час',
      hour3OrMore: '3 часа и более'
    },

    dayPicker: {
      today: 'Сегодня',
      tomorrow: 'Завтра'
    },

    //Обед
    LunchOptions: {
      iwilleat: "Пойду поем",
      alreadyAte: `Уже наел${syaOrAs}`
    },
    LunchMessagesForAdmin: {
      wentToLunch: `уш${elOrLa} на обед`,
      cameFromLanch: `приш${elOrLa} с обеда`
    },
    LunchQuestion: 'Чё там с обедом?',

    LunchAnswerCallback: {
      iwilleat: 'Не подавись! Кушай хорошо — работы много!',
      alreadyAte: 'Плодотворной работы! Дедлайн не за горами...',
    },

    //Опаздания
    LateQuestion: 'Чё там, на сколько опаздываешь?',

    //Пойду
    iWentOptions: {
      personalThings: 'Личные дела',
      bank: `Пош${elOrLa} в банк`,
      postOffice: `Пош${elOrLa} на почту`,
      taxCommitte: `Пош${elOrLa} в налоговый комитет`,
      returned: `Вернул${syaOrAs}`
    },
    iWentQuestion: 'Почему ты уходишь? ☹️',
    youReturnedQuestion: `Ты вернул${syaOrAs}?`,

    iWentMessagesForAdmin: {
      personalThings: `уш${elOrLa} по личным делам.`,
      bank: `уш${elOrLa} в банк.`,
      postOffice: `уш${elOrLa} на почту.`,
      taxCommitte:`уш${elOrLa}  в налоговый комитет.`,
      returned: `он${sheOrhe} вернул${syaOrAs}.`
    },
    iWentAnswerCallback: {
      personalThings: 'уходишь по личным делам.',
      bank: 'уходишь в банк.',
      postOffice: 'идёшь на почту.',
      taxCommitte: 'уходишь в налоговый комитет.',
      returned: `вернул${syaOrAs}`
    },

    whereWentOptions: {
      iwentPersonalThings: 'personal',
      iwentBank: 'bank',
      iwentPostOffice: 'postOffice',
      iwentTaxCommitte: 'taxCommitte',
      ireturned: 'returned'

    },
    //Уйду пораньше
    iwillLeaveEarlyQuestion: `Когда уходить собрал${syaOrAs}?`,
    iNotComeDayQuestion: 'Когда не придешь?',




  }

  return russian;
}

/** Exports */
module.exports = locale;
