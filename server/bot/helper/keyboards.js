/**
 * File that should handle all keyboards creations and functions (ideally)
 *
 * @module helpers/keyboards
 * @license MIT
 */

/** Dependencies */
const strings = require('./strings');
const dbmanager = require('../../database/dbmanager');

/** Keyboards */
/**
 * Returns localized main menu keyboard
 * @param {Mongoose:User} user User that contains info about localization
 * @return {Telegram:InlineKeyboard} Localized keyboard
 */
function mainMenuKeyboard(employee) {
  return [
    [{ text: strings(employee).mainMenuOptions.menu },
    { text: strings(employee).mainMenuOptions.help }],
  ];
}

function employeeKeyboard(employee){
  return [
    [{ text: strings().employeeMenuOptions.warnToBoss, callback_data: strings().actionTypes.warnBossInline }],
    [{ text: strings().employeeMenuOptions.salaryInfo, callback_data: strings().actionTypes.salaryInfoInline }],
    [{ text: strings().employeeMenuOptions.autocheckin, callback_data: strings().actionTypes.autoCheckinInline }],
    [{ text: strings().employeeMenuOptions.back, callback_data: strings().actionTypes.backMenu }],
  ];
}


const helpKeyboard = [
  [{ text: 'Контактная поддержка', url: 'https://telegram.me/alibi1337' }],
];

const SupportKeyboard = [
  [{ text: 'Контактная поддержка', url: 'https://telegram.me/alibi1337' }],
];

/** Functions */


/**
 * Sends main menu keyboard to user with chat id
 *
 * @param {Telegram:Bot} bot - Bot that should send keyboard
 * @param {Number} chatId - Chat id of user who should receive this keyboard
 */
function sendMainMenu(bot, chatId, firstTime) {
  return dbmanager.findEmployee({ botId: chatId })
    .then((employee) => {
      return sendKeyboard(
        bot,
        chatId,
        ((firstTime) ? strings(employee).mainMenuMessage : strings(employee).mainMenuMessage),
        mainMenuKeyboard(employee)
      );
    })
    .catch(/** todo: handle error */);
}

/**
 * Отправляет меню сотрудника с идентификатором чата;
 * @param {Telegram:Bot} bot - Bot that should send keyboard
 * @param {Number} chatId - Chat id of user who should receive keyboard
 */
function sendEmployeeMenu(bot, chatId) {
  /**
   * Основная внешняя клавиатура для Сотрудника.
   */
  dbmanager.findEmployee({ botId: chatId })
    .then((employee) => {

      sendInline(
        bot,
        chatId,
        strings().chooseActionMessage,
        employeeKeyboard(employee)
      );
    })
    .catch(/** todo: handle error */);
}

/**
 * Sends menu with help to user chat id
 *
 * @param {Telegram:Bot} bot - Bot that should send keyboard
 * @param {Number} chatId - Chat id of user who should receive keyboard
 */
function sendHelp(bot, chatId) {
  sendInline(
    bot,
    chatId,
    strings().helpMessage,
    helpKeyboard
  );
}

/**
 * Sends keyboard to user
 *
 * @param {Telegram:Bot} bot - Bot that should send keyboard
 * @param {Number} chatId - Telegram chat id where to send keyboard
 * @param {String} text - Text that should come along with keyboard
 * @param {Telegram:Keyboard} keyboard Keyboard that should be sent
 * @param {Function} then - Function that should be executed when message is delivered
 */
function sendKeyboard(bot, chatId, text, keyboard, then) {
  const options = {
    reply_markup: {
      keyboard,
      resize_keyboard: true,
    },
    disable_web_page_preview: 'true',
  };

  options.reply_markup = JSON.stringify(options.reply_markup);
  return bot.sendMessage(chatId, text, options)
}

/**
 * Sends inline to user
 *
 * @param {Telegram:Bot} bot - Bot that should send inline
 * @param {Number} chatId - Chat id where to send inline
 * @param {String} text - Text to send along with inline
 * @param {Telegram:Inline} keyboard - Inline keyboard to send
 */
function sendInline(bot, chatId, text, keyboard, then, markdown) {
  const options = {
    reply_markup: { inline_keyboard: keyboard },
    disable_web_page_preview: 'true',
  };

  if (markdown) {
    options.parse_mode = 'Markdown';
  }
  options.reply_markup = JSON.stringify(options.reply_markup);

  bot.sendMessage(chatId, text, options)
}

/**
 * Method to edit message with inline
 *
 * @param {Telegram:Bot} bot - Bot that should edit msg
 * @param {Number} chatId - Id of chat where to edit msg
 * @param {Number} messageId - Id of message to edit
 * @param {Telegram:InlineKeyboard} keyboard - Inline keyboard to appear in message
 */
function editInline(bot, chatId, messageId, keyboard) {
  const inlineMarkup = JSON.stringify({
    inline_keyboard: keyboard,
  });

  const options = {
    chat_id: chatId,
    message_id: messageId,
    disable_web_page_preview: 'true',
  };

  bot.editMessageReplyMarkup(inlineMarkup, options)
}

/**
 * Method to edit message
 *
 * @param {Telegram:Bot} bot - Bot that should edit msg
 * @param {Number} chatId - Id of chat where to edit msg
 * @param {Number} messageId - Id of message to edit
 * @param {String} text - Text to appear in message
 * @param {Telegram:InlineKeyboard} keyboard - Inline keyboard to appear in message
 */
function editMessage(bot, chatId, messageId, text, keyboard) {
  return bot.editMessageText(text, {
    chat_id: chatId,
    message_id: messageId,
    reply_markup: JSON.stringify({
      inline_keyboard: keyboard,
    }),
    disable_web_page_preview: 'true',
  })
}


/**
 * Function to send message and hide keyboard
 * @param {Telegram:Bot} bot Bot that should send message
 * @param {String} chatId Id of chat where to send message
 * @param {String} text Text that should go to this message
 */
function hideKeyboard(bot, chatId, text) {
  return bot.sendMessage(chatId, text, {
    reply_markup: JSON.stringify({
      hide_keyboard: true,
    }),
    disable_web_page_preview: 'true',
  });
}

/** Exports */
module.exports = {
  mainMenuKeyboard,
  sendEmployeeMenu,
  helpKeyboard,
  SupportKeyboard,
  sendMainMenu,
  sendHelp,
  sendKeyboard,
  sendInline,
  editInline,
  editMessage,
  hideKeyboard,
};
