const Employee = require('../../app/Models/Employee');
const { SendMsgToAdmins } = require('./employees')
const { matchInObjArr, dateToArray, nd, Ago, comp } = require('../../helpers/AllFunctions')

//говорит что ушел куда-либо в боте
function alertAdmins(Some_id, alert, msg){
  // формируется запрос в бд, в зависимости от типа ID
  var query = Some_id ?
  (/^([0-9]{8,10})$/i.test(Some_id) ?
  {botId: Some_id} :
  {employee_id: Some_id.toString().toUpperCase()})
  : false;
  if(!query) return false
  // тут в зависимости от alert создается строка которая оповещает амина о том кула ушел человек
  Employee.findOne(query,  function (err, user) {
    var where = 'ушел не известно куда'; //переманная
    // var where2 = 'ушел не известно куда';
    var came = false; //пришел или нет
    var self = false; //по личным делам или делам компании
    switch (alert) {
        case 'self':
          self = true;
          where = 'ушел по личным делам.'
          where2 = 'уходишь по личным делам.'
          break;
        case 'bank':
          where = 'ушел в банк.'
          where2 = 'уходишь в банк.'
          break;
        case 'mail':
          where = 'ушел на почту.'
          where2 = 'идёшь на почту.'
          break;
        case 'nalog':
          where = 'ушел в налоговый комитет.'
          where2 = 'уходишь в налоговый комитет.'
          break;
        case 'came':
          where = 'он вернулся.'
          where2 = 'вернулся'
          came = true;
          break;
        case 'cameself':
          where = 'он вернулся.'
          where2 = 'вы вернулись.'
          self = true;
          came = true;
          break;
      default:
        where = 'ушел по причине: '+alert
    }
    SendMsgToAdmins(user.firstname+' '+user.lastname + ' (' +user.employee_id +') Оповещает вас, что '+where  )
    bot.answerCallbackQuery(msg.id, 'Ты сообщил БигБоссу, что ' + where2)
    var todayArray = dateToArray()
    var day = matchInObjArr(user.days, todayArray, ['year', 'month', 'day']);
    if(self){
      if(day.length > 0 ) {
        if(!user.days[day[0]].notes) user.days[day[0]].notes = {}
        if(!user.days[day[0]].notes.selfGo) user.days[day[0]].notes.selfGo = [];
          if(!came){
            if(!user.days[day[0]].notes.reasons) user.days[day[0]].notes.reasons =[]
            if(user.days[day[0]].notes.reasons.indexOf(where2)<0)
            user.days[day[0]].notes.reasons.push(where2);
            user.days[day[0]].notes.selfGo.push([nd(),false])
          } else {
            user.days[day[0]].notes.selfGo[user.days[day[0]].notes.selfGo.length-1][1]= nd()
          }
      } else {
        user.days.push({
          year:todayArray[0],
          month: todayArray[1],
          day: todayArray[2],
          notes: {
            selfGo: [[nd(),false]],
            reasons: [where2]
          }
        })
      }
    } else {
      if(day.length > 0 ) {
        if(!user.days[day[0]].notes) user.days[day[0]].notes = {}
          user.days[day[0]].notes.gone = came ? false : nd();
          if(!came) {if(!user.days[day[0]].notes.reasons) user.days[day[0]].notes.reasons =[]
          if(user.days[day[0]].notes.reasons.indexOf(where) <0)
          user.days[day[0]].notes.reasons.push(where);}
      } else {
        user.days.push({
          year:todayArray[0],
          month: todayArray[1],
          day: todayArray[2],
          notes: {
            gone: came ? false : nd(),
            reasons: [where]
          }
        })
      }

    }
    user.markModified('days');
    user.save()
  })
}

// говорит что уходит на обед или приходит "going" true or false
function goToLunch( Some_id , going, cb) {
  var query = Some_id ?
  (/^([0-9]{8,10})$/i.test(Some_id) ?
  {botId: Some_id} :
  {employee_id: Some_id.toString().toUpperCase()})
  : false;
  if(!query) return false
  Employee.findOne(query, function (err,user) {
    var todayArray = dateToArray()
    var day = matchInObjArr(user.days, todayArray, ['year', 'month', 'day']);
    if(going){
      SendMsgToAdmins(user.firstname+' '+user.lastname+' ('+user.employee_id+') ушел на обед' )
      if(day.length > 0 ) {
        if(!user.days[day[0]].notes) user.days[day[0]].notes = {}
        // if(user.days[day[0]].notes.lunch){
          // console.log(user.days[day[0]].notes.lunch,'exist');
          user.days[day[0]].notes.lunch = true;
          if(!user.days[day[0]].notes.reasons) user.days[day[0]].notes.reasons =[]
          user.days[day[0]].notes.reasons.push('Ушел на обед')
          if(cb) cb(false)
        // }
        // else {
        //   user.days[day[0]].notes.lunch = true;
        //   console.log(user.days[day[0]].notes.lunch,'exist2');
        //   if(cb) cb(true)
        // }
      } else {
        user.days.push({
          year:todayArray[0],
          month: todayArray[1],
          day: todayArray[2],
          notes: {
            lunch: true,
            reasons: ['Ушел на обед']
          }
        })
        console.log(user.days[user.days.length-1],'not exist');
        if(cb) cb(false)
      }

    } else {
      SendMsgToAdmins(user.firstname+' '+user.lastname+' ('+user.employee_id+') пришел с обеда' )
      if(day.length > 0 ) {
        if(!user.days[day[0]].notes) user.days[day[0]].notes = {}
        if(user.days[day[0]].notes.lunch){
          // console.log(user.days[day[0]].notes,day,'exist');
          user.days[day[0]].notes.lunch = false;
          if(cb) cb(false)
        } else {
          user.days[day[0]].notes.lunch = false;
          if(cb) cb(true)
        }
      } else {
        user.days.push({
          year:todayArray[0],
          month: todayArray[1],
          day: todayArray[2],
          notes: {
            lunch: false
          }
        })
        console.log(user.days[user.days.length-1],'not exist');
        if(cb) cb(false)
      }
    }
    user.markModified('days');
    user.save()
    // console.log(matchInObjArr(user.days, dateToArray(), ['year', 'month', 'day']));
    //,'[===>',JSON.stringify(user.days),'<===]'
  })
}

// вызывается если человек говорит что опаздает
function iWillLate(Some_id, period, cb) {
  var query = Some_id ?
  (/^([0-9]{8,10})$/i.test(Some_id) ?
  {botId: Some_id} :
  {employee_id: Some_id.toString().toUpperCase()})
  : false;
  if(!query) return false
  Employee.findOne(query, function (err,user) {
    var cmp = comp(new Date(), user.inTime, 'min')
    if(cmp.result == '<') {

      SendMsgToAdmins(user.firstname+' '+user.lastname+'('+user.employee_id+') говорит что опоздает на '+period )
      var todayArray = dateToArray()
      var day = matchInObjArr(user.days, todayArray, ['year', 'month', 'day']);
      if(day.length > 0 && user.days[day[0]].notes) {
        if(user.days[day[0]].notes.late){
          console.log(user.days[day[0]].notes,day,'exist');
          user.days[day[0]].notes.late = true;
          if(!user.days[day[0]].notes.reasons) user.days[day[0]].notes.reasons =[]
          user.days[day[0]].notes.reasons.push('Опаздаю на '+period)
          // if(cb) cb(false)
        } else {
          // if(cb) cb(true)
        }
      } else {
        user.days.push({
          year:todayArray[0],
          month: todayArray[1],
          day: todayArray[2],
          notes: {
            late: true,
            reasons: ['Опаздаю на '+period]
          }
        })

        console.log(user.days[user.days.length-1],'not exist');
        // if(cb) cb(false)
      }
      user.markModified('days');
      user.save()
      // console.log(matchInObjArr(user.days, dateToArray(), ['year', 'month', 'day']));
      //,'[===>',JSON.stringify(user.days),'<===]'
      cb(false)
    } else {
      cb(true)
    }

  })
}

// вызывается когда кто-то предупреждает в боте о том что уйдет пораньше
function leaveEarly(Some_id, when, time){
  // формируется запрос в бд, в зависимости от типа ID
  var query = Some_id ?
  (/^([0-9]{8,10})$/i.test(Some_id) ?
  {botId: Some_id} :
  {employee_id: Some_id.toString().toUpperCase()})
  : false;
  if(!query) return false
  // тут сформировался запрос в бд
  Employee.findOne(query,  function (err, user) {
    SendMsgToAdmins(user.firstname+' '+user.lastname + ' (' +user.employee_id +') говорит что '+when+' уйдёт пораньше на '+time  )
    var todayArray = dateToArray((when == 'сегодня') ? null : Ago('days', 1))
    var day = matchInObjArr(user.days, todayArray, ['year', 'month', 'day']);
    if(day.length > 0 ) {
      if(!user.days[day[0]].notes) user.days[day[0]].notes = {}
        user.days[day[0]].notes.nedorabotaiu = nd();
        if(!user.days[day[0]].notes.reasons) user.days[day[0]].notes.reasons =[]
        user.days[day[0]].notes.reasons.push(when+' уйду пораньше на '+time )
    } else {
      user.days.push({
        year:todayArray[0],
        month: todayArray[1],
        day: todayArray[2],
        notes: {
          nedorabotaiu: nd(),
          reasons: ['уйду пораньше на '+time]
        }
      })
    }
    user.markModified('days');
    user.save()
  })
}

// принимает id, callback, [2017,9,15] возвращает предупреждения за этот день
function noteStatus(Some_id, cb, dateArray){
  var query = Some_id ?
  (/^([0-9]{8,10})$/i.test(Some_id) ?
  {botId: Some_id} :
  {employee_id: Some_id.toString().toUpperCase()})
  : false;
  if(!query) return false
  Employee.findOne(query, function (err,user) {
    if(dateArray && dateArray instanceof Array ){
      var result = [];
      dateArray.forEach(function (date) {
        console.log(date)
        var todayArray = dateToArray(date)
        var day = matchInObjArr(user.days, todayArray, ['year', 'month', 'day']);
        if(day.length > 0 ) {

          if(user.days[day[0]].notes){
            result.push(user.days[day[0]].notes)
          } else {
            result.push({})
          }
        } else {
          result.push({})
        }
      })
      if(cb) cb(result)
    } else {

      var todayArray = dateToArray(dateArray)
      var day = matchInObjArr(user.days, todayArray, ['year', 'month', 'day']);
      if(day.length > 0) {
        if(user.days[day[0]].notes){
          if(cb) cb(user.days[day[0]].notes)
        } else {
          if(cb) cb({})
        }
      } else {

        if(cb) cb({})
      }

    }
  })
}


module.exports.alertAdmins = alertAdmins
module.exports.goToLunch = goToLunch
module.exports.iWillLate = iWillLate
module.exports.leaveEarly = leaveEarly
module.exports.noteStatus = noteStatus
