const Employee = require('../../app/Models/Employee');
const { bot } = require('../initBot')
const waterfall= require('async/waterfall');
const moment = require('moment');
// дает окончание -ся или -ась в зависимости от пола
function getWordEndSyaAs(user_Id){
    Employee.findOne({
      botId: user_Id
      }, function(err, emp) {
        if (!err) {
          wordEnding= emp.getSuffixSyaAs();
          return wordEnding;
        } else {
         console.log('Не нашел имя сотрудника, который отправил эту команду');
        }
      })
}
// дает окончание -ел или -ла в зависимости от пола
function getWordEndElLa(user_Id){
    Employee.findOne({
      botId: user_Id
      }, function(err, emp) {
        if (!err) {
          wordEndElLa= emp.getSuffixElLa();
          return wordEndElLa;
        } else {
        console.log('Не нашел имя сотрудника, который отправил эту команду');
        }
      })
}
// дает окончание - или -а в зависимости от пола
function getWordEnd(user_Id){
    Employee.findOne({
      botId: user_Id
      }, function(err, emp) {
        if (!err) {
          wordEnd= emp.getSuffix();
          return wordEnd;
        } else {
          console.log('Не нашел имя сотрудника, который отправил эту команду');
        }
      })
}

function kickUser(id, user_Id){//для кика юзера, который по факту не в офисе
  Employee.find({
    disabled: false,
    employee_id: id,
    checked: true
  }, function(err, emp) {
    if (!err && emp.length > 0) {
      emp.forEach(function(e) {
        console.log('kicking ', e.employee_id);
        //var kickingDay = e.days[e.days.length-1].check; // //чекин-чекауты последнего дня
        //var resultOfKick = kickingDay.splice(kickingDay.length-1, 1);//удаляет последний чекин этого дня
        var kickingDay = e.days;
        // console.log('kickingDay!!!!!!!', kickingDay);
        var resultOfKick = kickingDay.splice(kickingDay.length-1, 1);//удаляет последний целый день чекина-чекаута
        // console.log('resultOfKick!!!!!!!', resultOfKick);
        e.checked = false;
        e.markModified('days');
        var adminNameKicked;
        var genderType;
        Employee.findOne({
          botId: user_Id
          }, function(err, emp) {
            if (!err) {
              adminNameKicked = emp.firstname;
              genderType = emp.getSuffix();
              return  ;
            } else {
            console.log('Не нашел имя админа, который отправил эту команду');
            }
          })
        e.save().then(function(a) {
          bot.sendMessage(user_Id, 'кикнул ' + e.employee_id),
          bot.sendMessage(e.botId, 'Эй! Тебя кикнул' + genderType + ' ' + adminNameKicked +  '. Теперь твой день не будет засчитан.') //пишет имя админа кто кикнул
        })
      })
    } else {

      bot.sendMessage(user_Id, 'Указанный сотрудник с id ' + id + ' не был зачекинен');
    }
  })
}

function autoCheckInUser(id, user_Id){//для чекина юзера, который по факту не в офисе
  Employee.find({
    disabled: false,
    employee_id: id,
    checked: false
  }, function(err, emp) {
    if (!err && emp.length > 0) {
      emp.forEach(function(e) {
        var kickingDay = e.days;
        e.checked = true;
        e.save().then(function(a) {
          bot.sendMessage(user_Id, 'Ты дал добро на авточекин ' + e.employee_id),
          bot.sendMessage(e.botId, 'Тебе одобрили авточекин. Теперь твой день будет засчитан.') //пишет имя админа кто кикнул
        })
      })
    } else {
      bot.sendMessage(user_Id, 'Указанный сотрудник с id ' + id + ' уже был зачекинен');
    }
  })
}

function checkEmployees(cb){
  var result = "Все кто в офисе:\n";
  waterfall([
    function(callback){
      Employee.find({disabled: false, checked: true}, function(err, employees){
        if(err) console.log(err);
        if (employees) {
          callback(null, employees);
        }
      })
    },
    function(employees, callback){
      employees.forEach(function(employee, e){
            result+=employee.employee_id+' '+employee.firstname+ ' '+employee.lastname+ ' в офисе\n';
      })
      callback(null, result);
    }
  ], function (err, result) {
    return cb(null, result);
  });
}

function absentEmployees(cb){
  var result = "Кто тусит вне в офиса:\n";
  waterfall([
    function(callback){
      Employee.find({disabled: false, checked: false}, function(err, employees){
        if(err) console.log(err);
        if (employees) {
          callback(null, employees);
        }
      })
    },
    function(employees, callback){
      employees.forEach(function(employee, e){
            result+=employee.employee_id+' '+employee.firstname+ ' '+employee.lastname+ ' тусит\n';
      })
      callback(null, result);
    }
  ], function (err, result) {
    return cb(null, result);
  });
}

function updating(){
  Employee.find({}, (err, employees)=>{
    employees.forEach((emp, e)=>{
      reportings.find({employee: emp._id}, (err, reps)=>{
        reps.forEach((rep, r)=>{
          var each = {};
            if(moment(rep.check_in).format('DD MM YYYY').toString() === moment(rep.check_out).format('DD MM YYYY').toString()){
             each={
                year: moment(rep.check_in).format('YYYY'),
                month: moment(rep.check_in).get('month'),
                day: moment(rep.check_in).format('D'),
                check: [[rep.check_in, rep.check_out]]
              }
            }else{
             each={
                year: moment(rep.check_in).format('YYYY'),
                month: moment(rep.check_in).get('month'),
                day: moment(rep.check_in).format('D'),
                check: [[rep.check_in, moment(rep.check_in).endOf("day").toISOString()]]
              }
            }
            employees[e].days.push(each);
            employees[e].save((err, savedEmployee)=>{
              console.log("сохранено", e);
            })
        })
      })
    })
  })
}


module.exports.getWordEndSyaAs = getWordEndSyaAs
module.exports.getWordEndElLa = getWordEndElLa
module.exports.getWordEnd = getWordEnd
module.exports.kickUser = kickUser
module.exports.autoCheckInUser = autoCheckInUser
module.exports.checkEmployees = checkEmployees
module.exports.absentEmployees = absentEmployees
module.exports.updating = updating
