const Employee = require('../../app/Models/Employee');
const { bot } = require('../initBot');
// отправляет сообщение в телеграм всем админам
function SendMsgToAdmins(msg) {
  Employee.find({admin:true},function (err,admins) {
    if(admins){
      admins.forEach(function (admin) {
        var adm = admin.botId;
        console.log('Sending Message to Boss', admin.employee_id, msg)
        bot.sendMessage(adm,msg)
      })
    }
  })
}

function pushNotificationAdmin(){
  var info = 'Привет!\n'+
  'Чеклист обновился для вашего удобства, ознакомьтесь с новыми изменениями и поднимите зарплату Акмарал:)\n'+
  '1. /info ID или /info Имя сотрудника -  инфо о сотруднике\n'+
  '2. Сотрудники теперь могут делать чекин через телефон с вашего одобрения (предусмотрен для тех случаев, если вы отправили кого-то по делам вне офиса)\n'
  Employee.find({disabled: false, admin: true}, (err, admins)=>{
    if(err) console.log(err);
    if(admins){
      admins.forEach(function(admin, a){
        bot.sendMessage(admin.botId, info)
      })
    }
  })
}

function pushNotificationEmployee(){
  var info = 'Привет, Автоматовцы!\n'+
  'Чеклист обновился для вашего удобства, ознакомьтесь с новыми изменениями и пользуйтесь:)\n'+
  '\nВ меню появилась кнопка авточекин, которую вы можете использовать, если вас отправили по делам Босса.\n'+
  'После нажатия, нужно выбрать админа, который отправил вас по делам и ждать одобрения.\n'+
  '\nПлодотворной недели и не забудьте сказать спасибо Акмарал и Даулету!'
  Employee.find({disabled: false, admin: false}, (err, employees)=>{
    if(err) console.log(err);
    if(employees){
      employees.forEach(function(employee, e){
        bot.sendMessage(employee.botId, info)
      })
    }
  })
}

module.exports.SendMsgToAdmins = SendMsgToAdmins
module.exports.pushNotificationAdmin = pushNotificationAdmin
module.exports.pushNotificationEmployee = pushNotificationEmployee
