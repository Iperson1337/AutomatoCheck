const express = require('express');
const router = express.Router();
const { verifyJWT_MW } = require('../app/Middlewares/admin.mw')

const checkController = require('../app/Controllers/check.controller');
const departmentsController = require('../app/Controllers/departments.controller');
const employeesController = require('../app/Controllers/employees.controller');
const tasksController = require('../app/Controllers/tasks.controller');
const adminController = require('../app/Controllers/admin/admin.controller');

router.post('/check', checkController.checkHandler);

router.get('/employees/:id/show', employeesController.showEmployee);

router.post('/tasks/add', tasksController.storeTasks);
router.get('/tasks/statistics', tasksController.statisticsofTasks);
router.get('/tasks/today', tasksController.getTodayTasks);
router.post('/tasks/complated', tasksController.doneTasks);

router.post('/admin', adminController.checkAdmin);
router.get('/admin/employees/all', employeesController.allEmployees);
router.post('/admin/employee/update', employeesController.updateEmployee);
router.post('/admin/employee/disabled', employeesController.disabledEmployee);

router.post('/admin/employees/add/holidays', employeesController.addHolidays);

router.get('/admin/employees/office/in', employeesController.inOfficeEmployees);
router.get('/admin/employees/office/outside', employeesController.outsideOfficeEmployees);
router.get('/admin/employees/count', employeesController.countEmployees);

router.get('/admin/employees/statistics/piechart', employeesController.piechartstatistic);
router.get('/admin/employees/statistics/month', employeesController.statisticofmonth);
//crud departments
router.get('/admin/departments', departmentsController.departments);
router.post('/admin/departments/add', departmentsController.addDepartment);
router.post('/admin/departments/delete', departmentsController.deleteDepartments);

router.get('/admin/departments/count', departmentsController.countDepartments);

router.post('/admin/tasks/add', tasksController.storeTasks);
router.put('/admin/tasks/update', tasksController.updateTask);


var top;

router.get('/topkarma', (req, res) => {
  res.status(200).send({top: top});
})

router.post('/karmatop', (req, res) => {
  top = req.body.top;
  res.status(200).send('ok');
})

module.exports = router
